
Deals Action Backend Application
======================================

### introduction
This is a backend application developed using Reactjs, Redux. This app is only used for backend UI.

### How to run
Run following commands
<pre><code>cd app
npm install
npm start</code></pre>


## Environment variables
Environment variables is the main mechanism of manipulating application settings. Currently application recognizes
following environment variables:

| Variable             | Default value | Description              |
| -------------------- | ------------- | ------------------------ |
| HOST                 | 0.0.0.0       | Address to listen on     |
| PORT                 | 3000          | Port to listen on        |

<pre><code>
  http://dealsactions.com/admin
</code></pre>
