import axios from 'axios';
import Auth from '../modules/Auth';

class RequestHandler {
    getRequest(method, url, data) {
        return new Promise(async (resolve, reject) => {

            return await axios({
                method: method,
                url: Auth.getRestURL() + url,
                data: data,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `bearer ${Auth.getToken('token')}`
                }
            }).then(response => {
                return resolve({error: null, result: response})
            }).catch(err => {
                return resolve({error: err, result: ''})
            });
        })
    }
}

export default RequestHandler;