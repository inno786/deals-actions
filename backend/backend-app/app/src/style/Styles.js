export const styles = {

    dashboard:{
        fontSize:'14px'
    },
    textCenter: {
        textAlign: 'center'
    },
    errorMessage: {
        padding: '0 16px',
        color: '#f44336'
    },
    successMessage: {
        padding: '0 16px',
        color: 'green'
    },
    mainContainer: {
        width: '100%'
    },
    removePadding: {
        paddingLeft: '5px !important',
        paddingRight: '5px !important'
    },
    mainThemeContailer: {
        backgroundColor: '#00BCD4',
        alignContent: 'center'
    },
    tableDefault: {
        width: '100%',
        border: '0px'
    },
    tableLayout: {
        width: '20%',
        verticalAlign: 'top'
    },
    tableContainer: {
        width: '80%'
    },
    title: {
        fontStyle: '16px',
        fontWeight: 'bold',
        padding: '5px',
        clear: 'both'
    },
    btnLeft: {
       /*  padding: '5px' */
    },
    btnRight: {
        float: 'right'
    },
    inputFieldAlign: {
        width: '25% !important',
        float: 'left'
    },
    checkboxAlign: {
        width: '20% !important',
        float: 'left',
        margin: '40px 0px 0px 0px'
    },
    selectboxAlign: {
        width: '100%!important',
        float: 'left',
        margin: '0px 0px 0px 10px'
    },
    selectboxContainer:{
        width: '40%!important'
    },
    searchBtn: {
        margin: '30px 0px 0px 20px'
    },
    stylebtm:{
        margin: '10px auto 10px 10px'
    },
    mainHeader:{

    },
    textEditor:{
        width: 475,
        height: 220,
        align: 'center'
    },
    chbWidth:{
        width:'5%',
        textAlign:'center',
        paddingLeft: '5px !important',
        paddingRight: '5px !important'
      },
      nameWidth:{
        width:'20%',
        textOverflow:'unset',
        textAlign:'center',
        paddingLeft: '5px !important',
        paddingRight: '5px !important'
      },
      statusWidth:{
        width:'10%',
        textAlign:'center',
        paddingLeft: '5px !important',
        paddingRight: '5px !important'
      },
      createdWidth:{
        width:'15%',
        textAlign:'center',
        paddingLeft: '5px !important',
        paddingRight: '5px !important'
      },
      updatedWidth:{
        width:'15%',
        textAlign:'center',
        paddingLeft: '5px !important',
        paddingRight: '5px !important'
      },
      actionWidth:{
        width:'30%',
        textOverflow:'unset',
        textAlign:'center',
        paddingLeft: '5px !important',
        paddingRight: '5px !important'
      },
      iconStyles:{
        marginRight: 24,
        fontSize:14,
        cursor:'pointer'
   },
   notificationMsg:{
   // text: "#FFFFFF"
   }

}