import React from 'react'
import { Link } from 'react-router-dom'
import {
    Table, TableBody, TableRow, TableRowColumn
}
    from 'material-ui/Table'
import ReactQuill from 'react-quill'
import 'react-quill/dist/quill.snow.css'
import RaisedButton from 'material-ui/RaisedButton'
import TextField from 'material-ui/TextField'
import SelectField from 'material-ui/SelectField'
import MenuItem from 'material-ui/MenuItem'
import { styles } from "../../style/Styles"

const Forms = (props) => (
    <form action="/" onSubmit={props.onSubmit}>
        {props.arrData.msg && <p style={styles.errorMessage}>{props.arrData.msg}</p>}
        <Table multiSelectable={false} selectable={false} style={{ width: '100%', float: 'left', border: 'none' }}>
            <TableBody displayRowCheckbox={false}>
                <TableRow style={styles.maiHeader}>
                    <TableRowColumn style={{ width: '50%' }}>
                        <h3>{props.mainTitle}</h3>
                    </TableRowColumn>
                    <TableRowColumn style={{ width: '50%', textAlign: 'right' }}>
                        <Link to='/admin/seo'>
                            <img src="/img/back-btn.png" width="30" alt="back" />
                        </Link>
                    </TableRowColumn>
                </TableRow>
                <TableRow>
                    <TableRowColumn colSpan='2' style={{ width: '100%' }}>
                        <Table multiSelectable={false} selectable={false}
                            style={{ width: '100%', float: 'left', border: 'none' }}>
                            <TableBody displayRowCheckbox={false}>
                                <TableRow displayBorder={false}>
                                    <TableRowColumn>
                                        <SelectField
                                            name="Type"
                                            id="type"
                                            value={props.arrData.field.type}
                                            onChange={props.onChangeSelect}
                                            floatingLabelText="Select Type"
                                            errorText={props.arrData.error.name}
                                        >
                                            <MenuItem value="Brands" key="Brands" primaryText="Brands" />
                                            <MenuItem value="Category" key="Category" primaryText="Category" />
                                            <MenuItem value="OfferBrand" key="OfferBrand" primaryText="Offer-Brand" />
                                            <MenuItem value="OfferCategory" key="OfferCategory" primaryText="Offer-Category" />
                                            <MenuItem value="Product" key="Product" primaryText="Product" />
                                        </SelectField>
                                    </TableRowColumn>
                                </TableRow>
                                <TableRow displayBorder={false}>
                                    <TableRowColumn>
                                        <TextField
                                            floatingLabelText="Title"
                                            name="title"
                                            value={props.arrData.field.title}
                                            onChange={props.onChange}
                                            multiLine={true}
                                        />
                                    </TableRowColumn>
                                </TableRow>
                                <TableRow displayBorder={false}>
                                    <TableRowColumn>
                                        <TextField
                                            floatingLabelText="Keyword"
                                            name="keyword"
                                            value={props.arrData.field.keyword}
                                            onChange={props.onChange}
                                            multiLine={true}
                                        />
                                    </TableRowColumn>
                                </TableRow>
                                <TableRow displayBorder={false}>
                                    <TableRowColumn>
                                        <TextField
                                            floatingLabelText="Description"
                                            name="description"
                                            value={props.arrData.field.description}
                                            onChange={props.onChange}
                                            multiLine={true}
                                        />
                                    </TableRowColumn>
                                </TableRow>
                                <TableRow displayBorder={false}>
                                    <TableRowColumn>
                                        <ReactQuill
                                            placeholder="Top Description"
                                            id="top_description"
                                            theme="snow"
                                            value={props.arrData.field.top_description}
                                            onChange={props.changeTopDesc}
                                            style={styles.textEditor}
                                        />
                                    </TableRowColumn>
                                </TableRow>
                                <TableRow displayBorder={false}>
                                    <TableRowColumn>
                                        <ReactQuill
                                            placeholder="Bottom Description"
                                            id="bottom_description"
                                            theme="snow"
                                            value={props.arrData.field.bottom_description}
                                            onChange={props.changeBottomDesc}
                                            style={styles.textEditor}
                                        />
                                    </TableRowColumn>
                                </TableRow>
                            </TableBody>
                        </Table>
                    </TableRowColumn>
                </TableRow>
                <TableRow>
                    <TableRowColumn colSpan='2' style={{ textAlign: 'center' }}>
                        <RaisedButton type="submit" label={props.btnText} secondary={true} style={{ textAlign: 'center' }} />
                    </TableRowColumn>
                </TableRow>
            </TableBody>
        </Table>
    </form>
);
export default Forms;
