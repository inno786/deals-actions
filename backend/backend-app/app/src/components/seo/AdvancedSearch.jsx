import React from 'react';
import {Link} from 'react-router-dom';
import {
    Table, TableBody, TableRow, TableRowColumn
}
    from 'material-ui/Table';
import RaisedButton from 'material-ui/RaisedButton';
import {styles} from '../../style/Styles'

const AdvancedSearch = () => (
   <Table multiSelectable={false} selectable={false} style={{width: '100%', float: 'left', border: 'none'}}>
            <TableBody displayRowCheckbox={false}>
                <TableRow style={styles.maniHeader}>
                    <TableRowColumn colSpan='2' style={{width: '100%'}}>
                        <h4 style={styles.title}>Dynamic Seo Management</h4>
                    </TableRowColumn>
                </TableRow>
                <TableRow style={styles.maniHeader}>
                    <TableRowColumn colSpan='2'>
                        <Link to='/admin/seo/create'>
                            <RaisedButton label='Add new Seo' primary={true} style={styles.btnLeft}/>
                        </Link>
                    </TableRowColumn>
                </TableRow>
            </TableBody>
        </Table>
);
export default AdvancedSearch;
