import React from 'react'
import { Link } from 'react-router-dom'
import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn }
  from 'material-ui/Table'
import { styles } from '../../style/Styles'

const List = (props) => (
  <div>
    <Table
      fixedHeader={true}
      multiSelectable={false}
      selectable={false}
    >
      <TableHeader
        enableSelectAll={false}
        displaySelectAll={false}
        adjustForCheckbox={false}
      >
        <TableRow>
          <TableHeaderColumn tooltip="Type">Type</TableHeaderColumn>
          <TableHeaderColumn tooltip="Title">Title</TableHeaderColumn>
          <TableHeaderColumn tooltip="Keyword">Keyword</TableHeaderColumn>
          <TableHeaderColumn tooltip="Description">Description</TableHeaderColumn>
          <TableHeaderColumn tooltip="Top Description">Top Description</TableHeaderColumn>
          <TableHeaderColumn tooltip="Bottom Description">Bottom Description</TableHeaderColumn>
          <TableHeaderColumn tooltip="Action">Action</TableHeaderColumn>
        </TableRow>
      </TableHeader>
      <TableBody displayRowCheckbox={false}>
        {props.arrData.length > 0 && props.arrData.map((row, index) => (
          <TableRow key={index}>
            <TableRowColumn style={styles.removePadding}>{row.type}</TableRowColumn>
            <TableRowColumn style={styles.removePadding}>{row.title}</TableRowColumn>
            <TableRowColumn style={styles.removePadding}>{row.keyword}</TableRowColumn>
            <TableRowColumn style={styles.removePadding}>{row.description}</TableRowColumn>
            <TableRowColumn style={styles.removePadding}>{row.top_description}</TableRowColumn>
            <TableRowColumn style={styles.removePadding}>{row.bottom_description}</TableRowColumn>
            <TableRowColumn style={styles.removePadding}>
              <Link to={`/admin/seo/update?id=${row._id}`}><img src="/img/edit_24x24.png" alt="edit" /></Link>
             {/*  <Link to={`/admin/seo/delete?id=${row._id}`} onClick={(event) => { props.deleteAction(row._id, event) }} ><img src="/img/delete_24x24.png" /></Link> */}
            </TableRowColumn>
          </TableRow>
        )
        )
        }
      </TableBody>
    </Table>
  </div>
);
export default List;
