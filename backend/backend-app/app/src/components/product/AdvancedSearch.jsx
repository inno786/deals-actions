import React from 'react';
import {
    Table, TableBody, TableRow, TableRowColumn
}
    from 'material-ui/Table';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import { styles } from '../../style/Styles'

const AdvancedSearch = ({
    onSubmit,
    arrSearch,
    onChange,
    changeVendor,
    changeStatus
                        }) => (
        <form action="/" onSubmit={onSubmit}>
            <Table multiSelectable={false} selectable={false} style={{ width: '100%', float: 'left', border: 'none' }}>
                <TableBody displayRowCheckbox={false}>
                    <TableRow style={styles.maniHeader}>
                        <TableRowColumn style={{ width: '20%' }}>
                            <h4 style={styles.title}>Product Management</h4>
                        </TableRowColumn>
                        <TableRowColumn style={{ width: '70%' }}>
                            <h4 style={styles.title}>Advanced Search:</h4>
                        </TableRowColumn>
                    </TableRow>
                    <TableRow style={styles.maniHeader}>
                        <TableRowColumn colSpan='2' style={{width: '100%'}}>
                            <TextField
                                floatingLabelText="Name"
                                name="name"
                                onChange={onChange}
                                value={arrSearch.name}
                                style={styles.inputFieldAlign}
                            />
                            <SelectField
                                name="vendor"
                                value={arrSearch.vendor}
                                onChange={changeVendor}
                                floatingLabelText="Select Vendor"
                            >
                                <MenuItem value="Flipkart" key="Flipkart" primaryText="Flipkart" />
                                <MenuItem value="Amazon" key="Amazon" primaryText="Amazon" />
                                <MenuItem value="Snapdeal" key="Snapdeal" primaryText="Snapdeal" />
                            </SelectField>
                            <SelectField
                                name="status"
                                value={arrSearch.status}
                                onChange={changeStatus}
                                floatingLabelText="Select Status"
                            >
                                <MenuItem value="published" key="Publish" primaryText="Publish" />
                                <MenuItem value="inherits" key="Inherits" primaryText="Inherits" />
                                <MenuItem value="draft" key="Draft" primaryText="Draft" />
                            </SelectField>
                            <RaisedButton type="submit" label="Search" secondary={true} style={styles.searchBtn} />
                        </TableRowColumn>
                    </TableRow>
                </TableBody>
            </Table>
        </form>
    );
export default AdvancedSearch;
