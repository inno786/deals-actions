import React from 'react'
import Pagination from 'material-ui-pagination';
import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn }
  from 'material-ui/Table'
import Avatar from 'material-ui/Avatar'
import FontIcon from 'material-ui/FontIcon'

const List = ({
  arrData,
  total,
  perPageItem,
  current,
  onChangePage
}) => (
    <div>
      <Pagination
        total={total}
        current={current}
        display={perPageItem}
        onChange={onChangePage}
      />
      <Table
        fixedHeader={true}
        multiSelectable={false}
        selectable={false}
      >
        <TableHeader
          enableSelectAll={false}
          displaySelectAll={false}
          adjustForCheckbox={false}
        >
          <TableRow>
            <TableHeaderColumn tooltip="Image">Image</TableHeaderColumn>
            <TableHeaderColumn tooltip="Vendor">Vendor</TableHeaderColumn>
            <TableHeaderColumn tooltip="Name">Name</TableHeaderColumn>
            <TableHeaderColumn tooltip="Slug">Slug</TableHeaderColumn>
            <TableHeaderColumn tooltip="Price">Price</TableHeaderColumn>
            <TableHeaderColumn tooltip="Stock">Stock</TableHeaderColumn>
            <TableHeaderColumn tooltip="Status">Status</TableHeaderColumn>
            <TableHeaderColumn tooltip="Created On">Created On</TableHeaderColumn>
            <TableHeaderColumn tooltip="Updated On">Updated On</TableHeaderColumn>
          </TableRow>
        </TableHeader>
        <TableBody displayRowCheckbox={false}>
          {arrData.length > 0 && arrData.map((row, index) => (
            <TableRow key={index}>
              <TableRowColumn className="remove_padding">
                <Avatar src={row.img_200} size={60} icon={<FontIcon className="muidocs-icon-communication-voicemail" />} />
              </TableRowColumn>
              <TableRowColumn className="remove_padding">{row.vendor}</TableRowColumn>
              <TableRowColumn className="remove_padding">{row.name}</TableRowColumn>
              <TableRowColumn className="remove_padding">{row.slug}</TableRowColumn>
              <TableRowColumn className="remove_padding">{row.flipkart_special_price}</TableRowColumn>
              <TableRowColumn className="remove_padding">{row.stock > 0 ? "Available" : "Not Available"}</TableRowColumn>
              <TableRowColumn className="remove_padding">{row.status}</TableRowColumn>
              <TableRowColumn className="remove_padding">{row.created_on}</TableRowColumn>
              <TableRowColumn className="remove_padding">{row.updated_on}</TableRowColumn>
            </TableRow>
          )
          )
          }
          <TableRow>
            <TableRowColumn colSpan='9'>
              <Pagination
                total={total}
                current={current}
                display={perPageItem}
                onChange={onChangePage}
              />
            </TableRowColumn>
          </TableRow>
        </TableBody>
      </Table>
    </div>
  );
export default List;
