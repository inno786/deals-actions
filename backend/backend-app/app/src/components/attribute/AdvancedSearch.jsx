import React from 'react';
import { Link } from 'react-router-dom';
import {
    Table, TableBody, TableRow, TableRowColumn
}
    from 'material-ui/Table';
import TextField from 'material-ui/TextField';
import Checkbox from 'material-ui/Checkbox';
import RaisedButton from 'material-ui/RaisedButton';
import { styles } from '../../style/Styles'

const AdvancedSearch = ({
    arrSearch,
    onChange,
    onSubmit
                        }) => (
        <form action="/" onSubmit={onSubmit}>
            <Table multiSelectable={false} selectable={false} style={{ width: '100%', float: 'left', border: 'none' }}>
                <TableBody displayRowCheckbox={false}>
                    <TableRow style={styles.maniHeader}>
                        <TableRowColumn style={{ width: '20%' }}>
                            <h4 style={styles.title}>Attribute Management</h4>
                        </TableRowColumn>
                        <TableRowColumn style={{ width: '70%' }}>
                            <h4 style={styles.title}>Advanced Search:</h4>
                        </TableRowColumn>
                    </TableRow>
                    <TableRow style={styles.maniHeader}>
                        <TableRowColumn style={{ width: '20%' }}>
                            <Link to='/admin/attribute/create'>
                                <RaisedButton label='Add new attribute' primary={true} style={styles.btnLeft} />
                            </Link>
                        </TableRowColumn>
                        <TableRowColumn style={{ width: '70%' }}>
                            <TextField
                                floatingLabelText="Name"
                                name="name"
                                onChange={onChange}
                                value={arrSearch.name}
                                className='input-field-align'
                            />
                            <Checkbox
                                label="Active"
                                name="status"
                                className='checkbox-align'
                                defaultChecked={arrSearch.status ? true : false}
                                onCheck={onChange}
                            />
                            <RaisedButton type="submit" label="Search" secondary={true} style={styles.searchBtn} />
                        </TableRowColumn>
                    </TableRow>
                </TableBody>
            </Table>
        </form>
    );
export default AdvancedSearch;
