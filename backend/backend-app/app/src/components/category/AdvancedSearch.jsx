import React from 'react';
import {Link} from 'react-router-dom';
import {
    Table, TableBody, TableRow, TableRowColumn
}
    from 'material-ui/Table';
import TextField from 'material-ui/TextField';
import Checkbox from 'material-ui/Checkbox';
import RaisedButton from 'material-ui/RaisedButton';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import {styles} from '../../style/Styles'

const AdvancedSearch = ({
                            categorySearch,
                            onChange,
                            changeSelectField,
                            onSubmit
                        }) => (
    <form action="/" onSubmit={onSubmit}>
        <Table multiSelectable={false} selectable={false} style={{width: '100%', float: 'left', border: 'none'}}>
            <TableBody displayRowCheckbox={false}>
                <TableRow style={styles.maniHeader}>
                    <TableRowColumn style={{width: '20%'}}>
                        <h4 style={styles.title}>Category Management</h4>
                    </TableRowColumn>
                    <TableRowColumn style={{width: '70%'}}>
                        <h4 style={styles.title}>Advanced Search:</h4>
                    </TableRowColumn>
                </TableRow>
                <TableRow style={styles.maniHeader}>
                    <TableRowColumn style={{width: '20%'}}>
                        <Link to='/admin/category/create'>
                            <RaisedButton label='Add new category' primary={true} style={styles.btnLeft}/>
                        </Link>
                    </TableRowColumn>
                    <TableRowColumn style={{width: '70%'}}>
                        <TextField
                            floatingLabelText="Name"
                            name="name"
                            onChange={onChange}
                            value={categorySearch.name}
                            style={styles.inputFieldAlign}
                        />
                        <SelectField
                            name="source"
                            value={categorySearch.source}
                            onChange={changeSelectField}
                            floatingLabelText="Select Source"
                        >
                            <MenuItem value="" key="Defailt" primaryText="Select vendor"/>
                            <MenuItem value="Flipkart" key="Flipkart" primaryText="Flipkart"/>
                            <MenuItem value="Amazon" key="Amazon" primaryText="Amazon"/>
                            <MenuItem value="Snapdeal" key="Snapdeal" primaryText="Snapdeal"/>
                        </SelectField>
                        <Checkbox
                            label="Active"
                            name="status"
                            style={styles.checkboxAlign}
                            defaultChecked={categorySearch.status ? true : false}
                            onCheck={onChange}
                        />
                        <Checkbox
                            label="Is Main Category"
                            name="main_category"
                            style={styles.checkboxAlign}
                            defaultChecked={categorySearch.main_category ? true : false}
                            onCheck={onChange}
                        />
                        <RaisedButton type="submit" label="Search" secondary={true} style={styles.searchBtn}/>
                    </TableRowColumn>
                </TableRow>
            </TableBody>
        </Table>
    </form>
);
export default AdvancedSearch;
