import React from 'react'
import { Link } from 'react-router-dom'
import Pagination from 'material-ui-pagination'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn}
  from 'material-ui/Table'
  import Avatar from 'material-ui/Avatar'
  import FontIcon from 'material-ui/FontIcon'
import {styles} from '../../style/Styles'

const List = ({
  deleteAction,
  message,
  arrData,
  imgRoot,
  total,
  perPageItem,
  current,
  onChangePage
}) => (
  <div>
    <Pagination
       total = {total}
       current ={current}
       display = {perPageItem}
       onChange ={onChangePage}
     />
  <Table
    fixedHeader={true}
    multiSelectable={false}
     selectable={false}
   >
   <TableHeader
    enableSelectAll={false}
    displaySelectAll={false}
    adjustForCheckbox={false}
    >
    <TableRow>
      <TableHeaderColumn tooltip="Image">Image</TableHeaderColumn>
      <TableHeaderColumn tooltip="Name">Name</TableHeaderColumn>
      <TableHeaderColumn tooltip="Slug">Slug</TableHeaderColumn>
      <TableHeaderColumn tooltip="Description">Description</TableHeaderColumn>
      <TableHeaderColumn tooltip="Main Category">Main Category</TableHeaderColumn>
      <TableHeaderColumn tooltip="Source">Source</TableHeaderColumn>
      <TableHeaderColumn tooltip="Status">Status</TableHeaderColumn>
      <TableHeaderColumn tooltip="Created On">Created On</TableHeaderColumn>
      <TableHeaderColumn tooltip="Updated On">Updated On</TableHeaderColumn>
      <TableHeaderColumn tooltip="Action">Action</TableHeaderColumn>
    </TableRow>
  </TableHeader>
  <TableBody displayRowCheckbox={false}>
   {arrData.length >0 && arrData.map( (row, index) => (
      <TableRow key={index}>
        <TableRowColumn style={styles.removePadding}>
          <Avatar src={imgRoot+row.image} size={60} icon={<FontIcon className="muidocs-icon-communication-voicemail" />} />
        </TableRowColumn>
        <TableRowColumn style={styles.removePadding}>{row.name}</TableRowColumn>
        <TableRowColumn style={styles.removePadding}>{row.slug}</TableRowColumn>
        <TableRowColumn style={styles.removePadding}>{row.description}</TableRowColumn>
        <TableRowColumn style={styles.removePadding}>{row.main_category >0 ? "Yes":"No"}</TableRowColumn>
        <TableRowColumn style={styles.removePadding}>{row.source}</TableRowColumn>
        <TableRowColumn style={styles.removePadding}>{row.status}</TableRowColumn>
        <TableRowColumn style={styles.removePadding}>{row.created_on}</TableRowColumn>
        <TableRowColumn style={styles.removePadding}>{row.updated_on}</TableRowColumn>
        <TableRowColumn style={styles.removePadding}>
          <Link to={`/admin/category/update?id=${row._id}`}><img src="/img/edit_24x24.png" alt="edit" /></Link>
            {/* <Link to={`/admin/category/delete?id=${row._id}`}><img src="/img/delete_24x24.png" /></Link> */}
        </TableRowColumn>
     </TableRow>
     )
   )
 }
 <TableRow>
    <TableRowColumn  colSpan='10'>
      <Pagination
         total = {total}
         current ={current}
         display = {perPageItem}
         onChange ={onChangePage}
       />
    </TableRowColumn>
 </TableRow>
    </TableBody>
  </Table>
</div>
   );
export default List;
