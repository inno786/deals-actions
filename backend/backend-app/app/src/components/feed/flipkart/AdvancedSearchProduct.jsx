import React from 'react';
import { Table, TableBody, TableRow, TableRowColumn }
    from 'material-ui/Table';
import RaisedButton from 'material-ui/RaisedButton';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import { styles } from '../../../style/Styles'

const AdvancedSearchProduct = (props) => (
        <form action="/" onSubmit={props.onSubmit}>
            <Table multiSelectable={false} selectable={false} style={{ width: '100%', float: 'left', border: 'none' }}>
                <TableBody displayRowCheckbox={false}>
                    <TableRow style={styles.maniHeader}>
                        <TableRowColumn style={{ width: '20%' }}>
                            <h4 style={styles.title}>Flipkart Category Product</h4>
                        </TableRowColumn>
                        <TableRowColumn style={{ width: '70%' }}>
                            <h4 style={styles.title}>Advanced Search:</h4>
                        </TableRowColumn>
                    </TableRow>
                    <TableRow style={styles.maniHeader}>
                        <TableRowColumn colSpan={2} style={{ width: '100%' }}>
                        <div style={{width:'40%'}}>
                            <SelectField
                                name="resource"
                                value={props.arrSearch.resource}
                                onChange={props.changeCategory}
                                floatingLabelText="Select Category"
                            >
                                {
                                    props.dropDownCatList.length > 0 ? (props.dropDownCatList.map((row, index) => <MenuItem value={row.resource} key={row.resource} primaryText={row.name} />)) : (<MenuItem value="" key="" primaryText="No Item" />)
                                }
                            </SelectField>
                           
                            <RaisedButton type="submit" label="Search" secondary={true} style={styles.searchBtn} />
                            </div>
                        </TableRowColumn>
                    </TableRow>
                </TableBody>
            </Table>
        </form>
    );
export default AdvancedSearchProduct;
