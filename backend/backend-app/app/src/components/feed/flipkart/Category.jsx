import React from 'react'
import Pagination from 'material-ui-pagination';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn}
  from 'material-ui/Table';
  import FontIcon from 'material-ui/FontIcon';
  import Checkbox from 'material-ui/Checkbox';
  import SelectField from 'material-ui/SelectField';
  import MenuItem from 'material-ui/MenuItem';
  import {blue500} from 'material-ui/styles/colors';
import {styles} from '../../../style/Styles'

const Category = ({
  arrData,
  getProducts,
  removeProducts,
  checkAll,
  clickCheckbox,
  onChangeStatus,
  changeStatus,
  total,
  perPageItem,
  current,
  onChangePage
}) => (
  <div>
    <Pagination
       total = {total}
       current ={current}
       display = {perPageItem}
       onChange ={onChangePage}
     />
     <Table
     fixedHeader={true}
     multiSelectable={false}
      selectable={false}
    >
    <TableHeader
     enableSelectAll={false}
     displaySelectAll={false}
     adjustForCheckbox={false}
     >
     <TableRow>
       <TableHeaderColumn tooltip="Select All" style={styles.chbWidth}>
       <Checkbox
       label=""
       id="checkAll"
       onClick={checkAll}
        />
       </TableHeaderColumn>
       <TableHeaderColumn tooltip="Name" style={styles.nameWidth}>Name</TableHeaderColumn>
       <TableHeaderColumn tooltip="Status" style={styles.statusWidth}>Status</TableHeaderColumn>
       <TableHeaderColumn tooltip="CreatedOn" style={styles.createdWidth}>CreatedOn</TableHeaderColumn>
       <TableHeaderColumn tooltip="UpdatedOn" style={styles.updatedWidth}>UpdatedOn</TableHeaderColumn>
       <TableHeaderColumn tooltip="Action" style={styles.actionWidth}>Action</TableHeaderColumn>
     </TableRow>
   </TableHeader>
   <TableBody displayRowCheckbox={false}>
    {arrData.length >0 && arrData.map( (row, index) => (
       <TableRow key={index}>
         <TableRowColumn style={styles.chbWidth}>
           <Checkbox
           name={`chb_${row.resource}`}
           id={`chb_${row.resource}`}
           label=""
           className="checkBoxClass"
           defaultChecked={false}
           onCheck={ (event) => {clickCheckbox(`${row.resource}`, event) } }
           />
         </TableRowColumn>
         <TableRowColumn style={styles.nameWidth}>{row.name}</TableRowColumn>
         <TableRowColumn style={styles.statusWidth}>{row.status}</TableRowColumn>
         <TableRowColumn style={styles.createdWidth}>{row.created_on}</TableRowColumn>
         <TableRowColumn style={styles.updatedWidth}>{row.updated_on}</TableRowColumn>
         <TableRowColumn style={styles.actionWidth}>
           <FontIcon style={styles.iconStyles} color={blue500} onClick={() => { removeProducts(row._id)}}>Remove Products</FontIcon>
           <FontIcon style={styles.iconStyles} color={blue500} onClick={() => { getProducts(row._id)}}>Get Products</FontIcon>
         </TableRowColumn>
      </TableRow>
      )
    )
  }
 <TableRow>
     <TableRowColumn  colSpan='6'>
       <SelectField
          name="change_status"
          value={changeStatus.resource}
          onChange={onChangeStatus}
          floatingLabelText="Select Status"
        >
        <MenuItem value="ready" key="ready" primaryText="Ready" />
        <MenuItem value="inprogress" key="inprogress" primaryText="Inprogress" />
        <MenuItem value="complete" key="complete" primaryText="Complete" />
        <MenuItem value="inactive" key="inactive" primaryText="Inactive" />
        </SelectField>
     </TableRowColumn>
  </TableRow>
 
  <TableRow>
     <TableRowColumn  colSpan='6'>
       <Pagination
          total = {total}
          current ={current}
          display = {perPageItem}
          onChange ={onChangePage}
        />
     </TableRowColumn>
  </TableRow>
     </TableBody>
   </Table>
</div>
   );
export default Category;
