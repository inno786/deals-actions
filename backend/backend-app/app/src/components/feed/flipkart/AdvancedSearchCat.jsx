import React from 'react';
import { Table, TableBody, TableRow, TableRowColumn }
    from 'material-ui/Table';
import RaisedButton from 'material-ui/RaisedButton';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import { styles } from '../../../style/Styles'

const AdvancedSearchCat = ({
    onSubmit,
    arrSearch,
    changeCategory,
    changeStatus,
    dropDownCatList,
    getCategory,
    clearData
}) => (
        <form action="/" onSubmit={onSubmit}>
            <Table multiSelectable={false} selectable={false} style={{ width: '100%', float: 'left', border: 'none' }}>
                <TableBody displayRowCheckbox={false}>
                    <TableRow style={styles.maniHeader}>
                        <TableRowColumn style={{ width: '20%' }}>
                            <h4 style={styles.title}>Flipkart Category</h4>
                        </TableRowColumn>
                        <TableRowColumn style={{ width: '70%' }}>
                            <h4 style={styles.title}>Advanced Search:</h4>
                        </TableRowColumn>
                    </TableRow>
                    <TableRow style={styles.maniHeader}>
                        <TableRowColumn style={{ width: '50%' }}>
                            <RaisedButton 
                            label='Get Category'
                            primary={true} 
                            style={styles.btnLeft} 
                            onClick={getCategory} 
                            />
                        </TableRowColumn>
                        <TableRowColumn style={{ width: '40%' }}>
                            <SelectField
                                name="resource"
                                value={arrSearch.resource}
                                onChange={changeCategory}
                                floatingLabelText="Select Category"
                            >
                                {
                                    dropDownCatList.length > 0 ? (dropDownCatList.map((row, index) => <MenuItem value={row.resource} key={row.resource} primaryText={row.name} />)) : (<MenuItem value="" key="" primaryText="No Item" />)
                                }
                            </SelectField>
                            <SelectField
                                name="status"
                                value={arrSearch.status}
                                onChange={changeStatus}
                                floatingLabelText="Select Status"
                            >
                                <MenuItem value="ready" key="ready" primaryText="Ready" />
                                <MenuItem value="inprogress" key="inprogress" primaryText="Inprogress" />
                                <MenuItem value="complete" key="complete" primaryText="Complete" />
                                <MenuItem value="inactive" key="inactive" primaryText="Inactive" />
                            </SelectField>
                            <RaisedButton type="submit" label="Search" secondary={true} style={styles.searchBtn} />
                        </TableRowColumn>
                    </TableRow>
                    <TableRow>
                        <TableRowColumn colspan={2}>
                        <RaisedButton 
                          label='Clear Data' 
                          primary={true} 
                          style={styles.btnRight} 
                          onClick={clearData} 
                          />
                        </TableRowColumn>
                    </TableRow>
                </TableBody>
            </Table>
        </form>
    );
export default AdvancedSearchCat;
