import React from 'react'
import Pagination from 'material-ui-pagination';
import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn }
  from 'material-ui/Table';
import Checkbox from 'material-ui/Checkbox';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import { styles } from '../../../style/Styles'

const Product = (props) => (
  <div>
    <Pagination
      total={props.pagging.total}
      current={props.pagging.current}
      display={props.pagging.display}
      onChange={props.onChangePage}
    />
    <Table
      fixedHeader={true}
      multiSelectable={false}
      selectable={false}
    >
      <TableHeader
        enableSelectAll={false}
        displaySelectAll={false}
        adjustForCheckbox={false}
      >
        <TableRow>
          <TableHeaderColumn tooltip="Select All" style={styles.chbWidth}>
            <Checkbox
              label=""
              id="checkAll"
              onClick={props.checkAll}
            />
          </TableHeaderColumn>
          <TableHeaderColumn tooltip="Name" style={styles.nameWidth}>Name</TableHeaderColumn>
          <TableHeaderColumn tooltip="Status" style={styles.statusWidth}>Status</TableHeaderColumn>
          <TableHeaderColumn tooltip="CreatedOn" style={styles.createdWidth}>CreatedOn</TableHeaderColumn>
          <TableHeaderColumn tooltip="UpdatedOn" style={styles.updatedWidth}>UpdatedOn</TableHeaderColumn>
        </TableRow>
      </TableHeader>
      <TableBody displayRowCheckbox={false}>
        {props.arrData.length > 0 && props.arrData.map((row, index) => (
          <TableRow key={index}>
            <TableRowColumn style={styles.chbWidth}>
              <Checkbox
                name={`chb_${row._id}`}
                id={`chb_${row._id}`}
                label=""
                className="checkBoxClass"
                defaultChecked={false}
                onCheck={(event) => { props.clickCheckbox(`${row._id}`, event) }}
              />
            </TableRowColumn>
            <TableRowColumn style={styles.nameWidth}>{row.name}</TableRowColumn>
            <TableRowColumn style={styles.statusWidth}>{row.status.replace(/\b[a-z]/g, function (f) {
              return f.toUpperCase();
          })}</TableRowColumn>
            <TableRowColumn style={styles.createdWidth}>{row.created_on}</TableRowColumn>
            <TableRowColumn style={styles.updatedWidth}>{row.updated_on}</TableRowColumn>
          </TableRow>
        )
        )
        }
        <TableRow>
          <TableRowColumn colSpan='5'>
            <SelectField
              name="change_status"
              value={props.changeStatus.resource}
              onChange={props.changeCatProd}
              floatingLabelText="Select Status"
            >
              <MenuItem value="ready" key="ready" primaryText="Ready" />
              <MenuItem value="inprogress" key="inprogress" primaryText="Inprogress" />
              <MenuItem value="complete" key="complete" primaryText="Complete" />
              <MenuItem value="inactive" key="inactive" primaryText="Inactive" />
              <MenuItem value="delete" key="delete" primaryText="Delete" />
            </SelectField>
          </TableRowColumn>
        </TableRow>

        <TableRow>
          <TableRowColumn colSpan='5'>
            <Pagination
              total={props.pagging.total}
              current={props.pagging.current}
              display={props.pagging.display}
              onChange={props.onChangePage}
            />
          </TableRowColumn>
        </TableRow>
      </TableBody>
    </Table>
  </div>
);
export default Product;
