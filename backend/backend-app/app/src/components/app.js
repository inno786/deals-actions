import React from 'react'
import {
    BrowserRouter as Router,
    Route,
    Switch
} from 'react-router-dom'
import { Provider } from 'react-redux'

// Container load
import Login from '../containers/login/Content'
import Dashboard from '../containers/dashboard/Content'
import CategoryList from '../containers/category/List'
import CategoryCreate from '../containers/category/Create'
import CategoryUpdate from '../containers/category/Update'
import SubCategoryList from '../containers/subcategory/List'
import SubCategoryCreate from '../containers/subcategory/Create'
import SubCategoryUpdate from '../containers/subcategory/Update'
import AttributeList from '../containers/attribute/List'
import AttributeCreate from '../containers/attribute/Create'
import AttributeUpdate from '../containers/attribute/Update'
import MapCategoryList from '../containers/map_category/List'
import MapCategoryCreate from '../containers/map_category/Create'
import ProductList from '../containers/product/List'
import FlipkartCategory from '../containers/feed/flipkart/Category'
import FlipkartProduct from '../containers/feed/flipkart/Product'
import SeoList from '../containers/seo/List'
import SeoCreate from '../containers/seo/Create'
import SeoUpdate from '../containers/seo/Update'
import CreateUpdate from '../containers/home_seo/CreateUpdate'

// Auth token
import RequireAuth from '../containers/RequireAuth'

const App = (props) => {
    return (
        <Provider store={props.store}>
            <Router>
                <Switch>
                    <Route exact path="/admin" component={Login} />
                    <Route exact path="/admin/dashboard" component={RequireAuth(Dashboard)} />
                    <Route exact path="/admin/category" component={RequireAuth(CategoryList)} />
                    <Route exact path="/admin/category/create" component={RequireAuth(CategoryCreate)} />
                    <Route exact path="/admin/category/update" component={RequireAuth(CategoryUpdate)} />
                    <Route exact path="/admin/subcategory" component={RequireAuth(SubCategoryList)} />
                    <Route exact path="/admin/subcategory/create" component={RequireAuth(SubCategoryCreate)} />
                    <Route exact path="/admin/subcategory/update" component={RequireAuth(SubCategoryUpdate)} />
                    <Route exact path="/admin/attribute" component={RequireAuth(AttributeList)} />
                    <Route exact path="/admin/attribute/create" component={RequireAuth(AttributeCreate)} />
                    <Route exact path="/admin/attribute/update" component={RequireAuth(AttributeUpdate)} />
                    <Route exact path="/admin/map-category" component={RequireAuth(MapCategoryList)} />
                    <Route exact path="/admin/map-category/create" component={RequireAuth(MapCategoryCreate)} />
                    <Route exact path="/admin/product" component={RequireAuth(ProductList)} />
                    <Route exact path="/admin/feed/flipkart/category" component={RequireAuth(FlipkartCategory)} />
                    <Route exact path="/admin/feed/flipkart/product" component={RequireAuth(FlipkartProduct)} />
                    <Route exact path="/admin/seo" component={RequireAuth(SeoList)} />
                    <Route exact path="/admin/seo/create" component={RequireAuth(SeoCreate)} />
                    <Route exact path="/admin/seo/update" component={RequireAuth(SeoUpdate)} />
                    <Route exact path="/admin/homeSeo" component={RequireAuth(CreateUpdate)} />
                    <Route render={() => <h1>Page not found</h1>} />
                </Switch>
            </Router>
        </Provider>
    );
}
export default App;
