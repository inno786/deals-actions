import React from 'react';
import {Link} from 'react-router-dom';
import {
    Table, TableBody, TableRow, TableRowColumn
}
    from 'material-ui/Table';
import TextField from 'material-ui/TextField';
import Checkbox from 'material-ui/Checkbox';
import RaisedButton from 'material-ui/RaisedButton';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import {styles} from '../../style/Styles'

const AdvancedSearch = () => (
   <Table multiSelectable={false} selectable={false} style={{width: '100%', float: 'left', border: 'none'}}>
            <TableBody displayRowCheckbox={false}>
                <TableRow style={styles.maniHeader}>
                    <TableRowColumn colSpan='2' style={{width: '100%'}}>
                        <h4 style={styles.title}>Dynamic Seo Management</h4>
                    </TableRowColumn>
                </TableRow>
                <TableRow style={styles.maniHeader}>
                    <TableRowColumn colSpan='2'>
                        <Link to='/admin/seo/create'>
                            <RaisedButton label='Add new Seo' primary={true} style={styles.btnLeft}/>
                        </Link>
                    </TableRowColumn>
                </TableRow>
            </TableBody>
        </Table>
);
export default AdvancedSearch;
