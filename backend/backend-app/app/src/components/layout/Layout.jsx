import React from 'react';
import { Link } from 'react-router-dom'
import Paper from 'material-ui/Paper';
import { List, ListItem } from 'material-ui/List';


const styles = {
    main: {
        display: 'flex',
        flexDirection: 'column',
        minHeight: '100vh',
    },
    body: {
        display: 'flex',
        flex: 1,
        backgroundColor: '#edecec',
        overflow: 'hidden',
    },
    content: {
        flex: 1,
    },
    menu: {
        open: {
            flex: '0 0 16em',
            order: -1,
            transition: 'margin 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
            marginLeft: 0,
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'space-between',
        },
        closed: {
            flex: '0 0 16em',
            order: -1,
            transition: 'margin 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
            marginLeft: '-16em',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'space-between',
        },
    },
};

class Layout extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.state ={
            open:false
        }
    }
    handleClick = () => {
        this.setState({ open: !this.state.open });
      };

    render() {
       /*  const { classes } = this.props; */
        return (
    <table className="table_default">
        <tbody>
            <tr>
                <td>
                    <Paper style={styles.menu.open}>
                        <List>
                            <ListItem
                                key='category'
                                containerElement={<Link to='/admin/category' />}
                                primaryText='Category'
                            />
                            <ListItem
                                key='subcategory'
                                containerElement={<Link to='/admin/subcategory' />}
                                primaryText='Sub Category'
                            />
                            <ListItem
                                key='attribute'
                                containerElement={<Link to='/admin/attribute' />}
                                primaryText='Attribute'
                            />
                            <ListItem
                                key='map-category'
                                containerElement={<Link to='/admin/map-category' />}
                                primaryText='Mapping Category'
                            />
                            <ListItem
                                key='Home-seo'
                                containerElement={<Link to='/admin/homeSeo' />}
                                primaryText='Home Seo'
                            /> 
                            <ListItem
                                key='seo'
                                containerElement={<Link to='/admin/seo' />}
                                primaryText='Dynamic Seo'
                            /> 
                            {/*<ListItem
                            key='subcategory_attribute'
                            containerElement={<Link to='/admin/subcatAttr'/>}
                            primaryText='Subcategory Attribute'
                        />
                        <ListItem
                            key='attribute_mapping'
                            containerElement={<Link to='/admin/attrMapping'/>}
                            primaryText='Attribute Mapping'
                         /> */}
                            <ListItem
                                key='products'
                                containerElement={<Link to='/admin/product' />}
                                primaryText='Products'
                            />
                            <ListItem
                                key='feed-category'
                                containerElement={<Link to='/admin/feed/flipkart/category' />}
                                primaryText='Feed Category'
                            />
                            <ListItem
                                key='feed-product'
                                containerElement={<Link to='/admin/feed/flipkart/product' />}
                                primaryText='Feed Product'
                            />                            
                        </List>
                    </Paper>
                </td>
            </tr>
        </tbody>
    </table>
 )}
}

export default Layout;
