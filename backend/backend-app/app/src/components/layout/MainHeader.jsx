import React from 'react';
import { Card, CardHeader } from 'material-ui/Card';
import Avatar from 'material-ui/Avatar';
import LightBulbIcon from 'material-ui/svg-icons/action/lightbulb-outline';
import Notifications from 'react-notify-toast';

const styles = {
    main: {
      backgroundColor:'#00bcd4'
    }
};

const MainHeader = () => (
  <div>
  <Card style={styles.main}>
       <CardHeader
         title={'Admin Panel'}
         avatar={<Avatar backgroundColor="#FFEB3B" icon={<LightBulbIcon />} />}
     />
   </Card>
   <Notifications />
   </div>
);
export default MainHeader;
