import React from 'react'
import Pagination from 'material-ui-pagination'
import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn }
  from 'material-ui/Table'
import Avatar from 'material-ui/Avatar'
import FontIcon from 'material-ui/FontIcon'
import { styles } from '../../style/Styles'

const List = ({
  arrData,
  imgRoot,
  total,
  perPageItem,
  current,
  onChangePage,
  message,
  deleteAction
}) => (
    <div>
      <Pagination
        total={total}
        current={current}
        display={perPageItem}
        onChange={onChangePage}
      />
      <Table
        fixedHeader={true}
        multiSelectable={false}
        selectable={false}
      >
        <TableHeader
          enableSelectAll={false}
          displaySelectAll={false}
          adjustForCheckbox={false}
        >
          <TableRow>
            <TableHeaderColumn tooltip="Image">Image</TableHeaderColumn>
            <TableHeaderColumn tooltip="Name">Name</TableHeaderColumn>
            <TableHeaderColumn tooltip="Parent Category">Category</TableHeaderColumn>
            <TableHeaderColumn tooltip="Feed Parent Category">Feed Parent</TableHeaderColumn>
            <TableHeaderColumn tooltip="Source">Source</TableHeaderColumn>
            <TableHeaderColumn tooltip="Status">Status</TableHeaderColumn>
            <TableHeaderColumn tooltip="Created On">Created On</TableHeaderColumn>
            <TableHeaderColumn tooltip="Updated On">Updated On</TableHeaderColumn>
          </TableRow>
        </TableHeader>
        <TableBody displayRowCheckbox={false}>
          {arrData.length > 0 && arrData.map((row, index) => (
            <TableRow key={index}>
              <TableRowColumn style={styles.removePadding}>
                <Avatar src={imgRoot + row.image} size={60} icon={<FontIcon className="muidocs-icon-communication-voicemail" />} />
              </TableRowColumn>
              <TableRowColumn style={styles.removePadding}>{row.fk_sub_category_name}</TableRowColumn>
              <TableRowColumn style={styles.removePadding}>{row.category}</TableRowColumn>
              <TableRowColumn style={styles.removePadding}>{row.fk_category_parent_id}</TableRowColumn>
              <TableRowColumn style={styles.removePadding}>{row.source}</TableRowColumn>
              <TableRowColumn style={styles.removePadding}>{row.status}</TableRowColumn>
              <TableRowColumn style={styles.removePadding}>{row.created_on}</TableRowColumn>
              <TableRowColumn style={styles.removePadding}>{row.updated_on}</TableRowColumn>
            </TableRow>
          )
          )
          }
          <TableRow>
            <TableRowColumn colSpan='8'>
              <Pagination
                total={total}
                current={current}
                display={perPageItem}
                onChange={onChangePage}
              />
            </TableRowColumn>
          </TableRow>
        </TableBody>
      </Table>
    </div>
  );
export default List;
