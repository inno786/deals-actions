import React from 'react';
import { Link } from 'react-router-dom';
import { Table, TableBody, TableRow, TableRowColumn }
    from 'material-ui/Table';
import RaisedButton from 'material-ui/RaisedButton';
import Checkbox from 'material-ui/Checkbox';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Avatar from 'material-ui/Avatar';
import { styles } from '../../style/Styles'

const Forms = ({
    onSubmit,
    onChange,
    onChangeFkCat,
    onChangeSource,
    catList,
    arrData,
    subCategoryList,
    onChangeSubCategory,
    errors,
    mainTitle,
    btnText
}) => (
        <form action="/" onSubmit={onSubmit}>
            <Table multiSelectable={false} selectable={false} style={{ width: '100%', float: 'left', border: 'none' }}>
                <TableBody displayRowCheckbox={false}>
                    <TableRow style={styles.maiHeader}>
                        <TableRowColumn style={{ width: '50%' }}>
                            <h3>{mainTitle}</h3>
                        </TableRowColumn>
                        <TableRowColumn style={{ width: '50%', textAlign: 'right' }}>
                            <Link to='/admin/map-category'>
                                <Avatar src="/img/back-btn.png" size={40} />
                            </Link>
                        </TableRowColumn>
                    </TableRow>
                    <TableRow>
                        <TableRowColumn colSpan='2'>
                            <Table multiSelectable={false} selectable={false} style={{ width: '100%', float: 'left', border: 'none' }}>
                                <TableBody displayRowCheckbox={false}>
                                    <TableRow displayBorder={false}>
                                        <TableRowColumn>
                                            <SelectField
                                                name="fk_category_id"
                                                value={arrData.fk_category_id}
                                                errorText={errors.fk_category_id}
                                                onChange={onChangeFkCat}
                                                floatingLabelText="Select Category"
                                            >
                                                {
                                                    catList.length > 0 ? (catList.map((row, index) => <MenuItem value={row._id} key={row._id} primaryText={row.name} />)) : (<MenuItem value="" key="" primaryText="No Item in category" />)
                                                }
                                            </SelectField>
                                        </TableRowColumn>
                                    </TableRow>
                                    <TableRow displayBorder={false}>
                                        <TableRowColumn>
                                            <SelectField
                                                name="source"
                                                value={arrData.source}
                                                errorText={errors.source}
                                                onChange={onChangeSource}
                                                floatingLabelText="Select Source"
                                            >
                                                <MenuItem value="Custom" key="Custom" primaryText="Custom"/>
                                                <MenuItem value="Flipkart" key="Flipkart" primaryText="Flipkart" />
                                                <MenuItem value="Amazon" key="Amazon" primaryText="Amazon" />
                                                <MenuItem value="Snapdeal" key="Snapdeal" primaryText="Snapdeal" />
                                            </SelectField>
                                        </TableRowColumn>
                                    </TableRow>
                                    <TableRow displayBorder={false}>
                                        <TableRowColumn>
                                            {
                                                subCategoryList && (subCategoryList.map((row, index) =>
                                                    <Checkbox
                                                        key={row._id}
                                                        id={row._id}
                                                        label={row.name}
                                                        name={row.name}
                                                        style={styles.checkboxAlign}
                                                        defaultChecked={row.mapped}
                                                        onCheck={onChange}
                                                    />
                                                ))
                                            }
                                        </TableRowColumn>
                                    </TableRow>
                                </TableBody>
                            </Table>
                        </TableRowColumn>
                    </TableRow>
                    <TableRow>
                        <TableRowColumn colSpan='2' style={{ textAlign: 'center' }}>
                            <RaisedButton type="submit" label={btnText} secondary={true} style={{ textAlign: 'center' }} />
                        </TableRowColumn>
                    </TableRow>
                </TableBody>
            </Table>
        </form>
    );
export default Forms;
