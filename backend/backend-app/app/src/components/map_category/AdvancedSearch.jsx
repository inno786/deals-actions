import React from 'react';
import { Link } from 'react-router-dom';
import {
    Table, TableBody, TableRow, TableRowColumn
}
    from 'material-ui/Table';
import TextField from 'material-ui/TextField';
import Checkbox from 'material-ui/Checkbox';
import RaisedButton from 'material-ui/RaisedButton';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import { styles } from '../../style/Styles'

const AdvancedSearch = ({
    categorySearch,
    onChange,
    onChangeSelect,
    onSubmit,
    catList
    }) => (
        <form action="/" onSubmit={onSubmit}>
            <Table multiSelectable={false} selectable={false} style={{ width: '100%', float: 'left', border: 'none' }}>
                <TableBody displayRowCheckbox={false}>
                    <TableRow style={styles.maniHeader}>
                        <TableRowColumn style={{ width: '20%' }}>
                            <h4 style={styles.title}>>Mapping Category</h4>
                        </TableRowColumn>
                        <TableRowColumn style={{ width: '70%' }}>
                            <h4 style={styles.title}>Advanced Search:</h4>
                        </TableRowColumn>
                    </TableRow>
                    <TableRow style={styles.maniHeader}>
                        <TableRowColumn style={{ width: '20%' }}>
                            <Link to='/admin/map-category/create'>
                                <RaisedButton label='Add new category' primary={true} style={styles.btnLeft} />
                            </Link>
                        </TableRowColumn>
                        <TableRowColumn style={{ width: '70%' }}>
                            <SelectField
                                name="fk_category_id"
                                value={categorySearch.fk_category_id}
                                onChange={onChangeSelect}
                                floatingLabelText="Select Category"
                                style={{ float: 'left', marginRight: '10px' }}
                            >
                                {
                                    catList.length > 0 ? (catList.map((row, index) => <MenuItem value={row._id} key={row._id} primaryText={row.name} />)) : (<MenuItem value="" key="" primaryText="No Item in category" />)
                                }
                            </SelectField>
                            <TextField
                                floatingLabelText="Name"
                                name="fk_sub_category_name"
                                onChange={onChange}
                                value={categorySearch.fk_sub_category_name}
                                className='input-field-align'
                            />
                            <Checkbox
                                label="Active"
                                name="status"
                                className='checkbox-align'
                                defaultChecked={categorySearch.status}
                                onCheck={onChange}
                            />
                            <RaisedButton type="submit" label="Search" secondary={true} style={styles.searchBtn} />
                        </TableRowColumn>
                    </TableRow>
                </TableBody>
            </Table>
        </form>
    );
export default AdvancedSearch;
