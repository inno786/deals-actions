
import { applyMiddleware, createStore } from 'redux';
import { createLogger } from 'redux-logger'
import thunk from 'redux-thunk'
import rootReducer from './reducers';
const middleware = [ thunk ];
// if (process.env.NODE_ENV == 'production') {
//   middleware.push(createLogger());
// }
  middleware.push(createLogger());

export default function configureStore() {
    return createStore(
        rootReducer,
        applyMiddleware(...middleware)
    );
}
