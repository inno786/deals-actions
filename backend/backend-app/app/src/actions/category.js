
import * as types from '../constants/ActionTypes'
import FileUploads from '../modules/FileUpload'

export const onChange = (event, index, value) => (dispatch) => {
    dispatch(onChangeDispatch({
        type: types.ON_CHANGE_FIELD,
        inputType: event.target.type,
        field: event.target.name,
        value: event.target.value
    }))
}
export const onChangeSource = (event, index, value) => (dispatch) => {
    dispatch(onChangeDispatch({
        type: types.ON_CHANGE_FIELD,
        inputType: event.target.type,
        field: 'source',
        value: value
    }))
}
export const onChangeSeo = (event, index, value) => (dispatch) => {
    dispatch(onChangeDispatch({
        type: types.ON_CHANGE_FIELD_SEO,
        field: 'keyword',
        value: event.target.value
    }))
}
export const changeTopDesc = (value) => (dispatch) => {
    dispatch(onChangeDispatch({
        type: types.ON_CHANGE_FIELD_SEO,
        field: 'top_description',
        value: value
    }))
}
export const changeBottomDesc = (value) => (dispatch) => {
    dispatch(onChangeDispatch({
        type: types.ON_CHANGE_FIELD_SEO,
        field: 'bottom_description',
        value: value
    }))
}
const onChangeDispatch = (data) => ({
    type: data.type,
    inputType: data.inputType,
    field: data.field,
    value: data.value
})

export const validate = (data) => (dispatch) => {
    dispatch(onValidateDispatch(data.error, data.msg))
}

const onValidateDispatch = (error, msg) => ({
    type: types.ON_VALIDATION,
    error: error,
    msg: msg
})

// Update MSG
export const onUpdateMsg = (msg) => (dispatch) => {

    dispatch(onUpdateMsgDispatch({
        msg: msg
    }))
}
const onUpdateMsgDispatch = (data) => ({
    type: types.ON_CHANGE_MSG,
    msg: data.msg
})

export const handleFile = (event) => async (dispatch) => {
    let response = await FileUploads.UploadFileAjax(event)
    if (response.error) {
        dispatch(fileUploadDispatch({
            type: types.FILE_UPLOAD_ERROR,
            msg: response.error

        }))
    } else {
        if (response.result.data.ack === 'OK') {
            dispatch(onChangeDispatch({
                type: types.ON_CHANGE_FIELD,
                inputType: 'input',
                field: 'image',
                value: response.result.data.file_name
            }))
        } else {
            dispatch(fileUploadDispatch({
                type: types.FILE_UPLOAD_ERROR,
                msg: response.result.data.msg
            }))
        }
    }
}
const fileUploadDispatch = (data) => ({
    type: data.type,
    msg: data.msg
})

export const updateInitialData = (data) => (dispatch) =>{
    dispatch(onUpdateInitialData({
        type: types.UPDATE_INITIAL_DATA,
        category: data.category ? {
            name: data.category.name,
            search_index: data.category.search_index,
            description: data.category.description,
            image: data.category.image,
            status: data.category.status ? true:false,
            main_category: data.category.main_category ? true:false,
            source: data.category.source
        }:null,
        seo: data.seo ? {
            top_description:data.seo.top_description,
            bottom_description:data.seo.bottom_description,
            keyword:data.seo.keyword,
            description:data.seo.description,
        }:null
    }))
}

const onUpdateInitialData = (data) =>({
    type: data.type,
    category: data.category,
    seo:data.seo
})
