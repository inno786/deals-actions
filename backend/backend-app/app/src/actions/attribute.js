import * as types from '../constants/ActionTypes'
import FileUploads from '../modules/FileUpload'

export const onChange = (event, index, value) => (dispatch) => {
    dispatch(onChangeDispatch({
        type: types.ON_CHANGE_FIELD_ATTR,
        inputType: event.target.type,
        field: event.target.name,
        value: event.target.value
    }))
}

export const onChangeSeo = (event, index, value) => (dispatch) => {
    dispatch(onChangeDispatch({
        type: types.ON_CHANGE_FIELD_SEO_ATTR,
        field: 'keyword',
        value: event.target.value
    }))
}
export const changeTopDesc = (value) => (dispatch) => {
    dispatch(onChangeDispatch({
        type: types.ON_CHANGE_FIELD_SEO_ATTR,
        field: 'top_description',
        value: value
    }))
}
export const changeBottomDesc = (value) => (dispatch) => {
    dispatch(onChangeDispatch({
        type: types.ON_CHANGE_FIELD_SEO_ATTR,
        field: 'bottom_description',
        value: value
    }))
}
const onChangeDispatch = (data) => ({
    type: data.type,
    inputType: data.inputType,
    field: data.field,
    value: data.value
})

export const validate = (data) => (dispatch) => {
    dispatch(onValidateDispatch(data.error, data.msg))
}

const onValidateDispatch = (error, msg) => ({
    type: types.ON_VALIDATION_ATTR,
    error: error,
    msg: msg
})

// Update MSG
export const onUpdateMsg = (msg) => (dispatch) => {

    dispatch(onUpdateMsgDispatch({
        msg: msg
    }))
}
const onUpdateMsgDispatch = (data) => ({
    type: types.ON_CHANGE_MSG_ATTR,
    msg: data.msg
})

export const handleFile = (event) => async (dispatch) => {
    let response = await FileUploads.UploadFileAjax(event)
    if (response.error) {
        dispatch(fileUploadDispatch({
            type: types.FILE_UPLOAD_ERROR_ATTR,
            msg: response.error

        }))
    } else {
        if (response.result.data.ack === 'OK') {
            dispatch(onChangeDispatch({
                type: types.ON_CHANGE_FIELD_ATTR,
                inputType: 'input',
                field: 'image',
                value: response.result.data.file_name
            }))
        } else {
            dispatch(fileUploadDispatch({
                type: types.FILE_UPLOAD_ERROR_ATTR,
                msg: response.result.data.msg
            }))
        }
    }
}
const fileUploadDispatch = (data) => ({
    type: data.type,
    msg: data.msg
})

export const updateInitialData = (data) => (dispatch) =>{
    dispatch(onUpdateInitialData({
        type: types.UPDATE_INITIAL_DATA_ATTR,
        attribute: data.attribute ? {
            name: data.attribute.name,
            display_name: data.attribute.display_name,
            image: data.attribute.image,
            status: data.attribute.status ? true:false
        }:null,
        seo: data.seo ? {
            keyword:data.seo.keyword,
            top_description:data.seo.top_description,
            bottom_description:data.seo.bottom_description
        }:null
    }))
}

const onUpdateInitialData = (data) =>({
    type: data.type,
    attribute: data.attribute,
    seo:data.seo
})
