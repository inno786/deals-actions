
import * as types from '../constants/ActionTypes'
import FileUploads from '../modules/FileUpload'

export const onChange = (event, index, value) => (dispatch) => {
    dispatch(onChangeDispatch({
        type: types.ON_CHANGE_HOME_SEO_FIELD,
        inputType: event.target.type,
        field: event.target.name,
        value: event.target.value
    }))
}
export const changeTopDesc = (value) => (dispatch) => {
    dispatch(onChangeDispatch({
        type: types.ON_CHANGE_HOME_SEO_FIELD,
        inputType: 'input',
        field: 'top_description',
        value: value
    }))
}
export const changeBottomDesc = (value) => (dispatch) => {
    dispatch(onChangeDispatch({
        type: types.ON_CHANGE_HOME_SEO_FIELD,
        inputType: 'input',
        field: 'bottom_description',
        value: value
    }))
}
const onChangeDispatch = (data) => ({
    type: data.type,
    inputType: data.inputType,
    field: data.field,
    value: data.value
})

export const validate = (data) => (dispatch) => {
    dispatch(onValidateDispatch(data.error, data.msg))
}

const onValidateDispatch = (error, msg) => ({
    type: types.ON_HOME_SEO_VALIDATION,
    error: error,
    msg: msg
})

export const handleFile = (event) => async (dispatch) => {
    let response = await FileUploads.UploadFileAjax(event)
    if (response.error) {
        dispatch(fileUploadDispatch({
            type: types.FILE_SEO_UPLOAD_ERROR,
            msg: response.error

        }))
    } else {
        if (response.result.data.ack === 'OK') {
            dispatch(onChangeDispatch({
                type: types.ON_CHANGE_HOME_SEO_FIELD,
                inputType: 'input',
                field: 'banner',
                value: response.result.data.file_name
            }))
        } else {
            dispatch(fileUploadDispatch({
                type: types.FILE_SEO_UPLOAD_ERROR,
                msg: response.result.data.msg
            }))
        }
    }
}
const fileUploadDispatch = (data) => ({
    type: data.type,
    msg: data.msg
})



// Update MSG
export const onUpdateMsg = (msg) => (dispatch) => {

    dispatch(onUpdateMsgDispatch({
        msg: msg
    }))
}
const onUpdateMsgDispatch = (data) => ({
    type: types.ON_HOME_SEO_CHANGE_MSG,
    msg: data.msg
})

export const updateInitialData = (data) => (dispatch) => {
    dispatch(onUpdateInitialData({
        type: types.UPDATE_HOME_SEO_INITIAL_DATA,
        seo: {
            title: data.title,
            keyword: data.keyword,
            description: data.description,
            top_description: data.top_description,
            bottom_description: data.bottom_description,
            banner: data.banner,
            banner_status: data.banner_status ? true:false
        }
    }))
}

export const initialData = () => (dispatch) => {
    dispatch(onUpdateInitialData({
        type: types.UPDATE_HOME_SEO_INITIAL_DATA,
        seo: {
            title: '',
            keyword: '',
            description: '',
            top_description: '',
            bottom_description: '',
            banner: '',
            banner_status: false
        }
    }))
}

const onUpdateInitialData = (data) => ({
    type: data.type,
    seo: data.seo
})
