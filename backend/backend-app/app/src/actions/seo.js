
import * as types from '../constants/ActionTypes'

export const onChange = (event, index, value) => (dispatch) => {
    dispatch(onChangeDispatch({
        type: types.ON_CHANGE_SEO_FIELD,
        field: event.target.name,
        value: event.target.value
    }))
}
export const onChangeSelect = (event, index, value) => (dispatch) => {
    dispatch(onChangeDispatch({
        type: types.ON_CHANGE_SEO_FIELD,
        field: 'type',
        value: value
    }))
}
export const changeTopDesc = (value) => (dispatch) => {
    dispatch(onChangeDispatch({
        type: types.ON_CHANGE_SEO_FIELD,
        field: 'top_description',
        value: value
    }))
}
export const changeBottomDesc = (value) => (dispatch) => {
    dispatch(onChangeDispatch({
        type: types.ON_CHANGE_SEO_FIELD,
        field: 'bottom_description',
        value: value
    }))
}
const onChangeDispatch = (data) => ({
    type: data.type,
    field: data.field,
    value: data.value
})

export const validate = (data) => (dispatch) => {
    dispatch(onValidateDispatch(data.error, data.msg))
}

const onValidateDispatch = (error, msg) => ({
    type: types.ON_SEO_VALIDATION,
    error: error,
    msg: msg
})

// Update MSG
export const onUpdateMsg = (msg) => (dispatch) => {

    dispatch(onUpdateMsgDispatch({
        msg: msg
    }))
}
const onUpdateMsgDispatch = (data) => ({
    type: types.ON_SEO_CHANGE_MSG,
    msg: data.msg
})

export const updateInitialData = (data) => (dispatch) => {
    dispatch(onUpdateInitialData({
        type: types.UPDATE_SEO_INITIAL_DATA,
        seo: {
            type: data.type,
            title: data.title,
            description: data.description,
            keyword: data.keyword,
            top_description: data.top_description,
            bottom_description: data.bottom_description
        }
    }))
}

export const initialData = (data) => (dispatch) => {
    dispatch(onUpdateInitialData({
        type: types.UPDATE_SEO_INITIAL_DATA,
        seo: {
            type: '',
            title: '',
            description: '',
            keyword: '',
            top_description: '',
            bottom_description: ''
        }
    }))
}

const onUpdateInitialData = (data) => ({
    type: data.type,
    seo: data.seo
})
