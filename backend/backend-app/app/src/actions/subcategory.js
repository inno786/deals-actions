
import * as types from '../constants/ActionTypes'
import FileUploads from '../modules/FileUpload'

export const onChange = (event, index, value) => (dispatch) => {
    dispatch(onChangeDispatch({
        type: types.ON_CHANGE_FIELD_SUB_CAT,
        inputType: event.target.type,
        field: event.target.name,
        value: event.target.value
    }))
}
export const onChangeSource = (event, index, value) => (dispatch) => {
    dispatch(onChangeDispatch({
        type: types.ON_CHANGE_FIELD_SUB_CAT,
        inputType: event.target.type,
        field: 'source',
        value: value
    }))
}
export const onChangeSelect = (event, index, value) => (dispatch) => {
    dispatch(onChangeDispatch({
        type: types.ON_CHANGE_FIELD_SUB_CAT,
        inputType: event.target.type,
        field: 'fk_category_id',
        value: value
    }))
}
export const onChangeSeo = (event, index, value) => (dispatch) => {
    dispatch(onChangeDispatch({
        type: types.ON_CHANGE_FIELD_SEO_SUB_CAT,
        field: 'keyword',
        value: event.target.value
    }))
}
export const changeTopDesc = (value) => (dispatch) => {
    dispatch(onChangeDispatch({
        type: types.ON_CHANGE_FIELD_SEO_SUB_CAT,
        field: 'top_description',
        value: value
    }))
}
export const changeBottomDesc = (value) => (dispatch) => {
    dispatch(onChangeDispatch({
        type: types.ON_CHANGE_FIELD_SEO_SUB_CAT,
        field: 'bottom_description',
        value: value
    }))
}
const onChangeDispatch = (data) => ({
    type: data.type,
    inputType: data.inputType,
    field: data.field,
    value: data.value
})

export const validate = (data) => (dispatch) => {
    dispatch(onValidateDispatch(data.error, data.msg))
}

const onValidateDispatch = (error, msg) => ({
    type: types.ON_VALIDATION_SUB_CAT,
    error: error,
    msg: msg
})

// Update MSG
export const onUpdateMsg = (msg) => (dispatch) => {

    dispatch(onUpdateMsgDispatch({
        msg: msg
    }))
}
const onUpdateMsgDispatch = (data) => ({
    type: types.ON_CHANGE_MSG_SUB_CAT,
    msg: data.msg
})

export const handleFile = (event) => async (dispatch) => {
    let response = await FileUploads.UploadFileAjax(event)
    if (response.error) {
        dispatch(fileUploadDispatch({
            type: types.FILE_UPLOAD_ERROR_SUB_CAT,
            msg: response.error

        }))
    } else {
        if (response.result.data.ack === 'OK') {
            dispatch(onChangeDispatch({
                type: types.ON_CHANGE_FIELD_SUB_CAT,
                inputType: 'input',
                field: 'image',
                value: response.result.data.file_name
            }))
        } else {
            dispatch(fileUploadDispatch({
                type: types.FILE_UPLOAD_ERROR_SUB_CAT,
                msg: response.result.data.msg
            }))
        }
    }
}
const fileUploadDispatch = (data) => ({
    type: data.type,
    msg: data.msg
})

export const updateInitialData = (data) => (dispatch) =>{
    console.log("data =>", data)
    dispatch(onUpdateInitialData({
        type: types.UPDATE_INITIAL_DATA_SUB_CAT,
        subcategory: data.subcategory ? {
            fk_category_id: data.subcategory.fk_category_id,
            name: data.subcategory.name,
            feed_name: data.subcategory.feed_name,
            description: data.subcategory.description,
            status: data.subcategory.status ? true:false,
            image: data.subcategory.image,
            source: data.subcategory.source
        }:null,
        seo: data.seo ? {
            top_description:data.seo.top_description,
            bottom_description:data.seo.bottom_description,
            keyword:data.seo.keyword,
            description:data.seo.description,
        }:null
    }))
}

const onUpdateInitialData = (data) =>({
    type: data.type,
    subcategory: data.subcategory,
    seo:data.seo
})
