import login from './login'
import category from './category'

export default {
    login,
    category
}
