import {
    ON_CHANGE_FIELD_ATTR,
    ON_VALIDATION_ATTR,
    ON_CHANGE_MSG_ATTR,
    ON_CHANGE_FIELD_SEO_ATTR,
    FILE_UPLOAD_ERROR_ATTR,
    UPDATE_INITIAL_DATA_ATTR
} from '../constants/ActionTypes'

// Initialize stat
const initialState = {
    field: {
        name:'',
        display_name:'',
        image:'',
        status: false
    },
    seo: {
        keyword: '',
        top_description: '',
        bottom_description: ''
    },
    error: {
        name: '',
        image: ''
    },
    msg: ''
}

// Event Handler
const attributeHandler = (state = initialState, action) => {

    switch (action.type) {

        case ON_CHANGE_FIELD_ATTR:
            let value = (action.inputType === 'checkbox' ? (state.field[action.field] ? false : true) : action.value)
            state.field[action.field] = value
            state.error.image = ''
            return {
                ...state,
                field: state.field,
                seo: state.seo,
                error: state.error,
                msg: state.msg
            }
        case ON_CHANGE_FIELD_SEO_ATTR:
            state.seo[action.field] = action.value
            return {
                ...state,
                field: state.field,
                seo: state.seo,
                error: state.error,
                msg: state.msg
            }
        case FILE_UPLOAD_ERROR_ATTR:
            state.error.image = action.msg
            return {
                ...state,
                field: state.field,
                seo: state.seo,
                error: state.error,
                msg: state.msg
            }
        case ON_VALIDATION_ATTR:
            state.error = action.error
            state.msg = action.msg
            return {
                ...state,
                error: state.error,
                msg: state.msg
            }
        case ON_CHANGE_MSG_ATTR:
            state.msg = action.msg
            return {
                ...state,
                msg: state.msg
            }
        case UPDATE_INITIAL_DATA_ATTR:
            state.field = action.attribute ? action.attribute:state.field
            state.seo = action.seo ? action.seo:state.seo
            return {
                ...state,
                field: state.field,
                seo: state.seo,
                error: state.error,
                msg: state.msg
            }
        default:
            return {
                ...state
            }
    }
}

export default attributeHandler
