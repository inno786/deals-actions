import {
    ON_CHANGE_HOME_SEO_FIELD,
    ON_HOME_SEO_VALIDATION,
    ON_HOME_SEO_CHANGE_MSG,
    UPDATE_HOME_SEO_INITIAL_DATA,
    FILE_SEO_UPLOAD_ERROR
} from '../constants/ActionTypes'

// Initialize stat
const initialState = {
    field: {
        title: '',
        keyword: '',
        description: '',
        top_description: '',
        bottom_description: '',
        banner:'',
        banner_status:false
    },
    error: {
        title: '',
        banner: ''
    },
    msg: ''
}

// Event Handler
const homeSeoHandler = (state = initialState, action) => {

    switch (action.type) {

        case ON_CHANGE_HOME_SEO_FIELD:
            let value = (action.inputType === 'checkbox' ? (state.field[action.field] ? false : true) : action.value)
            state.field[action.field] = value
            state.error.banner = ''
            return {
                ...state,
                field: state.field,
                error: state.error,
                msg: state.msg
            }
        case ON_HOME_SEO_CHANGE_MSG:
            state.msg = action.msg
            return {
                ...state,
                msg: state.msg
            }
        case FILE_SEO_UPLOAD_ERROR:
            state.error.image = action.msg
            return {
                ...state,
                field: state.field,
                seo: state.seo,
                error: state.error,
                msg: state.msg
            }
        case ON_HOME_SEO_VALIDATION:
            state.banner = action.error
            state.msg = action.msg
            return {
                ...state,
                error: state.error,
                msg: state.msg
            }
        case UPDATE_HOME_SEO_INITIAL_DATA:
            state.field = action.seo
            return {
                ...state,
                field: state.field,
                error: {type:''},
                msg: ""
            }
        default:
            return {
                ...state
            }
    }
}

export default homeSeoHandler
