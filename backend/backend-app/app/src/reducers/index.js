import { combineReducers } from 'redux';
import loginHandler from './login'
import authHandler from './authentication'
import categoryHandler from './category'
import subCategoryHandler from './subcategory'
import attributeHandler from './attribute'
import seoHandler from './seo'
import homeSeoHandler from './homeSeo'

export default combineReducers({
    loginHandler,
    authHandler,
    categoryHandler,
    subCategoryHandler,
    attributeHandler,
    seoHandler,
    homeSeoHandler
})
