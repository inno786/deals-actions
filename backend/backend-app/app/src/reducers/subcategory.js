import {
    ON_CHANGE_FIELD_SUB_CAT,
    ON_VALIDATION_SUB_CAT,
    ON_CHANGE_MSG_SUB_CAT,
    ON_CHANGE_FIELD_SEO_SUB_CAT,
    FILE_UPLOAD_ERROR_SUB_CAT,
    UPDATE_INITIAL_DATA_SUB_CAT
} from '../constants/ActionTypes'

// Initialize stat
const initialState = {
    field: {
        fk_category_id:'',
        name:'',
        feed_name:'',
        description: '',
        image:'',
        source:'',
        status: false
    },
    seo: {
        top_description: '',
        bottom_description: '',
        keyword: ''
    },
    error: {
        name: '',
        image: ''
    },
    msg: ''
}

// Event Handler
const subCategoryHandler = (state = initialState, action) => {

    switch (action.type) {

        case ON_CHANGE_FIELD_SUB_CAT:
            let value = (action.inputType === 'checkbox' ? (state.field[action.field] ? false : true) : action.value)
            state.field[action.field] = value
            state.error.image = ''
            return {
                ...state,
                field: state.field,
                seo: state.seo,
                error: state.error,
                msg: state.msg
            }
        case ON_CHANGE_FIELD_SEO_SUB_CAT:
            state.seo[action.field] = action.value
            return {
                ...state,
                field: state.field,
                seo: state.seo,
                error: state.error,
                msg: state.msg
            }
        case FILE_UPLOAD_ERROR_SUB_CAT:
            state.error.image = action.msg
            return {
                ...state,
                field: state.field,
                seo: state.seo,
                error: state.error,
                msg: state.msg
            }
        case ON_VALIDATION_SUB_CAT:
            state.error = action.error
            state.msg = action.msg
            return {
                ...state,
                error: state.error,
                msg: state.msg
            }
        case ON_CHANGE_MSG_SUB_CAT:
            state.msg = action.msg
            return {
                ...state,
                msg: state.msg
            }
        case UPDATE_INITIAL_DATA_SUB_CAT:
            state.field = action.subcategory ? action.subcategory:state.field
            state.seo = action.seo ? action.seo:state.seo
            return {
                ...state,
                field: state.field,
                seo: state.seo,
                error: state.error,
                msg: state.msg
            }
        default:
            return {
                ...state
            }
    }
}

export default subCategoryHandler
