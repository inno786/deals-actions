import {
    ON_CHANGE_FIELD,
    ON_VALIDATION,
    ON_CHANGE_MSG,
    ON_CHANGE_FIELD_SEO,
    FILE_UPLOAD_ERROR,
    UPDATE_INITIAL_DATA
} from '../constants/ActionTypes'

// Initialize stat
const initialState = {
    field: {
        name: '',
        search_index: '',
        description: '',
        image: '',
        status: false,
        main_category: false,
        source: ''
    },
    seo: {
        top_description: '',
        bottom_description: '',
        keyword: '',
        description: ''
    },
    error: {
        name: '',
        image: ''
    },
    msg: ''
}

// Event Handler
const categoryHandler = (state = initialState, action) => {

    switch (action.type) {

        case ON_CHANGE_FIELD:
            let value = (action.inputType ==='checkbox' ? (state.field[action.field] ? false : true) : action.value)
            state.field[action.field] = value
            state.error.image = ''
            return {
                ...state,
                field: state.field,
                seo: state.seo,
                error: state.error,
                msg: state.msg
            }
        case ON_CHANGE_FIELD_SEO:
            state.seo[action.field] = action.value
            return {
                ...state,
                field: state.field,
                seo: state.seo,
                error: state.error,
                msg: state.msg
            }
        case FILE_UPLOAD_ERROR:
            state.error.image = action.msg
            return {
                ...state,
                field: state.field,
                seo: state.seo,
                error: state.error,
                msg: state.msg
            }
        case ON_VALIDATION:
            state.error = action.error
            state.msg = action.msg
            return {
                ...state,
                error: state.error,
                msg: state.msg
            }
        case ON_CHANGE_MSG:
            state.msg = action.msg
            return {
                ...state,
                msg: state.msg
            }
        case UPDATE_INITIAL_DATA:
            state.field = action.category ? action.category:state.field
            state.seo = action.seo ? action.seo:state.seo
            return {
                ...state,
                field: state.field,
                seo: state.seo,
                error: state.error,
                msg: state.msg
            }
        default:
            return {
                ...state
            }
    }
}

export default categoryHandler
