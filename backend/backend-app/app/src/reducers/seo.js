import {
    ON_CHANGE_SEO_FIELD,
    ON_SEO_VALIDATION,
    ON_SEO_CHANGE_MSG,
    UPDATE_SEO_INITIAL_DATA
} from '../constants/ActionTypes'

// Initialize stat
const initialState = {
    field: {
        type: '',
        title: '',
        description: '',
        keyword: '',
        top_description: '',
        bottom_description: ''
    },
    error: {
        type: ''
    },
    msg: ''
}

// Event Handler
const seoHandler = (state = initialState, action) => {

    switch (action.type) {

        case ON_CHANGE_SEO_FIELD:
            state.field[action.field] = action.value
            return {
                ...state,
                field: state.field,
                error: state.error,
                msg: state.msg
            }
        case ON_SEO_CHANGE_MSG:
            state.msg = action.msg
            return {
                ...state,
                msg: state.msg
            }
        case ON_SEO_VALIDATION:
            state.error = action.error
            state.msg = action.msg
            return {
                ...state,
                error: state.error,
                msg: state.msg
            }
        case UPDATE_SEO_INITIAL_DATA:
            state.field = action.seo
            return {
                ...state,
                field: state.field,
                error: {type:''},
                msg: ""
            }
        default:
            return {
                ...state
            }
    }
}

export default seoHandler
