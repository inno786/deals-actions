import React from 'react';
import { connect } from 'react-redux'
import {notify} from 'react-notify-toast'
import { Card } from 'material-ui/Card';
import MainHeader from '../../components/layout/MainHeader'
import Layout from '../../components/layout/Layout'
import RequestHandler from '../../lib/RequestHandler'
import Forms from '../../components/home_seo/Forms'
import {styles} from "../../style/Styles"
import {
    onChange,
    changeTopDesc,
    changeBottomDesc,
    validate,
    handleFile,
    onUpdateMsg,
    updateInitialData,
    initialData
} from '../../actions/homeSeo'
let requestHandlerObj = new RequestHandler();

class CreateUpdate extends React.Component {

  /**
   * Class constructor.
   */
  constructor(props, context) {
    super(props, context);
    this.onSubmit = this.onSubmit.bind(this)
  }

  async componentWillMount()
  {
      let response =  await requestHandlerObj.getRequest('post', '/homeSeo/findSeo', {})
       if(!response.error && !response.result.data.message)
      {
         this.props.updateInitialData(response.result.data.arrData)
      }else{
        this.props.initialData()
      }
  }

    async onSubmit(event){
        event.preventDefault();
        let dataObj = {
            title: this.props.arrData.field.title,
            keyword:this.props.arrData.field.keyword,
            description: this.props.arrData.field.description,
            top_description:this.props.arrData.field.top_description,
            bottom_description: this.props.arrData.field.bottom_description,
            banner: this.props.arrData.field.banner,
            banner_status: this.props.arrData.field.banner_status ? 1 :0
        }
        let response =  await requestHandlerObj.getRequest('post', '/homeSeo/create-update', dataObj)
        if(response.error)
        {
            this.props.onUpdateMsg(response.error)
        }else{
            if(response.result.data.success)
            {
                notify.show(response.result.data.message, 'success', 5000, styles.notificationMsg)
            }else{
                this.props.validate(response.result.data)
            }
        }
  }

  /**
   * Render the component.
   */
  render() {
    return (
        <div>
            <MainHeader/>
            <table style={styles.tableDefault}>
                <tbody>
                <tr>
                    <td style={styles.tableLayout}>
                    <Layout />
                    </td>
                    <td style={styles.tableContainer}>
                        <Card>
                            <Forms
                                arrData={this.props.arrData}
                                onSubmit={this.onSubmit}
                                onChange={this.props.onChange}
                                handleFile={this.props.handleFile}
                                changeTopDesc={this.props.changeTopDesc}
                                changeBottomDesc={this.props.changeBottomDesc}
                                mainTitle="Home Page Seo Update"
                                btnText="Submit"
                            />
                        </Card>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
  );
 }
}


const mapStateToProps = (state, props) =>{
    return {
        arrData:state.homeSeoHandler
    }
}

export default connect(
    mapStateToProps,
    {onChange, changeTopDesc, changeBottomDesc, validate, handleFile, onUpdateMsg, updateInitialData, initialData}
)(CreateUpdate)
