import React from 'react';
import { connect } from 'react-redux'
import { Card } from 'material-ui/Card';
import MainHeader from '../../components/layout/MainHeader'
import Layout from '../../components/layout/Layout'
import RequestHandler from '../../lib/RequestHandler'
import Forms from '../../components/subcategory/Forms'
import {styles} from "../../style/Styles"
import Auth from '../../modules/Auth'
import {
    onChange,
    onChangeSelect,
    onChangeSource,
    handleFile,
    onChangeSeo,
    changeTopDesc,
    changeBottomDesc,
    validate,
    onUpdateMsg,
    updateInitialData
} from '../../actions/subcategory'
let requestHandlerObj = new RequestHandler();

class Update extends React.Component {

    /**
     * Class constructor.
     */
    constructor(props, context) {
        super(props, context);


        let urlParams = new URLSearchParams(window.location.search)
        this.state ={
            id:urlParams.has('id') ? urlParams.get('id') : null,
            catList:[]
        }
        this.onSubmit = this.onSubmit.bind(this)
        this.categoryList=this.categoryList.bind(this)
    }

    async componentWillMount()
    {
        let response =  await requestHandlerObj.getRequest('post', '/subcategory/findByPk', {id:this.state.id})
        if(!response.error)
        {
           this.props.updateInitialData(response.result.data)
        }
        this.categoryList()
    }

    async categoryList()
    {
        let response  = await requestHandlerObj.getRequest('post', '/common/categoryList', {main_category:1})
        if(response.error)
        {
            this.setState({
                message: response.error
            })
        }else{
            this.setState({
                catList: response.result.data.arrData
            });
        }
    }

    async onSubmit(event){

        event.preventDefault();

        let dataObj = {
            _id:this.state.id,
            fk_category_id:this.props.arrData.field.fk_category_id,
            name: this.props.arrData.field.name,
            feed_name:this.props.arrData.field.feed_name,
            description: this.props.arrData.field.description,
            status: this.props.arrData.field.status ? 1 :0,
            image:this.props.arrData.field.image,
            source: this.props.arrData.field.source,
            keyword: this.props.arrData.seo.keyword,
            top_description: this.props.arrData.seo.top_description,
            bottom_description: this.props.arrData.seo.bottom_description
        }
        let response =  await requestHandlerObj.getRequest('post', '/subcategory/update', dataObj)
        if(response.error)
        {
            this.props.onUpdateMsg(response.error)
        }else{
            if(response.result.data.success)
            {
                Auth.authenticateUser('successMessage',response.result.data.message)
                this.props.history.push('/admin/subcategory')
            }else{
                this.props.validate(response.result.data)
            }
        }
    }

    /**
     * Render the component.
     */
    render() {
        return (
            <div>
                <MainHeader/>
                <table style={styles.tableDefault}>
                    <tbody>
                    <tr>
                        <td style={styles.tableLayout}>
                            <Layout/>
                        </td>
                        <td style={styles.tableContainer}>
                            <Card>
                                <Forms
                                    onSubmit={this.onSubmit}
                                    onChange={this.props.onChange}
                                    onChangeSelect={this.props.onChangeSelect}
                                    onChangeSource={this.props.onChangeSource}
                                    onChangeSeo={this.props.onChangeSeo}
                                    changeTopDesc={this.props.changeTopDesc}
                                    changeBottomDesc={this.props.changeBottomDesc}
                                    handleFile={this.props.handleFile}
                                    arrData={this.props.arrData}
                                    catList={this.state.catList}
                                    mainTitle="Update Subcategory"
                                    btnText="Update"
                                />
                            </Card>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        );
    }
}


const mapStateToProps = (state, props) =>{
    return {
        arrData:state.subCategoryHandler
    }
}

export default connect(
    mapStateToProps,
    {onChange, onChangeSelect, onChangeSource, onChangeSeo, changeTopDesc, changeBottomDesc, handleFile,  validate, onUpdateMsg, updateInitialData}
)(Update)
