import React from 'react'
import 'babel-polyfill'
import {notify} from 'react-notify-toast'
import Loader from 'react-loader-advanced'
import Auth from '../../../modules/Auth'
import MainHeader from '../../../components/layout/MainHeader'
import Layout from '../../../components/layout/Layout'
import AdvancedSearch from '../../../components/feed/flipkart/AdvancedSearchCat'
import CategoryList from '../../../components/feed/flipkart/Category'
import { Card} from 'material-ui/Card'
import $ from "jquery";
import {styles} from '../../../style/Styles'
import RequestHandler from '../../../lib/RequestHandler'
//import AdvancedSearchCat from '../../../components/feed/flipkart/AdvancedSearchCat';
let requestHandlerObj = new RequestHandler();

class Category extends React.Component {

  /**
   * Class constructor.
   */
  constructor(props, context) {
    super(props, context);

    const storedMessage = localStorage.getItem('successMessage');
    let successMessage = '';

    if (storedMessage) {
      successMessage = storedMessage;
      localStorage.removeItem('successMessage');
    }

    this.state ={
      notifiactionType:'success',
      successMessage:successMessage,
      arrData: [],
      dropDownCatList:[],
      arrSearch: {
        resource: '',
        status: ''
      },
      chb: [],
      changeStatus:{
        resource:''
      },
      loader:{
        status:false,
        msg:'Please wait...'
      },
      pagging:{
        total:0,
        display: Auth.perPageItem(),
        current:0
      }
    };
    this.listData =this.listData.bind(this);
    this.dropdownCategory =this.dropdownCategory.bind(this);
    this.onSearch =this.onSearch.bind(this);
    this.checkAll =this.checkAll.bind(this);
    this.clickCheckbox =this.clickCheckbox.bind(this);
    this.onChangeStatus =this.onChangeStatus.bind(this);
    this.getCategory = this.getCategory.bind(this);
    this.getProducts = this.getProducts.bind(this);
    this.removeProducts =this.removeProducts.bind(this);
    this.changeCategory = this.changeCategory.bind(this);
    this.changeStatus = this.changeStatus.bind(this);
    this.onChangePage =this.onChangePage.bind(this);
    this.clearData = this.clearData.bind(this);
  }

  async listData()
  {
    let objData ={
      resource:this.state.arrSearch.resource,
      status:this.state.arrSearch.status,
      limit:this.state.pagging.display,
      currentPage:this.state.pagging.current
    }
    let response =  await requestHandlerObj.getRequest('post', '/flipkart_category/list', objData)
    let pagging = this.state.pagging;
    pagging.total = response.result.data.total
    this.setState((prevState) =>{
      return{
        successMessage: response.result.data.message,
        notifiactionType:'success',
        arrData: response.result.data.arrData,
        pagging
      }
    }); 
  }

  async dropdownCategory()
  {

    let response =  await requestHandlerObj.getRequest('post', '/common/flipkartCategoryList', {})
    if(response.error)
    {
        this.setState({
            successMessage: response.error,
            notifiactionType:'error',
            arrData:[]
        })
    }else{
        this.setState({
          successMessage: response.result.data.message,
          notifiactionType:'success',
          dropDownCatList: response.result.data.arrData
        });
    }
  }

  /**
   * This method will be executed after initial rendering.
   */
  componentDidMount() {
    this.listData();
    this.dropdownCategory();
    if(this.state.successMessage)
    {
      notify.show(this.state.successMessage, this.state.notifiactionType, 5000, styles.notificationMsg)
    }
  }
  onSearch(event){
    // prevent default action. in this case, action is the form submission event
    event.preventDefault();
    this.listData();
  }

  changeCategory(event, index, value) {

    const arrSearch = this.state.arrSearch;
    arrSearch['resource'] = value;
    this.setState({
      arrSearch
    });
  }

  changeStatus(event, index, value) {

    const arrSearch = this.state.arrSearch;
    arrSearch['status'] = value;
    this.setState({
      arrSearch
    });
  }

  /* Get Category */
 async getCategory()
{
  let loader = this.state.loader
  loader.status = true
  this.setState({loader})
  let response =  await requestHandlerObj.getRequest('post', '/flipkart_category/getCategory', {})
  
  loader.status = false
  if(response.error)
  {
      this.setState({
          successMessage: response.error,
          notifiactionType:'error',
          loader,
          arrData:[]
      })
  }else{
      this.setState({
        successMessage: response.result.data.message,
        notifiactionType:'success',
        loader
      });
      this.componentDidMount();
  }
}


/* Get Products */
async getProducts(id)
{
  let loader = this.state.loader
  loader.status = true
  this.setState({loader})  
  if(id)
  {
    let response =  await requestHandlerObj.getRequest('post', '/flipkart_product/getProduct', {id:id})
    loader.status = false
    if(response.error)
    {
        this.setState({
            successMessage: response.error,
            notifiactionType:'error',
            arrData:[],
            loader
        })
    }else{
        this.setState({
          successMessage: response.result.data.message,
          notifiactionType:'success',
          loader
        });
        this.componentDidMount();
    }
  }else{
    loader.status = false
    this.setState({
      notifiactionType:'error',
      successMessage: 'Id is blank.',
      loader
    }, () =>{
      notify.show(this.state.successMessage, this.state.notifiactionType, 5000, styles.notificationMsg)
    });
 }
}

/* Get Products */
async removeProducts(id)
{
  let loader = this.state.loader
  loader.status = true
  this.setState({loader}) 
  if(id)
  {
    let response =  await requestHandlerObj.getRequest('post', '/flipkart_product/removeProduct', {id:id})
    loader.status = false
    if(response.error)
    {
        this.setState({
            successMessage: response.error,
            notifiactionType:'error',
            arrData:[],
            loader
        })
    }else{
        this.setState({
            successMessage: response.result.data.message,
            notifiactionType:'success',
            loader
        });
    }
    this.componentDidMount();
  }else{
    loader.status = false
    this.setState({
      successMessage: 'Id cannot be blank',
      notifiactionType:'error',
      loader
  });
  }
}
clickCheckbox(productId,event) {

  let chb = this.state.chb;
  let id =event.target.id;
  let check = $("#"+id).is(":checked");
   if(check) {
      chb.push(`${productId}`);
   } else {
       let index = chb.indexOf(`${productId}`);
       if (index > -1) {
           chb.splice(index, 1);
        }
     }
   this.setState({
     chb
   });
 }

 checkAll()
  {
    console.log("Call Check all");
  }

  async onChangeStatus(event, index, value) {

    const changeStatus = this.state.changeStatus;
    changeStatus['resource'] = value;
    this.setState({
      changeStatus
    });
    let chb = this.state.chb;
    let currentStatus = this.state.changeStatus.resource;
    if(chb.length >0 && currentStatus)
    {
      let objData ={
        currentStatus:currentStatus,
        resource:chb
      }
      let response =  await requestHandlerObj.getRequest('post', '/flipkart_category/changeStatus', objData)
      if(response.error)
      {
          this.setState({
              successMessage: response.error,
              notifiactionType:'error',
              arrData:[]
          })
      }else{
          this.setState({
              successMessage: response.result.data.message,
              notifiactionType:'success',
              chb:[]
          });
      }
    }else{
      this.setState({
        successMessage: 'Please select atleat one item',
        notifiactionType:'error'
     });
    }
    this.componentDidMount();
  }

/* Paging is start here */
onChangePage(event) {
  const pagging = this.state.pagging;
  pagging['current'] = event;

  this.setState({
    pagging
  });
  this.listData();
  }

  /* clear Data */
async clearData()
{
  let loader = this.state.loader
  loader.status = true
  this.setState({loader}) 
  let response =  await requestHandlerObj.getRequest('post', '/reset/clearData',{})
    loader.status = false
    if(response.error)
    {
        this.setState({
            successMessage: response.error,
            notifiactionType:'error',
            loader
        })
    }else{
        this.setState({
            successMessage: response.result.data.message,
            notifiactionType:'success',
            loader
        });
    }
    this.componentDidMount();
}


  /**
   * Render the component.
   */
  render() {
    return (
        <div>
          <MainHeader/>
          <table style={styles.tableDefault}>
            <tbody>
            <tr>
              <td style={styles.tableLayout}>
                <Layout/>
              </td>
              <td style={styles.tableContainer}>
                <Card>
                  <AdvancedSearch
                    onSubmit={this.onSearch}
                    arrSearch={this.state.arrSearch}
                    changeCategory={this.changeCategory}
                    changeStatus={this.changeStatus}
                    dropDownCatList={this.state.dropDownCatList}
                    getCategory={this.getCategory}
                    clearData={this.clearData}
                  />
                  <Loader show={this.state.loader.status} message={this.state.loader.msg}>
                  <CategoryList
                      arrData={this.state.arrData}
                      getProducts={this.getProducts}
                      removeProducts={this.removeProducts}
                      checkAll={this.checkAll}
                      clickCheckbox={this.clickCheckbox}
                      onChangeStatus={this.onChangeStatus}
                      changeStatus={this.state.changeStatus}
                      total={this.state.pagging.total}
                      perPageItem={this.state.pagging.display}
                      current={this.state.pagging.current}
                      onChangePage={this.onChangePage}
                    />
                 </Loader>
                </Card>
              </td>
            </tr>
            </tbody>
          </table>
        </div>
    );
 }
}
export default Category;
