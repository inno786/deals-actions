import React from 'react'
import 'babel-polyfill'
import {notify} from 'react-notify-toast'
import Auth from '../../../modules/Auth'
import MainHeader from '../../../components/layout/MainHeader'
import Layout from '../../../components/layout/Layout'
import AdvancedSearch from '../../../components/feed/flipkart/AdvancedSearchProduct'
import ProductList from '../../../components/feed/flipkart/Product'
import { Card} from 'material-ui/Card'
import $ from "jquery";
import {styles} from '../../../style/Styles'
import RequestHandler from '../../../lib/RequestHandler'
let requestHandlerObj = new RequestHandler()

class Product extends React.Component {

  /**
   * Class constructor.
   */
  constructor(props, context) {
    super(props, context);

    const storedMessage = localStorage.getItem('successMessage');
    let successMessage = '';

    if (storedMessage) {
      successMessage = storedMessage;
      localStorage.removeItem('successMessage');
    }

    this.state ={
      notifiactionType:'success',
      successMessage:successMessage,
      arrData: [],
      dropDownCatList:[],
      arrSearch: {
        resource: ''
      },
      chb: [],
      changeStatus:{
        resource:''
      },
      pagging:{
        total:0,
        display: Auth.perPageItem(),
        current:0
      }
    };
    this.listData =this.listData.bind(this);
    this.dropdownCategory =this.dropdownCategory.bind(this);
    this.onSearch =this.onSearch.bind(this);
    this.checkAll =this.checkAll.bind(this);
    this.clickCheckbox =this.clickCheckbox.bind(this);
    this.changeCatProd =this.changeCatProd.bind(this);
    this.changeCategory = this.changeCategory.bind(this);
    this.onChangePage =this.onChangePage.bind(this);

  }

  async listData()
  {
    let objData ={
      resource:this.state.arrSearch.resource,
      limit:this.state.pagging.display,
      currentPage:this.state.pagging.current
    }
    let response =  await requestHandlerObj.getRequest('post', '/flipkart_category/product_list', objData)
    
    let pagging = this.state.pagging;
    pagging.total = response.result.data.total
    this.setState((prevState) =>{
      return{
        successMessage: response.result.data.message,
        notifiactionType:'success',
        arrData: response.result.data.arrData,
        pagging
      }
    }); 
  }

  async dropdownCategory()
  {

    let response =  await requestHandlerObj.getRequest('post', '/common/flipkartCategoryList', {})
    if(response.error)
    {
        this.setState({
            successMessage: response.error,
            notifiactionType:'error',
            arrData:[]
        })
    }else{
        this.setState({
            successMessage: response.result.data.message,
            notifiactionType:'success',
            dropDownCatList: response.result.data.arrData
        });
    }
  }

  /**
   * This method will be executed after initial rendering.
   */
  componentDidMount() {
    this.listData();
    this.dropdownCategory();
    if(this.state.successMessage)
    {
      notify.show(this.state.successMessage, this.state.notifiactionType, 5000, styles.notificationMsg)
    }
  }
  onSearch(event){
    // prevent default action. in this case, action is the form submission event
    event.preventDefault();
    this.listData();
  }

  changeCategory(event, index, value) {

    const arrSearch = this.state.arrSearch;
    arrSearch['resource'] = value;
    this.setState({
      arrSearch
    });
  }

clickCheckbox(productId,event) {

  let chb = this.state.chb;
  let id =event.target.id;
  let check = $("#"+id).is(":checked");
   if(check) {
      chb.push(`${productId}`);
   } else {
       let index = chb.indexOf(`${productId}`);
       if (index > -1) {
           chb.splice(index, 1);
        }
     }
   this.setState({
     chb
   });
 }

 checkAll()
  {
    console.log("Call Check all");
  }

  async changeCatProd(event, index, value) {

    const changeStatus = this.state.changeStatus;
    changeStatus['resource'] = value;
    this.setState({
      changeStatus
    });
    let chb = this.state.chb;
    let currentStatus = this.state.changeStatus.resource;
    if(chb.length >0 && currentStatus)
    {
      let objData ={
        currentStatus:currentStatus,
        resource:chb
      }
      let response =  await requestHandlerObj.getRequest('post', '/flipkart_category/changeCatProd', objData)
      if(response.error)
      {
          this.setState({
            successMessage: response.error,
            notifiactionType:'error',
            arrData:[]
          })
      }else{
          this.setState({
            successMessage: response.result.data.message,
            notifiactionType:'error',
            chb:[]
          });
          this.componentDidMount();
      }
    }else{
      this.setState({
        notifiactionType:'error',
        successMessage: 'Please select atleat one item'
      }, () =>{
        notify.show(this.state.successMessage, this.state.notifiactionType, 5000, styles.notificationMsg)
      });
    }
  }

/* Paging is start here */
onChangePage(event) {
  const pagging = this.state.pagging;
  pagging['current'] = event;

  this.setState({
    pagging
  });
  this.listData();
  }


  /**
   * Render the component.
   */
  render() {
    return (
        <div>
          <MainHeader/>
          <table style={styles.tableDefault}>
            <tbody>
            <tr>
              <td style={styles.tableLayout}>
                <Layout/>
              </td>
              <td style={styles.tableContainer}>
                <Card>
                  <AdvancedSearch
                      {...this.state}
                      onSubmit={this.onSearch}
                      arrSearch={this.state.arrSearch}
                      changeCategory={this.changeCategory}
                  />
                  <ProductList
                      {...this.state}
                      checkAll={this.checkAll}
                      clickCheckbox={this.clickCheckbox}
                      changeCatProd={this.changeCatProd}
                      onChangePage={this.onChangePage}
                  />
                </Card>
              </td>
            </tr>
            </tbody>
          </table>
        </div>
    );
 }
}
export default Product;
