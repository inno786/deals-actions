import React from 'react';
import { connect } from 'react-redux'
import 'babel-polyfill'
import Login from '../../components/layout/Login'
import {onChange, validate, onUpdateMsg} from '../../actions/login'
import Auth from '../../modules/Auth'
import RequestHandler from '../../lib/RequestHandler'
let requestHandlerObj = new RequestHandler();

class Content extends React.Component {

    constructor(props, context) {
        super(props, context);

        Auth.deauthenticateUser('token');
        Auth.deauthenticateUser('loggingUser');
        Auth.deauthenticateUser('loggingEmail');
        Auth.deauthenticateUser('loggingRole');

        this.onSubmit = this.onSubmit.bind(this)
    }

    async onSubmit(event)
    {
        event.preventDefault()
        let username = this.props.arrData.field.username,
            password = this.props.arrData.field.password;
        this.props.validate(this.props.arrData.field)
        if(username && password) {
           let response =  await requestHandlerObj.getRequest('post', '/auth/login', {email:username, password:password})
            if(response.error)
            {
                this.props.onUpdateMsg(response.error)
            }else{
                if(response.result.data.success)
                {
                    Auth.authenticateUser('token',response.result.data.token)
                    Auth.authenticateUser('loggingUser',`${response.result.data.user.name}`)
                    Auth.authenticateUser('loggingEmail',`${response.result.data.user.email}`)
                    Auth.authenticateUser('loggingRole',`${response.result.data.user.role}`)
                    this.props.history.push('/admin/dashboard')
                }else{
                    this.props.onUpdateMsg(response.result.data.message)
                }
            }
        }
    }
    /**
     * Render the component.
     */
    render() {
        return (
            <Login
                onSubmit={this.onSubmit}
                onChange={this.props.onChange}
                arrData={this.props.arrData}
            />
        );
    }
}

const mapStateToProps = (state, props) =>{
  return {
        arrData:state.loginHandler
  }
}

export default connect(
        mapStateToProps,
    {onChange, validate, onUpdateMsg}
)(Content)