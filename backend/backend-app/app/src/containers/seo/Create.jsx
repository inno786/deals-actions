import React from 'react';
import { connect } from 'react-redux'
import { Card } from 'material-ui/Card';
import MainHeader from '../../components/layout/MainHeader'
import Layout from '../../components/layout/Layout'
import RequestHandler from '../../lib/RequestHandler'
import Forms from '../../components/seo/Forms'
import {styles} from "../../style/Styles"
import Auth from '../../modules/Auth'
import {
    onChange,
    onChangeSelect,
    changeTopDesc,
    changeBottomDesc,
    validate,
    initialData,
    onUpdateMsg
} from '../../actions/seo'
let requestHandlerObj = new RequestHandler();

class Create extends React.Component {

  /**
   * Class constructor.
   */
  constructor(props, context) {
    super(props, context);
    this.onSubmit = this.onSubmit.bind(this)
  }

  componentDidMount()
  {
      this.props.initialData();
  }

    async onSubmit(event){
        event.preventDefault();
        let dataObj = {
            type: this.props.arrData.field.type,
            title:this.props.arrData.field.title,
            keyword: this.props.arrData.field.keyword,
            description:this.props.arrData.field.description,
            top_description: this.props.arrData.field.top_description,
            bottom_description: this.props.arrData.field.bottom_description
        }
        let response =  await requestHandlerObj.getRequest('post', '/seo/create', dataObj)
        if(response.error)
        {
            this.props.onUpdateMsg(response.error)
        }else{
            if(response.result.data.success)
            {
                Auth.authenticateUser('successMessage',response.result.data.message)
                this.props.history.push('/admin/seo')
            }else{
                this.props.validate(response.result.data)
            }
        }
  }

  /**
   * Render the component.
   */
  render() {
    return (
        <div>
            <MainHeader/>
            <table style={styles.tableDefault}>
                <tbody>
                <tr>
                    <td style={styles.tableLayout}>
                    <Layout />
                    </td>
                    <td style={styles.tableContainer}>
                        <Card>
                            <Forms
                                arrData={this.props.arrData}
                                onSubmit={this.onSubmit}
                                onChange={this.props.onChange}
                                onChangeSelect={this.props.onChangeSelect}
                                changeTopDesc={this.props.changeTopDesc}
                                changeBottomDesc={this.props.changeBottomDesc}
                                mainTitle="Create New Seo"
                                btnText="Create"
                            />
                        </Card>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
  );
 }
}


const mapStateToProps = (state, props) =>{
    return {
        arrData:state.seoHandler
    }
}

export default connect(
    mapStateToProps,
    {onChange, onChangeSelect, changeTopDesc, changeBottomDesc, validate, initialData, onUpdateMsg}
)(Create)
