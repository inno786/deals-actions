import React from 'react'
import 'babel-polyfill'
import {notify} from 'react-notify-toast'
import MainHeader from '../../components/layout/MainHeader'
import Layout from '../../components/layout/Layout'
import AdvancedSearch from '../../components/seo/AdvancedSearch'
import SeoList from '../../components/seo/List'
import { Card} from 'material-ui/Card'
import {styles} from '../../style/Styles'
import RequestHandler from '../../lib/RequestHandler'
let requestHandlerObj = new RequestHandler();

class List extends React.Component {

  /**
   * Class constructor.
   */
  constructor(props, context) {
    super(props, context);

    const storedMessage = localStorage.getItem('successMessage');
    let successMessage = '';

    if (storedMessage) {
      successMessage = storedMessage;
      localStorage.removeItem('successMessage');
    }

    this.state ={
      notifiactionType:'success',
      successMessage:successMessage,
      arrData: []
    };
    this.listData =this.listData.bind(this);
    this.deleteAction =this.deleteAction.bind(this);
  }

  async listData()
  {
      let response =  await requestHandlerObj.getRequest('post', '/seo/list', {})
      if(response.error)
      {
          this.setState({
              successMessage: response.error
          }, () =>{
           notify.show(response.error, 'error', 5000, styles.notificationMsg)
          })
      }else{
          this.setState({
              arrData: response.result.data.arrData
          });
      }
  }

  /**
   * This method will be executed after initial rendering.
   */
  componentDidMount() {
    this.listData();
    if(this.state.successMessage)
    {
      notify.show(this.state.successMessage, this.state.notifiactionType, 5000, styles.notificationMsg)
    }
  }

  deleteAction(eventId){
   // const id = `id=${eventId}`;
  }

  /**
   * Render the component.
   */
  render() {
    return (
        <div>
          <MainHeader/>
          <table style={styles.tableDefault}>
            <tbody>
            <tr>
              <td style={styles.tableLayout}>
              <Layout />
              </td>
              <td style={styles.tableContainer}>
                <Card>
                 <AdvancedSearch />
                  <SeoList
                      {...this.state}
                      deleteAction={this.deleteAction}
                  />
                </Card>
              </td>
            </tr>
            </tbody>
          </table>
        </div>
    );
 }
}
export default List;
