import React from 'react';
import { connect } from 'react-redux'
import { Card } from 'material-ui/Card';
import MainHeader from '../../components/layout/MainHeader'
import Layout from '../../components/layout/Layout'
import RequestHandler from '../../lib/RequestHandler'
import Forms from '../../components/seo/Forms'
import {styles} from "../../style/Styles"
import Auth from '../../modules/Auth'
import {
    onChange,
    onChangeSelect,
    changeTopDesc,
    changeBottomDesc,
    validate,
    onUpdateMsg,
    updateInitialData
} from '../../actions/seo'
let requestHandlerObj = new RequestHandler();

class Update extends React.Component {

    /**
     * Class constructor.
     */
    constructor(props, context) {
        super(props, context);

        let urlParams = new URLSearchParams(window.location.search)
        this.state ={
            id:urlParams.has('id') ? urlParams.get('id') : null
        }
        this.onSubmit = this.onSubmit.bind(this)
    }

    async componentWillMount()
    {
        let response =  await requestHandlerObj.getRequest('post', '/seo/findByPk', {id:this.state.id})
        console.log("response =>", response);
        if(!response.error)
        {
           this.props.updateInitialData(response.result.data.arrData)
        }
    }

    async onSubmit(event){

        event.preventDefault();

        let dataObj = {
            _id:this.state.id,
            type: this.props.arrData.field.type,
            title:this.props.arrData.field.title,
            keyword: this.props.arrData.field.keyword,
            description:this.props.arrData.field.description,
            top_description: this.props.arrData.field.top_description,
            bottom_description: this.props.arrData.field.bottom_description
        }
        let response =  await requestHandlerObj.getRequest('post', '/seo/update', dataObj)
        if(response.error)
        {
            this.props.onUpdateMsg(response.error)
        }else{
            if(response.result.data.success)
            {
                Auth.authenticateUser('successMessage',response.result.data.message)
                this.props.history.push('/admin/seo')
            }else{
                this.props.validate(response.result.data)
            }
        }
    }
    

    /**
     * Render the component.
     */
    render() {
        return (
            <div>
                <MainHeader/>
                <table style={styles.tableDefault}>
                    <tbody>
                    <tr>
                        <td style={styles.tableLayout}>
                        <Layout />
                        </td>
                        <td style={styles.tableContainer}>
                            <Card>
                                <Forms
                                    arrData={this.props.arrData}
                                    onSubmit={this.onSubmit}
                                    onChange={this.props.onChange}
                                    onChangeSelect={this.props.onChangeSelect}
                                    changeTopDesc={this.props.changeTopDesc}
                                    changeBottomDesc={this.props.changeBottomDesc}
                                    mainTitle="Update Seo"
                                    btnText="Update"
                                />
                            </Card>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        );
    }
}


const mapStateToProps = (state, props) =>{
    return {
        arrData:state.seoHandler
    }
}

export default connect(
    mapStateToProps,
    {onChange, onChangeSelect, changeTopDesc, changeBottomDesc, validate, onUpdateMsg, updateInitialData}
)(Update)
