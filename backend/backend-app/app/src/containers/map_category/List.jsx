import React from 'react'
import 'babel-polyfill'
import {notify} from 'react-notify-toast'
import Auth from '../../modules/Auth'
import MainHeader from '../../components/layout/MainHeader'
import Layout from '../../components/layout/Layout'
import AdvancedSearch from '../../components/map_category/AdvancedSearch'
import MapCategoryList from '../../components/map_category/List'
import { Card } from 'material-ui/Card'
import { styles } from '../../style/Styles'
import RequestHandler from '../../lib/RequestHandler'
let requestHandlerObj = new RequestHandler();

class List extends React.Component {

  /**
   * Class constructor.
   */
  constructor(props, context) {
    super(props, context);

    const storedMessage = localStorage.getItem('successMessage');
    let successMessage = '';

    if (storedMessage) {
      successMessage = storedMessage;
      localStorage.removeItem('successMessage');
    }

    this.state = {

      imgRoot: Auth.getRestURL(),
      notifiactionType:'success',
      successMessage: successMessage,
      catList: [],
      arrData: [],
      categorySearch: {
        fk_category_id: '',
        fk_sub_category_name: '',
        status: true
      },
      pagging: {
        total: 0,
        display: Auth.perPageItem(),
        current: 0
      }
    };
    this.listData = this.listData.bind(this);
    this.categoryList = this.categoryList.bind(this);
    this.onSearch = this.onSearch.bind(this);
    this.filterChange = this.filterChange.bind(this);
    this.changeSelectField = this.changeSelectField.bind(this);
    this.onChangePage = this.onChangePage.bind(this);
    this.deleteAction = this.deleteAction.bind(this);
  }

  /**
  * This method will be executed after initial rendering.
  */
  componentDidMount() {
    this.listData();
    this.categoryList();
    if(this.state.successMessage)
    {
      notify.show(this.state.successMessage, this.state.notifiactionType, 5000, styles.notificationMsg)
    }
   }

  async listData() {
    let fk_category_id = this.state.categorySearch.fk_category_id,
      fk_sub_category_name = this.state.categorySearch.fk_sub_category_name,
      status = (this.state.categorySearch.status === '1' || this.state.categorySearch.status === 'true') ? 1 : 0,
      limit = this.state.pagging.display,
      currentPage = this.state.pagging.current;

    let dataObj = { fk_category_id: fk_category_id, fk_sub_category_name: fk_sub_category_name, status: status, limit: limit, currentPage: currentPage }

    let response = await requestHandlerObj.getRequest('post', '/mapping_category/list', dataObj)
    if (response.error) {
      this.setState({
        successMessage: response.error,
        notifiactionType:'error'
      })
    } else {
      let pagging = this.state.pagging;
      pagging['total'] = response.result.data.total
      this.setState({
        successMessage: response.result.data.message,
        notifiactionType:'success',
        arrData: response.result.data.arrData,
        pagging
      });
    }
  }
  async categoryList() {
    let response = await requestHandlerObj.getRequest('post', '/common/categoryList', { main_category: 1 })
    if (response.error) {
      this.setState({
        successMessage: response.error,
        notifiactionType:'error'
      })
    } else {
      this.setState({
        catList: response.result.data.arrData
      });
    }
  }
  onSearch(event) {
    // prevent default action. in this case, action is the form submission event
    event.preventDefault();
    this.listData();
  }

  filterChange(event, value) {
    const field = event.target.name;
    const categorySearch = this.state.categorySearch;
    categorySearch[field] = value;

    this.setState({
      categorySearch
    });
  }
  changeSelectField(event, index, value) {
    const categorySearch = this.state.categorySearch;
    categorySearch['fk_category_id'] = value;
    this.setState({
      categorySearch
    });
  }

  /* Paging is start here */
  onChangePage(event) {
    const pagging = this.state.pagging;
    pagging['current'] = event;

    this.setState({
      pagging
    });
    this.listData();
  }

  deleteAction(eventId) {

  //  const id = `id=${eventId}`;
  }

  /**
   * Render the component.
   */
  render() {
    return (
      <div>
        <MainHeader />
        <table style={styles.tableDefault}>
          <tbody>
            <tr>
              <td style={styles.tableLayout}>
                <Layout />
              </td>
              <td style={styles.tableContainer}>
                <Card>
                  <AdvancedSearch
                    categorySearch={this.state.categorySearch}
                    onChange={this.filterChange}
                    onChangeSelect={this.changeSelectField}
                    onSubmit={this.onSearch}
                    catList={this.state.catList}
                  />
                  <MapCategoryList
                    arrData={this.state.arrData}
                    imgRoot={this.state.imgRoot}
                    total={this.state.pagging.total}
                    perPageItem={this.state.pagging.display}
                    current={this.state.pagging.current}
                    onChangePage={this.onChangePage}
                    message={this.state.message}
                    deleteAction={this.deleteAction}
                  />
                </Card>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}
export default List;
