import React from 'react';
import 'babel-polyfill'
import {notify} from 'react-notify-toast'
import { Card } from 'material-ui/Card';
import MainHeader from '../../components/layout/MainHeader'
import Layout from '../../components/layout/Layout'
import RequestHandler from '../../lib/RequestHandler'
import Forms from '../../components/map_category/Forms'
import {styles} from "../../style/Styles"
import $ from 'jquery'

let requestHandlerObj = new RequestHandler();

class Create extends React.Component {

    /**
     * Class constructor.
     */
    constructor(props, context) {
      super(props, context);

      this.state = {
        notifiactionType:'success',
        successMessage:'',
        errors: {},
        catList: [],
        subCategoryList:[],
        listData: {
          fk_category_id:'',
          source:''
        },
       arrSubCategorySelected:[]
      }
      this.categoryList = this.categoryList.bind(this);
      this.processForm = this.processForm.bind(this);
      this.changeFieldValue =this.changeFieldValue.bind(this);
      this.onChangeFkCat = this.onChangeFkCat.bind(this);
      this.onChangeSource = this.onChangeSource.bind(this);
      this.getSubCategory = this.getSubCategory.bind(this);
    }
  
   async categoryList() {
        let response = await requestHandlerObj.getRequest('post', '/common/categoryList', {main_category:1})
        if (response.error) {
          this.setState({
            successMessage: response.error,
            notifiactionType:'error'
          })
        } else {
          this.setState({
          catList: response.result.data.arrData
        });
      } 
    }
    /**
     * This method will be executed after initial rendering.
     */
    async componentDidMount() {

       await this.categoryList()
    }
  
  
    async processForm(event){
  
      // prevent default action. in this case, action is the form submission event
      event.preventDefault();
      let fk_category_id = this.state.listData.fk_category_id,
       source =this.state.listData.source,
       fk_category_parent_id = fk_category_id,
       arrSubCategorySelected = this.state.arrSubCategorySelected;

       let objData ={
        fk_category_id:fk_category_id,
        source:source,
        fk_category_parent_id:fk_category_parent_id,
        arrSubcat:arrSubCategorySelected
       }
       let response = await requestHandlerObj.getRequest('post', '/mapping_category/create', objData)
       
       if(response.error)
        {
            this.setState({
                successMessage: response.error,
                notifiactionType:'error'
            }, () =>{
              notify.show(this.state.successMessage, this.state.notifiactionType, 5000, styles.notificationMsg)
            });
          }else{
            if(response.result.data.success)
            {
                localStorage.setItem('successMessage', response.result.data.message);
                this.props.history.replace('/admin/map-category');
            }else{
                this.setState({
                  successMessage: response.error,
                  notifiactionType:'error'
              },() =>{
                notify.show(this.state.successMessage, this.state.notifiactionType, 5000, styles.notificationMsg)
              });
            }
        } 
    }
  
    changeFieldValue(event, value) {
      const ID = event.target.id;
      const Name = event.target.name;
      let check = $("#"+ID).is(":checked");
     const arrSubCategorySelected = this.state.arrSubCategorySelected;
  
     if(check) {
       const temp = {id:ID, name:Name};
       arrSubCategorySelected.push(temp);
     } else {
       for(var i in arrSubCategorySelected)
         {
           if(arrSubCategorySelected[i].id === ID)
           {
             arrSubCategorySelected.splice(i, 1);
           }
         }
       }
       this.setState({
         arrSubCategorySelected
       });
      //  console.log("Arr State =>",this.state.arrSubCategorySelected);
    }
  
  onChangeFkCat(event, index, value) {
      const listData = this.state.listData;
      listData['fk_category_id'] = value;
      this.setState({
        listData
      });
      this.getSubCategory()
    }
  
    onChangeSource(event, index, value) {
        const listData = this.state.listData;
        listData['source'] = value;
        this.setState({
          listData
        });
        this.getSubCategory()
      }
  
  async getSubCategory() {
   
      let source = this.state.listData.source,
          fk_category_id = this.state.listData.fk_category_id;

      if(source && fk_category_id)
      {
        let objData ={
            source:source,
            fk_category_id:fk_category_id
        }
        let response = await requestHandlerObj.getRequest('post', '/common/subcategoryList', objData)        
        const arrSubCategorySelected = this.state.arrSubCategorySelected;
        let arrDataSet = response.result.data.arrData;
        if(arrDataSet.length >0)
        {
          arrDataSet.forEach(elem => {
            if(elem.mapped)
            {
              var tempVar = {id:elem._id, name:elem.name};
              arrSubCategorySelected.push(tempVar);
            }
          });
        }else{
          arrSubCategorySelected.push([]);
        }
        this.setState({
          arrSubCategorySelected,
          subCategoryList: arrDataSet
        });
      }else{
        this.setState({
          arrSubCategorySelected:[],
          subCategoryList: []
        });
      }
    }
    /**
     * Render the component.
     */
    render() {
      return (
        <div>
        <MainHeader/>
        <table style={styles.tableDefault}>
            <tbody>
            <tr>
                <td style={styles.tableLayout}>
                    <Layout/>
                </td>
                <td style={styles.tableContainer}>
                    <Card>
                    <Forms
                    onSubmit={this.processForm}
                    onChange={this.changeFieldValue}
                    onChangeFkCat={this.onChangeFkCat}
                    onChangeSource={this.onChangeSource}
                    catList={this.state.catList}
                    arrData={this.state.listData}
                    subCategoryList={this.state.subCategoryList}
                    onChangeSubCategory={this.onChangeSubCategory}
                    errors={this.state.errors}
                    mainTitle="Mapping Category"
                    btnText="Update"
                    />
                    </Card>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    );
   }
  }
  export default Create;
