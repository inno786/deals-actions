import React from 'react'
import 'babel-polyfill'
import {notify} from 'react-notify-toast'
import Auth from '../../modules/Auth'
import MainHeader from '../../components/layout/MainHeader'
import Layout from '../../components/layout/Layout'
import AdvancedSearch from '../../components/product/AdvancedSearch'
import ProductList from '../../components/product/List'
import { Card} from 'material-ui/Card'
import {styles} from '../../style/Styles'
import RequestHandler from '../../lib/RequestHandler'
let requestHandlerObj = new RequestHandler();

class List extends React.Component {

  /**
   * Class constructor.
   */
  constructor(props, context) {
    super(props, context);

    const storedMessage = localStorage.getItem('successMessage');
    let successMessage = '';

    if (storedMessage) {
      successMessage = storedMessage;
      localStorage.removeItem('successMessage');
    }

    this.state ={

      imgRoot: Auth.getRestURL(),
      notifiactionType:'success',
      successMessage:successMessage,
      arrData: [],
      arrSearch: {
        name: '',
        vendor:'',
        status: ''
      },
      pagging:{
        total:0,
        display: Auth.perPageItem(),
        current:0
      }
    };
    this.listData =this.listData.bind(this);
    this.filterChange =this.filterChange.bind(this);
    this.onSearch =this.onSearch.bind(this);
    this.changeVendor = this.changeVendor.bind(this);
    this.changeStatus = this.changeStatus.bind(this);
    this.onChangePage =this.onChangePage.bind(this);

  }

  async listData()
  {
    let limit = this.state.pagging.display,
     currentPage = this.state.pagging.current,
     name = this.state.arrSearch.name,
     vendor = this.state.arrSearch.vendor,
     status = this.state.arrSearch.status;

      let dataObj ={
        limit:limit,
        currentPage:currentPage,
        name:name, 
        vendor:vendor, 
        status:status
      }
      let response =  await requestHandlerObj.getRequest('post', '/products/list', dataObj)
      if(response.error)
      {
          this.setState({
              successMessage: response.error,
              notifiactionType:'error'
          })
      }else{
          let pagging = this.state.pagging;
          pagging['total'] = response.result.data.total
          this.setState({
              successMessage: response.result.data.message,
              notifiactionType:'success',
              arrData: response.result.data.arrData,
              pagging
          });
      }
  }

  /**
   * This method will be executed after initial rendering.
   */
  componentDidMount() {
    this.listData();
        if(this.state.successMessage){
         notify.show(this.state.successMessage, this.state.notifiactionType, 5000, styles.notificationMsg);
      }
  }
  onSearch(event){
    // prevent default action. in this case, action is the form submission event
    event.preventDefault();
    this.listData();
  }

  filterChange(event, value) {
    const field = event.target.name;
    const arrSearch = this.state.arrSearch;
    arrSearch[field] = value;

    this.setState({
      arrSearch
    });
  }

  changeVendor(event, index, value) {

    const arrSearch = this.state.arrSearch;
    arrSearch['vendor'] = value;
    this.setState({
      arrSearch
    });
  }

  changeStatus(event, index, value) {

    const arrSearch = this.state.arrSearch;
    arrSearch['status'] = value;
    this.setState({
      arrSearch
    });
  }

/* Paging is start here */
onChangePage(event) {
  const pagging = this.state.pagging;
  pagging['current'] = event;

  this.setState({
    pagging
  });
  this.listData();
  }

  /**
   * Render the component.
   */
  render() {
    return (
        <div>
          <MainHeader/>
          <table style={styles.tableDefault}>
            <tbody>
            <tr>
              <td style={styles.tableLayout}>
                <Layout/>
              </td>
              <td style={styles.tableContainer}>
                <Card>
                  <AdvancedSearch
                    onSubmit={this.onSearch}
                    arrSearch={this.state.arrSearch}
                    onChange={this.filterChange}
                    changeVendor={this.changeVendor}
                    changeStatus={this.changeStatus}
                  />
                  <ProductList
                    arrData={this.state.arrData}
                    total={this.state.pagging.total}
                    perPageItem={this.state.pagging.display}
                    current={this.state.pagging.current}
                    onChangePage={this.onChangePage}
                  />
                </Card>
              </td>
            </tr>
            </tbody>
          </table>
        </div>
    );
 }
}
export default List;
