import React from 'react';
import { connect } from 'react-redux'
import { Card } from 'material-ui/Card';
import MainHeader from '../../components/layout/MainHeader'
import Layout from '../../components/layout/Layout'
import RequestHandler from '../../lib/RequestHandler'
import Forms from '../../components/attribute/Forms'
import { styles } from "../../style/Styles"
import Auth from '../../modules/Auth'
import {
    onChange,
    handleFile,
    onChangeSeo,
    changeTopDesc,
    changeBottomDesc,
    validate,
    onUpdateMsg,
    updateInitialData
} from '../../actions/attribute'
let requestHandlerObj = new RequestHandler();

class Update extends React.Component {

    /**
     * Class constructor.
     */
    constructor(props, context) {
        super(props, context);

        let urlParams = new URLSearchParams(window.location.search)
        this.state = {
            id: urlParams.has('id') ? urlParams.get('id') : null
        }
        this.onSubmit = this.onSubmit.bind(this)
    }

    async componentWillMount() {
        let response = await requestHandlerObj.getRequest('post', '/attribute/findByPk', { id: this.state.id })
        if (!response.error) {
            this.props.updateInitialData(response.result.data)
        }
    }

    async onSubmit(event) {

        event.preventDefault();

        let dataObj = {
            _id: this.state.id,
            name: this.props.arrData.field.name,
            display_name: this.props.arrData.field.display_name,
            status: this.props.arrData.field.status ? 1 : 0,
            image: this.props.arrData.field.image,
            keyword: this.props.arrData.seo.keyword,
            top_description: this.props.arrData.seo.top_description,
            bottom_description: this.props.arrData.seo.bottom_description
        }
        let response = await requestHandlerObj.getRequest('post', '/attribute/update', dataObj)
        if (response.error) {
            this.props.onUpdateMsg(response.error)
        } else {
            if (response.result.data.success) {
                Auth.authenticateUser('successMessage', response.result.data.message)
                this.props.history.push('/admin/attribute')
            } else {
                this.props.validate(response.result.data)
            }
        }
    }

    /**
     * Render the component.
     */
    render() {
        return (
            <div>
                <MainHeader />
                <table style={styles.tableDefault}>
                    <tbody>
                        <tr>
                            <td style={styles.tableLayout}>
                            <Layout />
                            </td>
                            <td style={styles.tableContainer}>
                                <Card>
                                    <Forms
                                        onSubmit={this.onSubmit}
                                        onChange={this.props.onChange}
                                        onChangeSeo={this.props.onChangeSeo}
                                        changeTopDesc={this.props.changeTopDesc}
                                        changeBottomDesc={this.props.changeBottomDesc}
                                        handleFile={this.props.handleFile}
                                        arrData={this.props.arrData}
                                        mainTitle="Update Attribute"
                                        btnText="Update"
                                    />
                                </Card>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        );
    }
}


const mapStateToProps = (state, props) => {
    return {
        arrData: state.attributeHandler
    }
}

export default connect(
    mapStateToProps,
    { onChange, onChangeSeo, changeTopDesc, changeBottomDesc, handleFile, validate, onUpdateMsg, updateInitialData }
)(Update)
