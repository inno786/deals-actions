import React from 'react';
import MainHeader from '../../components/layout/MainHeader'
import Layout from '../../components/layout/Layout'
import Dashboard from '../../components/dashboard/Main'
import {styles} from '../../style/Styles'

class Content extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.state = {}
    }


    /**
     * Render the component.
     */
    render() {
        return (
            <div>
                <MainHeader/>
                <table style={styles.tableDefault}>
                    <tbody>
                    <tr>
                        <td style={styles.tableLayout}>
                            <Layout/>
                        </td>
                        <td style={styles.tableContainer}>
                            <Dashboard/>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        );
    }
}

export default Content;
