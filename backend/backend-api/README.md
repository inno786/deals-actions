
Deals Action Backend API
======================================

### introduction
This is a backend application developed using Nodejs, Mongodb. This app is only used for backend API.

### How to run
Run following commands
<pre><code>cd app
npm install
npm start</code></pre>


## Environment variables
Environment variables is the main mechanism of manipulating application settings. Currently application recognizes
following environment variables:

| Variable             | Default value | Description              |
| -------------------- | ------------- | ------------------------ |
| HOST                 | 0.0.0.0       | Address to listen on     |
| PORT                 | 3500          | Port to listen on        |