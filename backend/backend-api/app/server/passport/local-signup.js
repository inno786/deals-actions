const dateFormat = require('dateformat');
const User = require('mongoose').model('User');
const PassportLocalStrategy = require('passport-local').Strategy;


/**
 * Return the Passport Local Strategy object.
 */
module.exports = new PassportLocalStrategy({
  usernameField: 'email',
  passwordField: 'password',
  session: false,
  passReqToCallback: true
}, (req, email, password, done) => {
   const now = new Date();
   const current_datetime =dateFormat(now, "yyyy-mm-dd H:MM:ss");
  const userData = {
    name: req.body.name.trim(),
    email: email.trim(),
    password: password.trim(),
    mobile: req.body.mobile.trim(),
    role: req.body.role.trim(),
    status: req.body.status.trim(),
    created_on:current_datetime,
    updated_on:current_datetime    
  };

  const newUser = new User(userData);
  newUser.save((err) => {
    if (err) { return done(err); }

    return done(null);
  });
});
