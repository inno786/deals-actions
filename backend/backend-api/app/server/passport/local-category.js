const dateFormat = require('dateformat');
const Category = require('mongoose').model('Category');
const Seo = require('mongoose').model('Seo');
const PassportLocalStrategy = require('passport-local').Strategy;

/**
 * Return the Passport Local Strategy object.
 */

module.exports = new PassportLocalStrategy({
  usernameField: 'email',
  passwordField: 'password',
  session: false,
  passReqToCallback: true
}, (req, email, password, done) => {
   const now = new Date();
   const current_datetime =dateFormat(now, "yyyy-mm-dd H:MM:ss");
  const arrData = {
    name: req.body.name.trim(),
    slug: req.body.name.trim(),
    description: req.body.description.trim(),
    image: req.body.image.trim(),
    status: req.body.status.trim(),
    created_on:current_datetime,
    updated_on:current_datetime    
  };
 // return done(arrData);
  console.log(arrData);
  //return done(arrData);

 const newCategory = new Category(arrData);
  newCategory.save((err) => {
    if (err) { return done(err) }

    return done(null);
  });
});
