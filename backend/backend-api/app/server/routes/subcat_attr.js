const express = require('express');
const mongoose = require('mongoose');
const dateFormat = require('dateformat');
const Attribute = require('mongoose').model('Attribute');
const SubCatAttribute = require('mongoose').model('SubCatAttribute');
const async = require('node-async-loop');
const router = new express.Router();

/**
 * Validate the sign up form
 *
 * @param {object} payload - the HTTP body message
 * @returns {object} The result of validation. Object contains a boolean validation result,
 *                   errors tips, and a global message for the whole form.
 */
function validateForm(payload) {
  const errors = {};
  let isFormValid = true;
  let message = '';

  if (payload.fk_subcat_id =='') {
    isFormValid = false;
    errors.name = 'Please select subcategory';
  }

  if (!isFormValid) {
    message = 'Please fixed error.';
  }

  return {
    success: isFormValid,
    message,
    errors
  };
}

function jsUcfirst(string)
{
    return string.replace(/\b[a-z]/g,function(f){return f.toUpperCase();});
}

function objectFindByKey(array, key, value) {
    for (var i = 0; i < array.length; i++) {
        if (array[i][key] === value) {
            return array[i];
        }
    }
    return null;
}

router.post('/create', (req, res) => {

  let reqBody = req.body;
  let arrAttr =JSON.parse(reqBody.arrAttr);

  const validationResult = validateForm(reqBody);
  if (!validationResult.success) {
    return res.status(400).json({
      success: false,
      message: validationResult.message,
      errors: validationResult.errors
    });
  }else{
    const now = new Date();
    const current_datetime =dateFormat(now, "yyyy-mm-dd HH:MM:ss");
    if(arrAttr.length >0)
    {
     SubCatAttribute.remove({fk_subcat_id:reqBody.fk_subcat_id}).exec(function(errRemove, arrRemove){
      // console.log("Remove Msg =>",errRemove);
      async(arrAttr, function (elem, callback){
        // body...
        var arrData = {
          fk_subcat_id: reqBody.fk_subcat_id.trim(),
          fk_attribute_id: elem.id,
          fk_attribute_display_name: elem.name,
          status: 1,
          created_on:current_datetime,
          updated_on:current_datetime
        };
      //  console.log(arrData);
        var model = new SubCatAttribute(arrData);
        model.save((err, resultSet) => {
          if (err) {
              return res.status(400).json({
              success: false,
              message: 'Could not process the form.',
              errors:{}
              });
          }else{
            callback();
          }
      });
      },function(err){
					if(err){
            return res.status(400).json({
            success: false,
            message: 'Error occurred during update',
            errors:{}
            });
					}else{
            return res.status(200).json({
               success: true,
               message: 'Record has been inserted successfully.',
               errors:{}
             });
					}
				});
      });
    }else{
      return res.status(400).json({
      success: false,
      message: 'Please select atleat one subcategory.',
      errors:{}
      });
    }
  }
});

// Category list
router.post('/list', (req, res) => {

  let reqData = req.body;
  let limit = parseInt(reqData.limit);
  let currentPage = parseInt(reqData.currentPage);
  if(currentPage >0)
  {
    currentPage = currentPage-1;
  }
  let skip = parseInt(limit * currentPage);

  let promises = SubCatAttribute.find().populate({ path: 'fk_subcat_id', select:'_id name'}).populate({ path: 'fk_attribute_id', select: 'display_name' }).skip(skip).limit(limit);

  if(reqData.fk_subcat_id && reqData.fk_subcat_id !="")
  {
    promises.where('fk_subcat_id').equals(reqData.fk_subcat_id);
  }
  if(reqData.status && reqData.status !="")
  {
    promises.where('status').equals(reqData.status);
  }
  //exec promisses
  promises.exec(function(err, resultSet){
     if(err)
      {
        return res.status(409).json({
          message: err,
          arrData:[]
        });
      }else{
      let arrResultSet =[];
      if(resultSet.length >0)
      {
      async(resultSet, function (elem, callback){
         var checkExist = objectFindByKey(arrResultSet, 'fk_subcat_id', elem.fk_subcat_id._id);
         if(checkExist)
          {
            checkExist.attr.push(elem.fk_attribute_id.display_name);
          }else{
              var temp ={
                fk_subcat_id:elem.fk_subcat_id._id,
                subcat:elem.fk_subcat_id.name,
                attr:[elem.fk_attribute_id.display_name]
              };
              arrResultSet.push(temp);
            }
          callback();
        },function(err){
          if(err)
          {
        /*    return res.status(400).json({
            success: false,
            message: 'Please Fixed error',
            errors:{}
          }); */
          }else{
            let totalObj = parseInt(arrResultSet.length);
            return res.status(200).json({
              message: '',
              arrData:arrResultSet,
              total: Math.ceil(totalObj/limit)
            });
          }
        });
      }
      }
    });
});
module.exports = router;
