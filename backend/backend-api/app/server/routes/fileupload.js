const express = require('express');
var crypto = require('crypto');
var fs = require('fs');
var  path = require('path');

const router = new express.Router();

router.post('/upload', (req, res) => {
    if (req.method === 'POST') {
    var fileExtension = [".jpg", ".jpeg", ".png", ".gif"];
    var busboy = req.busboy;
    req.pipe(busboy);
    busboy.on('file', function (fieldname, file, filename, encoding, mimetype) {
        var ack ='';
        var msg ='';
        var random_string = fieldname + filename + Date.now() + Math.random();
        newFilename = crypto.createHash('md5').update(random_string).digest('hex');
        var ext = path.extname(filename).toLowerCase();
        if (ext != "")
        {
            if (fileExtension.indexOf(ext) != -1)
            {
                newFilename = newFilename + ext;
                image =newFilename;
                fstream = fs.createWriteStream(rootPath + '/public/images/' + newFilename);
                file.pipe(fstream);
                ack ='OK';
                msg ="File Upload successfully";
            } else {
                ack ='Error';
                msg ="Invalid File";
            }
        } else {
            ack ='Error';
            msg ="Please select valid file";
        }
        arr = {ack:ack,msg:msg,file_name:newFilename};
        res.status(200).json(arr);
    });
  }
});


module.exports = router;
