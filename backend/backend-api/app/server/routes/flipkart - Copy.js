const express = require('express');
const mongoose = require('mongoose');
const dateFormat = require('dateformat');
const async = require('node-async-loop');
const replaceall = require("replaceall");
const isset = require('isset');

const FlipkartProduct = require('mongoose').model('FlipkartProduct');
const FlipKartCategory = require('mongoose').model('FlipKartCategory');
const Product = require('mongoose').model('Product');
const Category = require('mongoose').model('Category');
const SubCategory = require('mongoose').model('SubCategory');
const Attribute = require('mongoose').model('Attribute');
const AttributeValue = require('mongoose').model('AttributeValue');
const ObjectTaxonomy = require('mongoose').model('ObjectTaxonomy');

const router = new express.Router();


function jsUcfirst(string)
{
    return string.replace(/\b[a-z]/g,function(f){return f.toUpperCase();});
}
function getWords(str) {
    var splitStr = str.split(' ').slice(0, 20).join(' ');
    return splitStr;
}

// Category list
router.get('/import', (req, res) => {

  FlipkartProduct.findOne({status:'inprogress'}, function(err, arrData){
  if(err)
  {
    console.log("Process already running Error => ",JSON.stringify(err));
    return res.status(200).json({
      message: JSON.stringify(err)
    });
  }else{
    if(arrData){
      console.log("Process already running");
      return res.status(200).json({
        message: "Process already running"
      });
    }else{
      FlipkartProduct.findOne({status:'ready'}, function(errReady, arrDataReady){
      if(errReady)
      {
        console.log("Resource featching Error => ",JSON.stringify(errReady));
        return res.status(200).json({
          message: JSON.stringify(errReady)
        });
      }else{
        if(arrDataReady)
        {
          const now = new Date();
          const current_datetime =dateFormat(now, "yyyy-mm-dd HH:MM:ss");
          const objUpdate = {
            status: 'inprogress',
            updated_on:current_datetime
          };
        FlipkartProduct.update({_id:arrDataReady._id},{$set :objUpdate},(errUpdate, resultSetUpdate) => {
        if(errUpdate)
        {
          return res.status(200).json({
            message: JSON.stringify(errUpdate)
          });
        }else
        {
          // Do here coding...
          let json2Array = JSON.parse(arrDataReady.raw_data);
          async(json2Array.productInfoList, function (items, callback){

            var inputData = items.productBaseInfoV1;

            if(inputData.hasOwnProperty('attributes'))
            {
              inputData.attributes.brands = inputData.productBrand;
            }
            let attributes = inputData.attributes;
            let arrAttributes = [];
            for (var key in attributes) {
              if (attributes.hasOwnProperty(key)) {
                //console.log(key + " -> " + attributes[key]);
                if(attributes[key] !='')
                {
                  arrAttributes.push({key:key, value:attributes[key]});
                }
                //arrAttributes.push({key:key, value:attributes[key]});
              }
            }
          //  console.log("Arr Attr =>",arrAttributes);

            if(inputData.productId)
            {
              Product.findOne({product_id: inputData.productId}, function(errProduct, arrProduct){
                if(errProduct)
                {
                  console.log("Error has been occured on product check =>",JSON.stringify(errProduct));
                }else{
                  if(arrProduct)
                  {
                    //update here
                    var amount = arrProduct.price;
                    if(inputData.hasOwnProperty('flipkartSpecialPrice'))
                    {
                      if(inputData.flipkartSpecialPrice)
                      {
                        amount = inputData.flipkartSpecialPrice.amount;
                      }
                    }
                    var objProdUpdate =
                    {
                      feed_price: amount,
                      price:amount,
                      stock:(inputData.hasOwnProperty('inStock') && inputData.inStock) ? 1:0,
                      updated_on:current_datetime
                    };
                    Product.update({_id:arrProduct._id},{$set :objProdUpdate},(errUpdateProd, resultSetUpdateProd) => {
                    if(errUpdateProd)
                    {
                      console.log("Error has been occured on product update =>",JSON.stringify(errUpdateProd));
                    }else{
                      callback();
                     }
                   });
                  }else{
                    // insert here
                    var name = inputData.title;
                    var replaceSpecialChar = name.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
                    var replaceSpace =replaceSpecialChar.replace(/  +/g, ' ');
                    var splitString = replaceSpace.split(' ').slice(0, 20).join(' ');
                    var slug =replaceall(" ", "-", splitString).toLowerCase();

                    Product.findOne({slug: slug, parent_id:null}, function(errDuplicateProduct, arrDuplicateProduct){

                      var parent_id = null;
                      var status = 'published';
                      if(arrDuplicateProduct)
                      {
                        parent_id = arrDuplicateProduct.product_id;
                        status = 'inherit';
                      }
                      var amount =0;
                      if(inputData.hasOwnProperty('flipkartSpecialPrice'))
                      {
                        if(inputData.flipkartSpecialPrice)
                        {
                          amount = inputData.flipkartSpecialPrice.amount;
                        }
                      }
                      var temp ={
                         product_id: inputData.hasOwnProperty('productId') ? inputData.productId:'',
                         vendor:'Flipkart',
                         name: inputData.hasOwnProperty('title') ? inputData.title:'',
                         slug:slug,
                         description: inputData.hasOwnProperty('productDescription') ? inputData.productDescription:'',
                         feed_price: amount,
                         price: amount,
                         product_url: inputData.hasOwnProperty('productUrl') ? inputData.productUrl.split('&affid=indarbiha').slice(0).join(''):'',
                         small_image: inputData.hasOwnProperty('imageUrls') ? inputData.imageUrls['200x200']:'',
                         large_image: inputData.hasOwnProperty('imageUrls') ? inputData.imageUrls['800x800']:'',
                         stock: (inputData.hasOwnProperty('inStock') && inputData.inStock) ? 1:0,
                         parent_id: parent_id,
                         status: status,
                         visitors:1,
                         created_on: current_datetime,
                         updated_on: current_datetime
                      };
                      var productObj = new Product(temp);
                      productObj.save((errProductInsert, arrProductInsert) => {
                        if(errProductInsert)
                        {
                          console.log("Error has been occured on product insert =>",JSON.stringify(errProductInsert));
                        }else{
                          // Do here Category and attribute
                          // Add Attributes
                          if(inputData.hasOwnProperty('attributes'))
                          {
                            inputData.attributes.brands = inputData.productBrand;
                          }

                          // Add Category
                          var arrCategory = inputData.categoryPath.split('>');

                          if(arrCategory.length >0)
                          {
                            Category.findOne({search_index:arrCategory[0]}, function(errCat, objCat){
                              if(errCat)
                              {
                                console.log("Error has been occured on Category check =>",JSON.stringify(errCat));
                              }else{
                                if(objCat)
                                {
                                  // Parent category already exists
                                  arrCategory.shift(); // skip parent category
                                  if(arrCategory.length >0)
                                  {
                                    async(arrCategory, function (itemsCategory, callbackCategory){

                                      SubCategory.findOne({fk_category_id: objCat._id, feed_name:itemsCategory}, function(errSubCat, resultSubCat){
                                        if(errSubCat)
                                        {
                                          console.log("Error occure during SubCategory =>",JSON.stringify(errSubCat));
                                        }else{
                                          if(resultSubCat){
                                            // Check in ObjectTaxonomy
                                            var tempObj = {
                                              fk_product_id:arrProductInsert._id,
                                              fk_sub_category_id:resultSubCat._id,
                                              fk_attribute_value_id:resultSubCat._id,
                                              taxonomy:"Category",
                                              status:1,
                                              source:'Flipkart',
                                              created_on:arrProductInsert.created_on,
                                              updated_on:arrProductInsert.updated_on
                                            };
                                            var objTaxo = new ObjectTaxonomy(tempObj);
                                            objTaxo.save((errObjInsert, resultObjInsert) => {
                                              if(errObjInsert)
                                              {
                                                console.log("Error occure during ObjectTaxonomy insert =>",JSON.stringify(errObjInsert));
                                              }else{
                                                  // Saved subcat into tempObjTaxono
                                              }
                                            });
                                          }else{
                                            // insert SubCategory
                                            var replaceSpecialChar = itemsCategory.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
                                            var replaceSpace =replaceSpecialChar.replace(/  +/g, ' ');
                                            var slug =replaceall(" ", "-", replaceSpace).toLowerCase();

                                            var tempSubCategory = {
                                              fk_category_id:objCat._id,
                                              name: itemsCategory,
                                              slug: slug,
                                              feed_name:itemsCategory,
                                              description:'',
                                              image: arrProductInsert.small_image,
                                              status:1,
                                              source:'Flipkart',
                                              created_on:arrProductInsert.created_on,
                                              updated_on:arrProductInsert.updated_on
                                              };
                                          var subCatObj = new SubCategory(tempSubCategory);
                                          subCatObj.save((errSubCatInsert, resultSubCatInsert) => {
                                            if(errSubCatInsert)
                                            {
                                              console.log("Error occure during SubCategory insert =>",JSON.stringify(errSubCatInsert));
                                            }else{
                                              var tempObj = {
                                                fk_product_id:arrProductInsert._id,
                                                fk_sub_category_id:resultSubCatInsert._id,
                                                fk_attribute_value_id:resultSubCatInsert._id,
                                                taxonomy:"Category",
                                                status:1,
                                                source:'Flipkart',
                                                created_on:arrProductInsert.created_on,
                                                updated_on:arrProductInsert.updated_on
                                              };
                                              var objTaxo = new ObjectTaxonomy(tempObj);
                                              objTaxo.save((errObjInsert, resultObjInsert) => {
                                                if(errObjInsert)
                                                {
                                                  console.log("Error occure during ObjectTaxonomy insert =>",JSON.stringify(errObjInsert));
                                                }else{
                                                  // Saved subcat into tempObjTaxono
                                                }
                                              });

                                             }
                                           });
                                          }
                                          callbackCategory();
                                        }
                                      });
                                    },function(endOfSubCat){
                                      if(endOfSubCat)
                                      {
                                        console.log("Error occure during endOfSubCat =>",JSON.stringify(endOfSubCat));
                                      }else{
                                        if(arrAttributes.length >0)
                                        {
                                          async(arrAttributes, function (attrItems, callbackAttributes){

                                            Attribute.findOne({name:attrItems.key},function(errBrands, resultBrands){
                                              if(errBrands)
                                              {
                                                console.log("Error occure during Attributes featching =>",JSON.stringify(errBrands));
                                              }else{
                                                if(resultBrands)
                                                {
                                                  AttributeValue.findOne({fk_attribute_id:resultBrands._id, name:{'$regex': '^'+attrItems.value, '$options' : 'i'}}, function(errExistAttrVal, resultExistAttrVal){
                                                    if(errExistAttrVal)
                                                    {
                                                      console.log("Error occure during Brands Value checking =>",JSON.stringify(errExistAttrVal));
                                                    }else{
                                                      if(resultExistAttrVal)
                                                      {
                                                        // insert ObjTaxonomy
                                                        var tempObjTaxono = {
                                                          fk_product_id:arrProductInsert._id,
                                                          fk_sub_category_id:resultExistAttrVal._id,
                                                          fk_attribute_value_id:resultExistAttrVal._id,
                                                          taxonomy:"Attribute",
                                                          status:1,
                                                          source:'Flipkart',
                                                          created_on:arrProductInsert.created_on,
                                                          updated_on:arrProductInsert.updated_on
                                                        };
                                                        var objTaxo = new ObjectTaxonomy(tempObjTaxono);
                                                        objTaxo.save((errObjTaxoInsert, resultObjTaxoInsert) => {
                                                          if(errObjTaxoInsert)
                                                          {
                                                            console.log("Error occure during ObjectTaxonomy insert =>",JSON.stringify(errObjTaxoInsert));
                                                          }else{
                                                          //  console.log("ObjTaxonomy has been insert successfully.");
                                                           callbackAttributes();
                                                          }
                                                        });
                                                      }else{
                                                        // Insert AttributeValue
                                                        var replaceSpecialChar = attrItems.value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
                                                        var replaceSpace =replaceSpecialChar.replace(/  +/g, ' ');
                                                        var slug =replaceall(" ", "-", replaceSpace).toLowerCase();

                                                        var tempAttrValue = {
                                                          fk_attribute_id: resultBrands._id,
                                                          display_name: attrItems.value,
                                                          name: attrItems.value,
                                                          slug: slug,
                                                          parent_id:null,
                                                          image: arrProductInsert.small_image,
                                                          status:1,
                                                          created_on:arrProductInsert.created_on,
                                                          updated_on:arrProductInsert.updated_on
                                                        };
                                                        var attrValue = new AttributeValue(tempAttrValue);
                                                        attrValue.save((errAttrValueInsert, resultAttrValueInsert) => {
                                                          if(errAttrValueInsert)
                                                          {
                                                            console.log("Error occure during Attr Value insert =>",JSON.stringify(errAttrValueInsert));
                                                          }else{
                                                            // insert ObjTaxonomy
                                                            var tempObjTaxono = {
                                                              fk_product_id:arrProductInsert._id,
                                                              fk_sub_category_id:resultAttrValueInsert._id,
                                                              fk_attribute_value_id:resultAttrValueInsert._id,
                                                              taxonomy:"Attribute",
                                                              status:1,
                                                              source:'Flipkart',
                                                              created_on:arrProductInsert.created_on,
                                                              updated_on:arrProductInsert.updated_on
                                                            };
                                                            var objTaxo = new ObjectTaxonomy(tempObjTaxono);
                                                            objTaxo.save((errObjTaxoInsert, resultObjTaxoInsert) => {
                                                              if(errObjTaxoInsert)
                                                              {
                                                                console.log("Error occure during ObjectTaxonomy insert =>",JSON.stringify(errObjTaxoInsert));
                                                              }else{
                                                              //  console.log("ObjTaxonomy has been insert successfully.");
                                                               callbackAttributes();
                                                              }
                                                            });
                                                          }
                                                        });
                                                      }
                                                    }
                                                  });
                                                }else{
                                                  // Insert attributes
                                                  var display_name = attrItems.key;
                                                  display_name = display_name.replace(/([a-z])([A-Z])/g, '$1 $2');
                                                  display_name = display_name.charAt(0).toUpperCase() + display_name.slice(1);
                                                  var tempAttribute = {
                                                    name:attrItems.key,
                                                    display_name:display_name,
                                                    image: arrProductInsert.small_image,
                                                    status:1,
                                                    created_on:arrProductInsert.created_on,
                                                    updated_on:arrProductInsert.updated_on
                                                  };
                                                  var objAttr = new Attribute(tempAttribute);
                                                  objAttr.save((errBrandsInsert, resultBrands) => {
                                                    if(errBrandsInsert)
                                                    {
                                                      console.log("Error occure during Brand insert =>",JSON.stringify(errBrandsInsert));
                                                    }else{
                                                        AttributeValue.findOne({fk_attribute_id:resultBrands._id, name:{'$regex': '^'+attrItems.value, '$options' : 'i'}}, function(errExistAttrVal, resultExistAttrVal){
                                                          if(errExistAttrVal)
                                                          {
                                                            console.log("Error occure during Brands Value checking =>",JSON.stringify(errExistAttrVal));
                                                          }else{
                                                            if(resultExistAttrVal)
                                                            {
                                                              // insert ObjTaxonomy
                                                              var tempObjTaxono = {
                                                                fk_product_id:arrProductInsert._id,
                                                                fk_sub_category_id:resultExistAttrVal._id,
                                                                fk_attribute_value_id:resultExistAttrVal._id,
                                                                taxonomy:"Attribute",
                                                                status:1,
                                                                source:'Flipkart',
                                                                created_on:arrProductInsert.created_on,
                                                                updated_on:arrProductInsert.updated_on
                                                              };
                                                              var objTaxo = new ObjectTaxonomy(tempObjTaxono);
                                                              objTaxo.save((errObjTaxoInsert, resultObjTaxoInsert) => {
                                                                if(errObjTaxoInsert)
                                                                {
                                                                  console.log("Error occure during ObjectTaxonomy insert =>",JSON.stringify(errObjTaxoInsert));
                                                                }else{
                                                                //  console.log("ObjTaxonomy has been insert successfully.");
                                                                 callbackAttributes();
                                                                }
                                                              });
                                                            }else{
                                                              // Insert AttributeValue
                                                              var replaceSpecialChar = attrItems.value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
                                                              var replaceSpace =replaceSpecialChar.replace(/  +/g, ' ');
                                                              var slug =replaceall(" ", "-", replaceSpace).toLowerCase();

                                                              var tempAttrValue = {
                                                                fk_attribute_id: resultBrands._id,
                                                                display_name: attrItems.value,
                                                                name: attrItems.value,
                                                                slug: slug,
                                                                parent_id:null,
                                                                image: arrProductInsert.small_image,
                                                                status:1,
                                                                created_on:arrProductInsert.created_on,
                                                                updated_on:arrProductInsert.updated_on
                                                              };
                                                              var attrValue = new AttributeValue(tempAttrValue);
                                                              attrValue.save((errAttrValueInsert, resultAttrValueInsert) => {
                                                                if(errAttrValueInsert)
                                                                {
                                                                  console.log("Error occure during Attr Value insert =>",JSON.stringify(errAttrValueInsert));
                                                                }else{
                                                                  // insert ObjTaxonomy
                                                                  var tempObjTaxono = {
                                                                    fk_product_id:arrProductInsert._id,
                                                                    fk_sub_category_id:resultAttrValueInsert._id,
                                                                    fk_attribute_value_id:resultAttrValueInsert._id,
                                                                    taxonomy:"Attribute",
                                                                    status:1,
                                                                    source:'Flipkart',
                                                                    created_on:arrProductInsert.created_on,
                                                                    updated_on:arrProductInsert.updated_on
                                                                  };
                                                                  var objTaxo = new ObjectTaxonomy(tempObjTaxono);
                                                                  objTaxo.save((errObjTaxoInsert, resultObjTaxoInsert) => {
                                                                    if(errObjTaxoInsert)
                                                                    {
                                                                      console.log("Error occure during ObjectTaxonomy insert =>",JSON.stringify(errObjTaxoInsert));
                                                                    }else{
                                                                    //  console.log("ObjTaxonomy has been insert successfully.");
                                                                     callbackAttributes();
                                                                    }
                                                                  });
                                                                }
                                                              });
                                                            }
                                                          }
                                                        });
                                                      }
                                                  });
                                                }
                                              }



                                            });
                                          },function(endOfAttr){
                                             callback();
                                          });
                                        }
                                      }
                                      //callback();
                                    });
                                  }
                                }else{
                                  // Parent category no exists
                                  var replaceSpecialChar = arrCategory[0].replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
                                  var replaceSpace =replaceSpecialChar.replace(/  +/g, ' ');
                                  var slug =replaceall(" ", "-", replaceSpace).toLowerCase();

                                  var name = arrCategory[0].replace(/([a-z])([A-Z])/g, '$1 $2');
                                  name = name.charAt(0).toUpperCase() + name.slice(1);

                                  var tempCategory = {
                                    name: name,
                                    slug: slug,
                                    search_index:arrCategory[0],
                                    description:'',
                                    image: arrProductInsert.small_image,
                                    status:1,
                                    main_category:0,
                                    source:'Flipkart',
                                    created_on:arrProductInsert.created_on,
                                    updated_on:arrProductInsert.updated_on
                                  };
                                  var catObj = new Category(tempCategory);
                                  catObj.save((errCatInsert, objCat) => {
                                    if(errCatInsert)
                                    {
                                      console.log("Error occure during Category insert =>",errCatInsert);
                                    }else{

                                        // Parent category already exists
                                        arrCategory.shift(); // skip parent category
                                        if(arrCategory.length >0)
                                        {
                                          async(arrCategory, function (itemsCategory, callbackCategory){

                                            SubCategory.findOne({fk_category_id: objCat._id, feed_name:itemsCategory}, function(errSubCat, resultSubCat){
                                              if(errSubCat)
                                              {
                                                console.log("Error occure during SubCategory =>",JSON.stringify(errSubCat));
                                              }else{
                                                if(resultSubCat){
                                                  // Check in ObjectTaxonomy
                                                  var tempObj = {
                                                    fk_product_id:arrProductInsert._id,
                                                    fk_sub_category_id:resultSubCat._id,
                                                    fk_attribute_value_id:resultSubCat._id,
                                                    taxonomy:"Category",
                                                    status:1,
                                                    source:'Flipkart',
                                                    created_on:arrProductInsert.created_on,
                                                    updated_on:arrProductInsert.updated_on
                                                  };
                                                  var objTaxo = new ObjectTaxonomy(tempObj);
                                                  objTaxo.save((errObjInsert, resultObjInsert) => {
                                                    if(errObjInsert)
                                                    {
                                                      console.log("Error occure during ObjectTaxonomy insert =>",JSON.stringify(errObjInsert));
                                                    }else{
                                                        // Saved subcat into tempObjTaxono
                                                    }
                                                  });
                                                }else{
                                                  // insert SubCategory
                                                  var replaceSpecialChar = itemsCategory.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
                                                  var replaceSpace =replaceSpecialChar.replace(/  +/g, ' ');
                                                  var slug =replaceall(" ", "-", replaceSpace).toLowerCase();

                                                  var tempSubCategory = {
                                                    fk_category_id:objCat._id,
                                                    name: itemsCategory,
                                                    slug: slug,
                                                    feed_name:itemsCategory,
                                                    description:'',
                                                    image: arrProductInsert.small_image,
                                                    status:1,
                                                    source:'Flipkart',
                                                    created_on:arrProductInsert.created_on,
                                                    updated_on:arrProductInsert.updated_on
                                                    };
                                                var subCatObj = new SubCategory(tempSubCategory);
                                                subCatObj.save((errSubCatInsert, resultSubCatInsert) => {
                                                  if(errSubCatInsert)
                                                  {
                                                    console.log("Error occure during SubCategory insert =>",JSON.stringify(errSubCatInsert));
                                                  }else{
                                                    var tempObj = {
                                                      fk_product_id:arrProductInsert._id,
                                                      fk_sub_category_id:resultSubCatInsert._id,
                                                      fk_attribute_value_id:resultSubCatInsert._id,
                                                      taxonomy:"Category",
                                                      status:1,
                                                      source:'Flipkart',
                                                      created_on:arrProductInsert.created_on,
                                                      updated_on:arrProductInsert.updated_on
                                                    };
                                                    var objTaxo = new ObjectTaxonomy(tempObj);
                                                    objTaxo.save((errObjInsert, resultObjInsert) => {
                                                      if(errObjInsert)
                                                      {
                                                        console.log("Error occure during ObjectTaxonomy insert =>",JSON.stringify(errObjInsert));
                                                      }else{
                                                        // Saved subcat into tempObjTaxono
                                                      }
                                                    });

                                                   }
                                                 });
                                                }
                                                callbackCategory();
                                              }
                                            });
                                          },function(endOfSubCat){
                                            if(endOfSubCat)
                                            {
                                              console.log("Error occure during endOfSubCat =>",JSON.stringify(endOfSubCat));
                                            }else{
                                              if(arrAttributes.length >0)
                                              {
                                                async(arrAttributes, function (attrItems, callbackAttributes){

                                                  Attribute.findOne({name:attrItems.key},function(errBrands, resultBrands){
                                                    if(errBrands)
                                                    {
                                                      console.log("Error occure during Attributes featching =>",JSON.stringify(errBrands));
                                                    }else{
                                                      if(resultBrands)
                                                      {
                                                        AttributeValue.findOne({fk_attribute_id:resultBrands._id, name:{'$regex': '^'+attrItems.value, '$options' : 'i'}}, function(errExistAttrVal, resultExistAttrVal){
                                                          if(errExistAttrVal)
                                                          {
                                                            console.log("Error occure during Brands Value checking =>",JSON.stringify(errExistAttrVal));
                                                          }else{
                                                            if(resultExistAttrVal)
                                                            {
                                                              // insert ObjTaxonomy
                                                              var tempObjTaxono = {
                                                                fk_product_id:arrProductInsert._id,
                                                                fk_sub_category_id:resultExistAttrVal._id,
                                                                fk_attribute_value_id:resultExistAttrVal._id,
                                                                taxonomy:"Attribute",
                                                                status:1,
                                                                source:'Flipkart',
                                                                created_on:arrProductInsert.created_on,
                                                                updated_on:arrProductInsert.updated_on
                                                              };
                                                              var objTaxo = new ObjectTaxonomy(tempObjTaxono);
                                                              objTaxo.save((errObjTaxoInsert, resultObjTaxoInsert) => {
                                                                if(errObjTaxoInsert)
                                                                {
                                                                  console.log("Error occure during ObjectTaxonomy insert =>",JSON.stringify(errObjTaxoInsert));
                                                                }else{
                                                                //  console.log("ObjTaxonomy has been insert successfully.");
                                                                 callbackAttributes();
                                                                }
                                                              });
                                                            }else{
                                                              // Insert AttributeValue
                                                              var replaceSpecialChar = attrItems.value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
                                                              var replaceSpace =replaceSpecialChar.replace(/  +/g, ' ');
                                                              var slug =replaceall(" ", "-", replaceSpace).toLowerCase();

                                                              var tempAttrValue = {
                                                                fk_attribute_id: resultBrands._id,
                                                                display_name: attrItems.value,
                                                                name: attrItems.value,
                                                                slug: slug,
                                                                parent_id:null,
                                                                image: arrProductInsert.small_image,
                                                                status:1,
                                                                created_on:arrProductInsert.created_on,
                                                                updated_on:arrProductInsert.updated_on
                                                              };
                                                              var attrValue = new AttributeValue(tempAttrValue);
                                                              attrValue.save((errAttrValueInsert, resultAttrValueInsert) => {
                                                                if(errAttrValueInsert)
                                                                {
                                                                  console.log("Error occure during Attr Value insert =>",JSON.stringify(errAttrValueInsert));
                                                                }else{
                                                                  // insert ObjTaxonomy
                                                                  var tempObjTaxono = {
                                                                    fk_product_id:arrProductInsert._id,
                                                                    fk_sub_category_id:resultAttrValueInsert._id,
                                                                    fk_attribute_value_id:resultAttrValueInsert._id,
                                                                    taxonomy:"Attribute",
                                                                    status:1,
                                                                    source:'Flipkart',
                                                                    created_on:arrProductInsert.created_on,
                                                                    updated_on:arrProductInsert.updated_on
                                                                  };
                                                                  var objTaxo = new ObjectTaxonomy(tempObjTaxono);
                                                                  objTaxo.save((errObjTaxoInsert, resultObjTaxoInsert) => {
                                                                    if(errObjTaxoInsert)
                                                                    {
                                                                      console.log("Error occure during ObjectTaxonomy insert =>",JSON.stringify(errObjTaxoInsert));
                                                                    }else{
                                                                    //  console.log("ObjTaxonomy has been insert successfully.");
                                                                     callbackAttributes();
                                                                    }
                                                                  });
                                                                }
                                                              });
                                                            }
                                                          }
                                                        });
                                                      }else{
                                                        // Insert attributes
                                                        var display_name = attrItems.key;
                                                        display_name = display_name.replace(/([a-z])([A-Z])/g, '$1 $2');
                                                        display_name = display_name.charAt(0).toUpperCase() + display_name.slice(1);
                                                        var tempAttribute = {
                                                          name:attrItems.key,
                                                          display_name:display_name,
                                                          image: arrProductInsert.small_image,
                                                          status:1,
                                                          created_on:arrProductInsert.created_on,
                                                          updated_on:arrProductInsert.updated_on
                                                        };
                                                        var objAttr = new Attribute(tempAttribute);
                                                        objAttr.save((errBrandsInsert, resultBrands) => {
                                                          if(errBrandsInsert)
                                                          {
                                                            console.log("Error occure during Brand insert =>",JSON.stringify(errBrandsInsert));
                                                          }else{
                                                              AttributeValue.findOne({fk_attribute_id:resultBrands._id, name:{'$regex': '^'+attrItems.value, '$options' : 'i'}}, function(errExistAttrVal, resultExistAttrVal){
                                                                if(errExistAttrVal)
                                                                {
                                                                  console.log("Error occure during Brands Value checking =>",JSON.stringify(errExistAttrVal));
                                                                }else{
                                                                  if(resultExistAttrVal)
                                                                  {
                                                                    // insert ObjTaxonomy
                                                                    var tempObjTaxono = {
                                                                      fk_product_id:arrProductInsert._id,
                                                                      fk_sub_category_id:resultExistAttrVal._id,
                                                                      fk_attribute_value_id:resultExistAttrVal._id,
                                                                      taxonomy:"Attribute",
                                                                      status:1,
                                                                      source:'Flipkart',
                                                                      created_on:arrProductInsert.created_on,
                                                                      updated_on:arrProductInsert.updated_on
                                                                    };
                                                                    var objTaxo = new ObjectTaxonomy(tempObjTaxono);
                                                                    objTaxo.save((errObjTaxoInsert, resultObjTaxoInsert) => {
                                                                      if(errObjTaxoInsert)
                                                                      {
                                                                        console.log("Error occure during ObjectTaxonomy insert =>",JSON.stringify(errObjTaxoInsert));
                                                                      }else{
                                                                      //  console.log("ObjTaxonomy has been insert successfully.");
                                                                       callbackAttributes();
                                                                      }
                                                                    });
                                                                  }else{
                                                                    // Insert AttributeValue
                                                                    var replaceSpecialChar = attrItems.value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
                                                                    var replaceSpace =replaceSpecialChar.replace(/  +/g, ' ');
                                                                    var slug =replaceall(" ", "-", replaceSpace).toLowerCase();

                                                                    var tempAttrValue = {
                                                                      fk_attribute_id: resultBrands._id,
                                                                      display_name: attrItems.value,
                                                                      name: attrItems.value,
                                                                      slug: slug,
                                                                      parent_id:null,
                                                                      image: arrProductInsert.small_image,
                                                                      status:1,
                                                                      created_on:arrProductInsert.created_on,
                                                                      updated_on:arrProductInsert.updated_on
                                                                    };
                                                                    var attrValue = new AttributeValue(tempAttrValue);
                                                                    attrValue.save((errAttrValueInsert, resultAttrValueInsert) => {
                                                                      if(errAttrValueInsert)
                                                                      {
                                                                        console.log("Error occure during Attr Value insert =>",JSON.stringify(errAttrValueInsert));
                                                                      }else{
                                                                        // insert ObjTaxonomy
                                                                        var tempObjTaxono = {
                                                                          fk_product_id:arrProductInsert._id,
                                                                          fk_sub_category_id:resultAttrValueInsert._id,
                                                                          fk_attribute_value_id:resultAttrValueInsert._id,
                                                                          taxonomy:"Attribute",
                                                                          status:1,
                                                                          source:'Flipkart',
                                                                          created_on:arrProductInsert.created_on,
                                                                          updated_on:arrProductInsert.updated_on
                                                                        };
                                                                        var objTaxo = new ObjectTaxonomy(tempObjTaxono);
                                                                        objTaxo.save((errObjTaxoInsert, resultObjTaxoInsert) => {
                                                                          if(errObjTaxoInsert)
                                                                          {
                                                                            console.log("Error occure during ObjectTaxonomy insert =>",JSON.stringify(errObjTaxoInsert));
                                                                          }else{
                                                                          //  console.log("ObjTaxonomy has been insert successfully.");
                                                                           callbackAttributes();
                                                                          }
                                                                        });
                                                                      }
                                                                    });
                                                                  }
                                                                }
                                                              });
                                                            }
                                                        });
                                                      }
                                                    }
                                                  });
                                                },function(endOfAttr){
                                                   callback();
                                                });
                                              }
                                            }
                                            //callback();
                                          });
                                        }
                                      // @end else cond
                                    }
                                  });
                                }
                              }
                            });

                          }
                        }
                      });
                    });
                  } // @end insert
                }
              });
            }

          },function(endOfLoop){
            if(endOfLoop)
            {
              return res.status(200).json({
                message: JSON.stringify(endOfLoop)
              });
            }else{
              const nowCurrent = new Date();
              const current_datetime_end =dateFormat(nowCurrent, "yyyy-mm-dd HH:MM:ss");
              const objUpdate = {
                status: 'complete',
                updated_on:current_datetime_end
              };
              FlipkartProduct.update({_id:arrDataReady._id},{$set :objUpdate},(errUpdateEnd, resultSetUpdateEnd) => {
                return res.status(200).json({
                  message: "End of Loop"
                });
              });
            }
          });
        }
       });
     }else{
       return res.status(200).json({
         message: "No task available"
       });
     }
    }
   });
  }
 }
}); // @end of checking satatus

}); //@end of the action

module.exports = router;
