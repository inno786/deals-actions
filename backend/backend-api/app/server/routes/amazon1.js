const express = require('express');
var mongoose = require('mongoose');
const dateFormat = require('dateformat');
var replaceall = require("replaceall");
const isset = require('isset');
const validator = require('validator');

const Category = require('mongoose').model('Category');
const SubCategory = require('mongoose').model('SubCategory');
const Attribute = require('mongoose').model('Attribute');
const SubCatAttribute = require('mongoose').model('SubCatAttribute');
const AttributeMapping = require('mongoose').model('AttributeMapping');
const Product = require('mongoose').model('Product');
const ProductMapping = require('mongoose').model('ProductMapping');
const ProductVendor = require('mongoose').model('ProductVendor');

const router = new express.Router();
var amazon = require('amazon-product-api');
var affiliate = require('snapdeal-affiliate-client');

var client = amazon.createClient({
  awsId: "AKIAJNRCYNFD3CPTKKDQ",
  awsSecret: "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",
  awsTag: "coupon25-20"
});

// var snapdealClient = affiliate.createClient({
//   SdAffId: '31712',
//   SdAffToken: '0beb1c80df235b3e5c3c77bfcc4739',
//   responseType: 'json'
// });

/**
 * Validate the sign up form
 *
 * @param {object} payload - the HTTP body message
 * @returns {object} The result of validation. Object contains a boolean validation result,
 *                   errors tips, and a global message for the whole form.
 */
function validateForm(payload) {
  const errors = {};
  let isFormValid = true;
  let message = '';

  if (payload.SearchIndex =='') {
    isFormValid = false;
    errors.SearchIndex = 'Category cannot be blank.';
  }
  if (!isFormValid) {
    message = 'Please fixed error.';
  }

  return {
    success: isFormValid,
    message,
    errors
  };
}

function getWords(str) {
    var splitStr = str.split(' ').slice(0, 20).join(' ');
    return splitStr;
}
function amoutFormat(amout) {
   var formated =0;
    if(amout !="" && amout >0)
     {
       formated = amout/100;
     }
     return formated;
}

function subcatIdentifier(){
    arrData.forEach(function(ancestorItem){
      if(isset(ancestorItem.BrowseNode)){
         var objType = typeof ancestorItem.BrowseNode;
         if(objType =='object')
         {
             var arrAncestors = ancestorItem.BrowseNode[0].Ancestors;
             var objTypeAncestors = typeof ancestorItem.BrowseNode[0].Ancestors;
             if(objTypeAncestors =='object' && ancestorItem.BrowseNode[0].Ancestors[0].BrowseNode[0].Name[0] !='Categories'){
               return subcatIdentifier(ancestorItem.BrowseNode[0].Ancestors);
             }else{
               console.log("Return  =="+JSON.stringify(ancestorItem.BrowseNode[0].Name[0]));
               return ancestorItem.BrowseNode[0].Name[0];
             }
         }
      }
  });
}

router.post('/search', (req, res) => {
  var reqBody = req.body;
  const validationResult = validateForm(reqBody);
  if (!validationResult.success) {
    return res.status(400).json({
      success: false,
      message: validationResult.message,
      errors: validationResult.errors
    });
  }else{
    client.itemSearch(reqBody, function(err, results, response) {
          if (err) {
            console.log("Error =="+err);
            return res.status(400).json({
              success: false,
              message: "Error: "+err[0].Error[0].Message[0],
              errors:{},
               arrData:{}
              });
          } else {
            var now = new Date();
            var current_datetime =dateFormat(now, "yyyy-mm-dd HH:MM:ss");

            var arrAttrSkip =["Binding","EAN","Feature","HardwarePlatform", "Label", "LegalDisclaimer","Manufacturer", "NumberOfItems", "PackageQuantity", "PartNumber","ProductGroup", "ProductTypeName", "Title","UPC", "MPN"];
            results.forEach(function(items){
              var arrItemList = items.ItemAttributes[0];
              var arrAtt =[];
              for (var key in arrItemList) {
                var valueType =typeof arrItemList[key];
              //  console.log("Key ="+key+", type ="+arrItemList[key]+", valueType ="+valueType+", length ="+arrItemList[key].length);
                if(!arrAttrSkip.includes(key) && arrItemList[key] !='[object Object]')
                {
                  var temp = {key: key, value:arrItemList[key][0]};
                  arrAtt.push(temp);
                }
              }
              //console.log("Attributes  =="+JSON.stringify(arrAtt,  null, '\t'));
            if(isset(items.ASIN[0])){
                 Product.findOne({vendor:'amazon', product_id:items.ASIN[0]}, function(err, arrProdData){
                   if(err)
                   {
                     console.log("Product Error: "+err);
                   }else{
                     if(isset(arrProdData))
                     {

                     }else{
                       //console.log("Title =="+items.ItemAttributes[0].Title);
                       var title =isset(items.ItemAttributes[0].Title[0])? items.ItemAttributes[0].Title[0]:'';
                       var replaceSpecialChar = title.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
                       var replaceSpace =replaceSpecialChar.replace(/  +/g, ' ');
                       var splitString = getWords(replaceSpace);
                       var slug =replaceall(" ", "-", splitString).toLowerCase();
                       var price =0;
                       if(isset(items.ItemAttributes[0].ListPrice))
                       {
                           //console.log("ListPrice "+items.ItemAttributes[0].ListPrice[0].Amount[0]);
                           price = amoutFormat(items.ItemAttributes[0].ListPrice[0].Amount[0]);
                       }else {
                         if(isset(items.OfferSummary)){
                           //console.log("OfferSummary ="+ items.OfferSummary[0].LowestUsedPrice[0].Amount[0]);
                           price = amoutFormat(items.OfferSummary[0].LowestUsedPrice[0].Amount[0]);
                         }
                       }


                       // Product part start here
                       var arrProdTemp ={
                         vendor: 'amazon',
                         product_id: items.ASIN[0],
                         name: title,
                         slug:slug,
                         description: isset(items.EditorialReviews[0].EditorialReview[0].Content[0])? items.EditorialReviews[0].EditorialReview[0].Content[0]:'',
                         feature: isset(items.ItemAttributes[0].Feature[0])? items.ItemAttributes[0].Feature[0]:'',
                         price: price,
                         smallImage: items.SmallImage[0].URL[0],
                         mediumImage: items.MediumImage[0].URL[0],
                         largeImage: items.LargeImage[0].URL[0],
                         status:1,
                         created_on: current_datetime,
                         updated_on: current_datetime
                       }
                       //console.log(arrProdTemp);
                       var productObj = new Product(arrProdTemp);
                       productObj.save((productResultErr, productResultSet) => {
                           if(productResultErr)
                             {
                               console.log("Error during product insertion =="+productResultErr)
                             }else{
                               // Product Vendor part start here
                               var jsonProductVendor ={
                                  fk_product_id :productResultSet._id,
                                  vendor: productResultSet.vendor,
                                  product_id: productResultSet.product_id,
                                  name: productResultSet.name,
                                  product_detail_url: items.DetailPageURL[0],
                                  price: price,
                                  stock: 'In Stock',
                                  status: 1,
                                  created_on: current_datetime,
                                  updated_on: current_datetime
                               }
                               var productVendorObj = new ProductVendor(jsonProductVendor);
                               productVendorObj.save((productVendorErr, productVendorResultSet) =>{
                                 if(productVendorErr)
                                   {
                                     console.log("Error during product Vendor insertion =="+productVendorErr)
                                   }else{
                                     // Category part start here
                                     Category.findOne({search_index:reqBody.SearchIndex}, function(catErr, catResultSet){
                                       if(catErr)
                                       {
                                         console.log("Error occure during featiching category ="+catErr);
                                       }else{
                                         if(catResultSet)
                                         {
                                           var arrAncestors = items.BrowseNodes[0].BrowseNode[0].Ancestors;
                                           var findSubCategory = (arrAncestors) =>{
                                               arrAncestors.forEach(function(ancestorItem){
                                               if(isset(ancestorItem.BrowseNode)){
                                                  var objType = typeof ancestorItem.BrowseNode;
                                                  if(objType =='object')
                                                  {
                                                      var arrAncestors = ancestorItem.BrowseNode[0].Ancestors;
                                                      var objTypeAncestors = typeof ancestorItem.BrowseNode[0].Ancestors;
                                                      if(objTypeAncestors =='object' && ancestorItem.BrowseNode[0].Ancestors[0].BrowseNode[0].IsCategoryRoot[0] !='1'){
                                                        return findSubCategory(ancestorItem.BrowseNode[0].Ancestors);
                                                      }else{
                                                        console.log("Return  =="+JSON.stringify(ancestorItem.BrowseNode[0].Name[0]));
                                                        subcategory = ancestorItem.BrowseNode[0].Name[0];
                                                        if(isset(subcategory))
                                                        {
                                                          SubCategory.findOne({fk_category_id:catResultSet._id,name: subcategory}, function(subCaterr, subCatResultSet){
                                                            if(subCaterr)
                                                            {
                                                              console.log("Error Occurs during subcategory ="+subCaterr);
                                                            }else{
                                                              if(subCatResultSet)
                                                              {
                                                                arrAtt.forEach(function(attrItems){
                                                                  Attribute.findOne({name:attrItems.key}, function(attrErr, attributeResultSet){
                                                                    if(attrErr)
                                                                    {
                                                                      console.log("Error during Attribute fetching =="+attrErr);
                                                                    }else{
                                                                      if(attributeResultSet)
                                                                      {

                                                                      }else{
                                                                        var attrTemp ={
                                                                          display_name: attrItems.key.replace(/([a-z])([A-Z])/g, '$1 $2'),
                                                                          name:attrItems.key,
                                                                          image:productResultSet.mediumImage,
                                                                          description:'',
                                                                          status:1,
                                                                          created_on: current_datetime,
                                                                          updated_on: current_datetime
                                                                        }
                                                                        var attributeObj = new Attribute(attrTemp);
                                                                        attributeObj.save((attrErr, attrResultSet) =>{
                                                                          if(attrErr)
                                                                            {
                                                                              console.log("Error during attribute insertion =="+attrErr);
                                                                            }else{
                                                                              SubCatAttribute.findOne({fk_subcat_id: subCatResultSet._id,fk_attribute_id: attrResultSet._id}, function(checkSubCatAttrErr, checkSubCatAttResultSet){
                                                                                //if(checkSubCatAttResultSet)
                                                                              });

                                                                              // subcatattr start
                                                                              var subCatAttributeTemp ={
                                                                                fk_subcat_id: subCategoryResultSet._id,
                                                                                fk_attribute_id: attrResultSet._id,
                                                                                status:1,
                                                                                created_on: current_datetime,
                                                                                updated_on: current_datetime
                                                                              }
                                                                             var subCatAttributeeObj = new SubCatAttribute(subCatAttributeTemp);
                                                                             subCatAttributeeObj.save((subCatAttrErr, subCatAttResultSet) =>{
                                                                               if(subCatAttrErr)
                                                                                 {
                                                                                   console.log("Error during subcatattr insertion =="+subCatAttrErr);
                                                                                 }else{

                                                                                 }
                                                                               });
                                                                               // subcatattr start
                                                                               var replaceSpecials = attrResultSet.name.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
                                                                               var replaceSpaces =replaceSpecials.replace(/  +/g, ' ');
                                                                               var subCatAttrSlug =replaceall(" ", "-", replaceSpaces).toLowerCase();
                                                                               var attributeMappingTemp ={
                                                                                 fk_subcat_id: subCategoryResultSet._id,
                                                                                 fk_attribute_id: attrResultSet._id,
                                                                                 name: attrResultSet.name,
                                                                                 slug: subCatAttrSlug,
                                                                                 parent_id:'',
                                                                                 status:1,
                                                                                 created_on: current_datetime,
                                                                                 updated_on: current_datetime
                                                                               }
                                                                               var attributeMappingObj = new AttributeMapping(attributeMappingTemp);
                                                                               attributeMappingObj.save((attributeMappingErr, attributeMappingResultSet) =>{
                                                                                if(attributeMappingErr)
                                                                                  {
                                                                                    console.log("Error during attributeMapping insertion =="+attributeMappingErr);
                                                                                  }else{
                                                                                    // subcatattr start
                                                                                    var productMappingTemp ={
                                                                                      fk_product_id: productResultSet._id,
                                                                                      fk_attribute_mapping_id: attributeMappingResultSet._id,
                                                                                      name: attributeMappingResultSet.name,
                                                                                      slug: attributeMappingResultSet.slug,
                                                                                      status:1,
                                                                                      created_on: current_datetime,
                                                                                      updated_on: current_datetime
                                                                                    }
                                                                                    var productMappingObj = new ProductMapping(productMappingTemp);
                                                                                    productMappingObj.save((productMappingErr, productMappingResultSet) =>{
                                                                                     if(productMappingErr)
                                                                                       {
                                                                                         console.log("Error during productMappingErr insertion =="+productMappingErr);
                                                                                       }else{

                                                                                       }
                                                                                     });

                                                                                  }
                                                                                });
                                                                            } //end 1
                                                                          });
                                                                      }
                                                                    }
                                                                  })
                                                                })

                                                              }else{
                                                                var replaceSpecialCharSlug = subcategory.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
                                                                var replaceSpaceSlug =replaceSpecialCharSlug.replace(/  +/g, ' ');
                                                                var splitStringSlug =replaceSpaceSlug.split(' ').slice(0, 20).join(' ');
                                                                var subCatSlug =replaceall(" ", "-", splitStringSlug).toLowerCase();
                                                                var subcatTemp ={
                                                                  fk_category_id:catResultSet._id,
                                                                  name:subcategory,
                                                                  slug:subCatSlug,
                                                                  description:'',
                                                                  image: productResultSet.mediumImage,
                                                                  status:1,
                                                                  created_on: current_datetime,
                                                                  updated_on: current_datetime
                                                                }
                                                                var subCategoryObj = new SubCategory(subcatTemp);
                                                                subCategoryObj.save((subCategoryErr, subCategoryResultSet) =>{
                                                                  if(subCategoryErr)
                                                                    {
                                                                      console.log("Error during subcategory insertion =="+subCategoryErr);
                                                                    }else{
                                                                      arrAtt.forEach(function(attrItems){
                                                                        Attribute.findOne({name:attrItems.key}, function(attrErr, attributeResultSet){
                                                                          if(attrErr)
                                                                          {
                                                                            console.log("Error during Attribute fetching =="+attrErr);
                                                                          }else{
                                                                            if(attributeResultSet)
                                                                            {

                                                                            }else{
                                                                              var attrTemp ={
                                                                                display_name: attrItems.key.replace(/([a-z])([A-Z])/g, '$1 $2'),
                                                                                name:attrItems.key,
                                                                                image:productResultSet.mediumImage,
                                                                                description:'',
                                                                                status:1,
                                                                                created_on: current_datetime,
                                                                                updated_on: current_datetime
                                                                              }
                                                                              var attributeObj = new Attribute(attrTemp);
                                                                              attributeObj.save((attrErr, attrResultSet) =>{
                                                                                if(attrErr)
                                                                                  {
                                                                                    console.log("Error during attribute insertion =="+attrErr);
                                                                                  }else{
                                                                                    // subcatattr start
                                                                                    var subCatAttributeTemp ={
                                                                                      fk_subcat_id: subCategoryResultSet._id,
                                                                                      fk_attribute_id: attrResultSet._id,
                                                                                      status:1,
                                                                                      created_on: current_datetime,
                                                                                      updated_on: current_datetime
                                                                                    }
                                                                                   var subCatAttributeeObj = new SubCatAttribute(subCatAttributeTemp);
                                                                                   subCatAttributeeObj.save((subCatAttrErr, subCatAttResultSet) =>{
                                                                                     if(subCatAttrErr)
                                                                                       {
                                                                                         console.log("Error during subcatattr insertion =="+subCatAttrErr);
                                                                                       }else{

                                                                                       }
                                                                                     });
                                                                                     // subcatattr start
                                                                                     var replaceSpecials = attrResultSet.name.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
                                                                                     var replaceSpaces =replaceSpecials.replace(/  +/g, ' ');
                                                                                     var subCatAttrSlug =replaceall(" ", "-", replaceSpaces).toLowerCase();
                                                                                     var attributeMappingTemp ={
                                                                                       fk_subcat_id: subCategoryResultSet._id,
                                                                                       fk_attribute_id: attrResultSet._id,
                                                                                       name: attrResultSet.name,
                                                                                       slug: subCatAttrSlug,
                                                                                       parent_id:'',
                                                                                       status:1,
                                                                                       created_on: current_datetime,
                                                                                       updated_on: current_datetime
                                                                                     }
                                                                                     var attributeMappingObj = new AttributeMapping(attributeMappingTemp);
                                                                                     attributeMappingObj.save((attributeMappingErr, attributeMappingResultSet) =>{
                                                                                      if(attributeMappingErr)
                                                                                        {
                                                                                          console.log("Error during attributeMapping insertion =="+attributeMappingErr);
                                                                                        }else{
                                                                                          // subcatattr start
                                                                                          var productMappingTemp ={
                                                                                            fk_product_id: productResultSet._id,
                                                                                            fk_attribute_mapping_id: attributeMappingResultSet._id,
                                                                                            name: attributeMappingResultSet.name,
                                                                                            slug: attributeMappingResultSet.slug,
                                                                                            status:1,
                                                                                            created_on: current_datetime,
                                                                                            updated_on: current_datetime
                                                                                          }
                                                                                          var productMappingObj = new ProductMapping(productMappingTemp);
                                                                                          productMappingObj.save((productMappingErr, productMappingResultSet) =>{
                                                                                           if(productMappingErr)
                                                                                             {
                                                                                               console.log("Error during productMappingErr insertion =="+productMappingErr);
                                                                                             }else{

                                                                                             }
                                                                                           });

                                                                                        }
                                                                                      });
                                                                                  }
                                                                                });
                                                                            }
                                                                          }
                                                                        })
                                                                      }) //end attr
                                                                    }
                                                                 });
                                                             }
                                                           }
                                                         });
                                                       }
                                                        return '';
                                                      }
                                                  }
                                               }
                                           });
                                           }

                                         }
                                       }
                                     });
                                   }
                               });
                             }
                       });
                     }
                   }
                 });
              }
            });
            return res.status(200).json({
               success: true,
               message: '',
               errors:{},
               arrData:results
             });
          }
        });
  }
});

// Category list
router.post('/categoryList', (req, res) => {

    Category.find({status:1}, function(err, resultSet){
      if(err)
      {
        return res.status(409).json({
          message: err,
          arrData:{}
        });
      }else{
        var arrResultSet =[];
        if(resultSet)
        {
          resultSet.forEach(function(items){
             var arrTemp ={
                id: items.search_index,
                name: items.name
             };
             arrResultSet.push(arrTemp);
          });
        }
        return res.status(200).json({
          message: '',
          arrData:arrResultSet
        });
      }
    })
});
module.exports = router;
