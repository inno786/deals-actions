let express = require('express'),
  mongoose = require('mongoose'),
  dateFormat = require('dateformat'),
  replaceall = require("replaceall"),
  SubCategory = require('mongoose').model('SubCategory'),
  Seo = require('mongoose').model('Seo'),
  fs = require('fs'),
  asyncLoop = require('node-async-loop'),
  router = new express.Router();

/**
 * Validate the sign up form
 *
 * @param {object} payload - the HTTP body message
 * @returns {object} The result of validation. Object contains a boolean validation result,
 *                   errors tips, and a global message for the whole form.
 */
function validateForm(payload) {
  let error = {
    name: '',
    fk_category_id: '',
    image: ''
  },
    isFormValid = true,
    message = '';

  if (payload.name == '') {
    isFormValid = false;
    error.name = 'Name cannot be blank.';
  }
  if (payload.fk_category_id == '') {
    isFormValid = false;
    error.fk_category_id = 'Select category';
  }

  if (!isFormValid) {
    message = 'Please fixed error.';
  }

  return {
    success: isFormValid,
    message,
    error
  };
}

function jsUcfirst(string) {
  return string.replace(/\b[a-z]/g, function (f) { return f.toUpperCase(); });
}

function checkFileExistsSync(filepath) {
  let flag = true;
  try {
    fs.accessSync(filepath, fs.F_OK);
  } catch (e) {
    flag = false;
  }
  return flag;
}

function findSubCategoryByAttribute(condition) {
  return new Promise(resolve => {
    return SubCategory.findOne(condition, (err, result) => {
      if (err) {
        return resolve({ error: JSON.stringify(err) })
      } else {
        if (result) {
          return resolve({ error: null, result: true })
        } else {
          return resolve({ error: null, result: false })
        }
      }
    })
  })
}

function dataTraverse(arrData) {
  return new Promise(resolve => {
    let arrResultSet = []
    return asyncLoop(arrData, (items, callback) => {
      if (items) {
        let image = '/images/category-placeholder.png',
          fileExists = checkFileExistsSync(rootPath + '/public/images/' + items.image);
        if (fileExists && items.image) {
          image = '/images/' + items.image;
        }
        let created_on = dateFormat(items.created_on, "yyyy-mm-dd"),
          updated_on = dateFormat(items.updated_on, "yyyy-mm-dd"),
          arrTemp = {
            _id: items._id,
            name: items.name,
            slug: items.slug,
            description: items.description,
            image: image,
            source: items.source,
            status: items.status == '1' ? "Active" : "Inactive",
            created_on: created_on,
            updated_on: updated_on
          }
        arrResultSet.push(arrTemp)
        callback()
      } else {
        callback()
      }
    }, err => {
      return resolve({ error: err, result: arrResultSet })
    })
  })
}

router.post('/create', async (req, res) => {

  let error = {
    name: '',
    fk_category_id: '',
    image: ''
  },
    reqBody = req.body;
  console.log("reqBody =>", reqBody)
  let validationResult = await validateForm(reqBody);
  if (!validationResult.success) {
    return res.status(200).json({
      success: false,
      message: validationResult.message,
      error: validationResult.error
    });
  } else {
    const now = new Date();
    const current_datetime = dateFormat(now, "yyyy-mm-dd HH:MM:ss");

    const arrData = {
      fk_category_id: reqBody.fk_category_id.trim(),
      name: jsUcfirst(reqBody.name.trim()),
      slug: replaceall(" ", "-", reqBody.name.trim()).toLowerCase(),
      feed_name: reqBody.feed_name.trim(),
      description: reqBody.description.trim(),
      image: reqBody.image.trim(),
      source: reqBody.source.trim(),
      status: reqBody.status,
      created_on: current_datetime,
      updated_on: current_datetime
    };
    //console.log(arrData);
    let duplicate = await findSubCategoryByAttribute({ slug: arrData.slug })
    if (duplicate.error) {
      return res.status(200).json({
        success: false,
        message: duplicate.error,
        error: error
      });
    } else {
      if (duplicate.result) {
        error.name = 'Subcategory name already exists.'
        return res.status(200).json({
          success: false,
          message: 'Check the form for errors.',
          error: error
        });
      } else {
        const model = new SubCategory(arrData);
        model.save((err, resultSet) => {
          if (err) {
            return res.status(200).json({
              success: false,
              message: 'Could not process the form.',
              error: error
            });
          } else {
            const arrData = {
              fk_id: resultSet._id,
              type: 'static',
              keyword: reqBody.keyword.trim(),
              top_description: reqBody.top_description.trim(),
              bottom_description: reqBody.bottom_description.trim(),
              created_on: current_datetime,
              updated_on: current_datetime
            };
            const SeoObj = new Seo(arrData);
            SeoObj.save((errSeo) => {
              if (errSeo) {
                return res.status(200).json({
                  success: false,
                  message: JSON.stringify(errSeo),
                  error: error
                });
              } else {
                return res.status(200).json({
                  success: true,
                  message: 'Sub category has been created successfully.',
                  error: error
                })
              }
            })
          }
        })
      }
    }
  }
});

// Update action start from here
router.post('/update', async (req, res) => {

  let error = {
    name: '',
    fk_category_id: '',
    image: ''
  },
    reqBody = req.body;
  console.log("reqBody =>", reqBody)
  let validationResult = await validateForm(reqBody);
  if (!validationResult.success) {
    return res.status(200).json({
      success: false,
      message: validationResult.message,
      error: validationResult.error
    });
  } else {

    const now = new Date();
    const current_datetime = dateFormat(now, "yyyy-mm-dd HH:MM:ss");

    const arrData = {
      fk_category_id: reqBody.fk_category_id,
      name: jsUcfirst(reqBody.name.trim()),
      slug: replaceall(" ", "-", reqBody.name.trim()).toLowerCase(),
      feed_name: reqBody.feed_name,
      description: reqBody.description,
      image: reqBody.image,
      source: reqBody.source,
      status: reqBody.status,
      updated_on: current_datetime
    };
    let duplicate = await findSubCategoryByAttribute({
      slug: arrData.slug,
      _id: { $ne: mongoose.Types.ObjectId(reqBody._id) }
    })
    if (duplicate.error) {
      return res.status(200).json({
        success: false,
        message: duplicate.error,
        error: error
      });
    } else {
      if (duplicate.result) {
        error.name = 'Category name already exists.'
        return res.status(200).json({
          success: false,
          message: 'Check the form for errors.',
          error: error
        });
      } else {

        SubCategory.update({ _id: reqBody._id }, { $set: arrData }, (err, resultSet) => {
          if (err) {
            return res.status(200).json({
              success: false,
              message: 'Could not process the form.',
              error: error
            });
          } else {
            Seo.findOne({ fk_id: reqBody._id }, function (seoErr, seoRes) {
              if (seoErr) {
                return res.status(200).json({
                  success: false,
                  message: JSON.stringify(seoErr),
                  error: error
                });
              } else {
                if (seoRes) {
                  const arrData = {
                    keyword: reqBody.keyword.trim(),
                    top_description: reqBody.top_description.trim(),
                    bottom_description: reqBody.bottom_description.trim(),
                    updated_on: current_datetime
                  };
                  Seo.update({ fk_id: reqBody._id }, { $set: arrData }, (errSeo, seoUpdateRes) => {
                    if (errSeo) {
                      return res.status(200).json({
                        success: false,
                        message: JSON.stringify(errSeo),
                        error: error
                      });
                    } else {
                      return res.status(200).json({
                        success: true,
                        message: 'Record has been updated successfully.',
                        error: error
                      });
                    }
                  });
                } else {

                  const arrData = {
                    fk_id: reqBody._id,
                    type: 'static',
                    keyword: reqBody.keyword.trim(),
                    top_description: reqBody.top_description.trim(),
                    bottom_description: reqBody.bottom_description.trim(),
                    created_on: current_datetime,
                    updated_on: current_datetime
                  };
                  const SeoObj = new Seo(arrData);
                  SeoObj.save((errSeo) => {
                    if (errSeo) {
                      return res.status(200).json({
                        success: false,
                        message: JSON.stringify(errSeo),
                        error: error
                      });
                    } else {
                      return res.status(200).json({
                        success: true,
                        message: 'Record has been updated successfully.',
                        error: error
                      });
                    }
                  });
                }
              }
            });
          }
        });
      }
    }
  }
});

// Category list
router.post('/list', (req, res) => {

  let reqData = req.body;
  let limit = parseInt(reqData.limit);
  let currentPage = parseInt(reqData.currentPage);
  if (currentPage > 0) {
    currentPage = currentPage - 1;
  }
  let skip = parseInt(limit * currentPage);

  let promises = SubCategory.find().populate({ path: 'fk_category_id', select: 'name' }).skip(skip).limit(limit);
  let totalItems = SubCategory.count().populate({ path: 'fk_category_id', select: 'name' });

  if (reqData.name && reqData.name != "") {
    promises.where({ name: { '$regex': '^' + reqData.name, '$options': 'i' } });
    totalItems.where({ name: { '$regex': '^' + reqData.name, '$options': 'i' } });
  }
  if (reqData.status && reqData.status != "") {
    promises.where('status').equals(reqData.status);
    totalItems.where('status').equals(reqData.status);
  }
  if (reqData.fk_category_id && reqData.fk_category_id != "") {
    promises.where('fk_category_id').equals(reqData.fk_category_id);
    totalItems.where('fk_category_id').equals(reqData.fk_category_id);
  }
  totalItems.exec(function (errCount, resultCount) {

    promises.exec(async (err, resultSet) => {
      if (err) {
        return res.status(200).json({
          message: err,
          arrData: []
        });
      } else {
        let getResult = await dataTraverse(resultSet)
        return res.status(200).json({
          message: '',
          arrData: getResult.result,
          total: Math.ceil(resultCount / limit)
        });
      }
    });
  });
});

// Find by pk
router.post('/findByPk', (req, res) => {

  let reqData = req.body;
  SubCategory.findOne({ _id: reqData.id }).populate('Category').exec(function (err, resultSet) {
    if (err) {
      return res.status(200).json({
        message: err,
        subcategory: [],
        seo: []
      });
    } else {
      Seo.findOne({ fk_id: reqData.id, type: 'static' }, function (errSeo, arrSeoData) {
        if (errSeo) {
          return res.status(200).json({
            message: errSeo,
            subcategory: [],
            seo: []
          });
        } else {
          //console.log(arrSeoData);
          return res.status(200).json({
            message: '',
            subcategory: resultSet,
            seo: arrSeoData
          });
        }
      });
    }
  });
});

module.exports = router;
