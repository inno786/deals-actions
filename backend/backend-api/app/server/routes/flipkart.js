let express = require('express'),
    mongoose = require('mongoose'),
    FlipkartProduct = require('mongoose').model('FlipkartProduct'),
    FkProductImport = require('../../helper/FkProductImport');

let FkProductImportObj = new FkProductImport;
let router = new express.Router();

// Category list
router.get('/import', async (req, res) => {

    FlipkartProduct.findOne({status: 'inprogress'}, function (err, arrData) {
        if (err) {
            console.log("Process already running Error => ", JSON.stringify(err));
            return res.status(200).json({
                message: JSON.stringify(err)
            });
        } else {
            if (arrData) {
                console.log("Process already running");
                return res.status(200).json({
                    message: "Process already running"
                });
            } else {
                FlipkartProduct.findOne({status: 'ready'}, async (error, response) => {
                    if (error) {
                        console.log("Resource featching Error => ", JSON.stringify(error));
                        return res.status(200).json({
                            message: JSON.stringify(error)
                        });
                    } else {
                        if (response) {

                            let cond = {
                                _id: response._id,
                                status: 'inprogress'
                            }
                            let arrData = await FkProductImportObj.updateResource(cond)

                            if (arrData.error) {
                                return res.status(200).json({
                                    message: JSON.stringify(arrData.error)
                                });
                            } else {
                                let rawData = JSON.parse(response.raw_data);
                                //let products = rawData.products[0]; 
                                //console.log("response =>", JSON.stringify(products, " ", 2));
                                let getResponse = await FkProductImportObj.productList(rawData.products)
                                let condition = {
                                    _id: response._id,
                                    status: 'complete'
                                }
                                response.status = 'complete'
                                let finalResp = await FkProductImportObj.updateResource(condition) 
                                return res.status(200).json({status:"Product has been imported successfully"});
                            }
                        } else {
                            return res.status(200).json({
                                message: "No task available"
                            });
                        }
                    }
                });
            }
        }
    }); // @end of checking satatus

})

module.exports = router;
