const express = require('express');
const mongoose = require('mongoose');
const dateFormat = require('dateformat');
const replaceall = require("replaceall");
const Attribute = require('mongoose').model('Attribute');
const Seo = require('mongoose').model('Seo');
const router = new express.Router();

/**
 * Validate the sign up form
 *
 * @param {object} payload - the HTTP body message
 * @returns {object} The result of validation. Object contains a boolean validation result,
 *                   errors tips, and a global message for the whole form.
 */
function validateForm(payload) {
  let error = {
    name: '',
    display_name: ''
  },
    isFormValid = true,
    message = '';

  if (payload.name == '') {
    isFormValid = false;
    error.name = 'Name cannot be blank.';
  }
  if (payload.display_name == '') {
    isFormValid = false;
    error.name = 'Display name cannot be blank.';
  }

  if (!isFormValid) {
    message = 'Please fixed error.';
  }

  return {
    success: isFormValid,
    message,
    error
  };
}

function jsUcfirst(string) {
  return string.replace(/\b[a-z]/g, function (f) { return f.toUpperCase(); });
}

function checkFileExistsSync(filepath) {
  let flag = true;
  try {
    fs.accessSync(filepath, fs.F_OK);
  } catch (e) {
    flag = false;
  }
  return flag;
}

function findAttributeByAttribute(condition) {
  return new Promise(resolve => {
    return Attribute.findOne(condition, (err, result) => {
      if (err) {
        return resolve({ error: JSON.stringify(err) })
      } else {
        if (result) {
          return resolve({ error: null, result: true })
        } else {
          return resolve({ error: null, result: false })
        }
      }
    })
  })
}

router.post('/create', async (req, res) => {

  let error = {
    name: '',
    display_name: ''
  },
    reqBody = req.body;

  let validationResult = await validateForm(reqBody);
  if (!validationResult.success) {
    return res.status(200).json({
      success: false,
      message: validationResult.message,
      error: validationResult.error
    });
  } else {

    const now = new Date();
    const current_datetime = dateFormat(now, "yyyy-mm-dd HH:MM:ss");

    const arrData = {
      name: reqBody.name.trim(),
      display_name: jsUcfirst(reqBody.display_name.trim()),
      image: reqBody.image,
      status: reqBody.status,
      created_on: current_datetime,
      updated_on: current_datetime
    };
    //console.log(arrData);
    let duplicate = await findAttributeByAttribute({ name: arrData.name })
    if (duplicate.error) {
      return res.status(200).json({
        success: false,
        message: duplicate.error,
        error: error
      });
    } else {
      if (duplicate.result) {
        error.name = 'Name already exists.'
        return res.status(200).json({
          success: false,
          message: 'Check the form for errors.',
          error: error
        });
      } else {

        const objAttribute = new Attribute(arrData);
        objAttribute.save((err, resultSet) => {
          if (err) {
            return res.status(200).json({
              success: false,
              message: JSON.stringify(err),
              error: error
            });
          } else {
            const arrData = {
              fk_id: resultSet._id,
              type: 'static',
              keyword: reqBody.keyword,
              top_description: reqBody.top_description,
              bottom_description: reqBody.bottom_description,
              created_on: current_datetime,
              updated_on: current_datetime
            };
            const SeoObj = new Seo(arrData);
            SeoObj.save((errSeo) => {
              if (errSeo) {
                return res.status(200).json({
                  success: false,
                  message: JSON.stringify(errSeo),
                  error: error
                });
              } else {
                return res.status(200).json({
                  success: true,
                  message: 'Attribute has been created successfully.',
                  error: error
                });
              }
            });
          }
        });
      }
    }
  }
});

// Update action start from here
router.post('/update', async (req, res) => {
  let error = {
    name: '',
    display_name: ''
  },
    reqBody = req.body;

  let validationResult = await validateForm(reqBody);
  if (!validationResult.success) {
    return res.status(200).json({
      success: false,
      message: validationResult.message,
      error: validationResult.error
    });
  } else {
    const now = new Date();
    const current_datetime = dateFormat(now, "yyyy-mm-dd HH:MM:ss");

    const arrData = {
      name: reqBody.name,
      display_name: jsUcfirst(reqBody.display_name),
      image: reqBody.image,
      status: reqBody.status,
      updated_on: current_datetime
    };
    let duplicate = await findAttributeByAttribute({
      name: arrData.name,
      _id: { $ne: mongoose.Types.ObjectId(reqBody._id) }
    })
    if (duplicate.error) {
      return res.status(200).json({
        success: false,
        message: duplicate.error,
        error: error
      });
    } else {
      if (duplicate.result) {
        error.name = 'Name already exists.'
        return res.status(200).json({
          success: false,
          message: 'Check the form for errors.',
          error: error
        });
      } else {

        Attribute.update({ _id: reqBody._id }, { $set: arrData }, (err, resultSet) => {
          if (err) {
            return res.status(200).json({
              success: false,
              message: JSON.stringify(err),
              error: error
            });
          } else {
            Seo.findOne({ fk_id: reqBody._id }, function (seoErr, seoRes) {
              if (seoErr) {
                return res.status(200).json({
                  success: false,
                  message: JSON.stringify(seoErr),
                  error: error
                });
              } else {
                if (seoRes) {
                  const arrData = {
                    keyword: reqBody.keyword,
                    top_description: reqBody.top_description,
                    bottom_description: reqBody.bottom_description,
                    updated_on: current_datetime
                  };
                  Seo.update({ fk_id: reqBody._id }, { $set: arrData }, (errSeo, seoUpdateRes) => {
                    if (errSeo) {
                      return res.status(200).json({
                        success: false,
                        message: JSON.stringify(errSeo),
                        error: error
                      });
                    } else {
                      return res.status(200).json({
                        success: true,
                        message: 'Record has been updated successfully.',
                        error: error
                      });
                    }
                  });
                } else {

                  const arrData = {
                    fk_id: reqBody._id,
                    type: 'static',
                    keyword: reqBody.keyword,
                    top_description: reqBody.top_description,
                    bottom_description: reqBody.bottom_description,
                    created_on: current_datetime,
                    updated_on: current_datetime
                  };
                  const SeoObj = new Seo(arrData);
                  SeoObj.save((errSeo) => {
                    if (errSeo) {
                      return res.status(200).json({
                        success: false,
                        message: JSON.stringify(errSeo),
                        error: error
                      });
                    } else {
                      return res.status(200).json({
                        success: true,
                        message: 'Record has been updated successfully.',
                        error: error
                      });
                    }
                  });
                }
              }
            });
          }
        });
      }
    }
  }
});

// Category list
router.post('/list', (req, res) => {
  let reqData = req.body;
  //console.log("reqData =>", reqData)
  let limit = parseInt(reqData.limit);
  let currentPage = parseInt(reqData.currentPage);
  if (currentPage > 0) {
    currentPage = currentPage - 1;
  }
  let skip = parseInt(limit * currentPage);

  let promises = Attribute.find().skip(skip).limit(limit);
  let totalItems = Attribute.count();
  if (reqData.name && reqData.name != "") {
    promises.where({ name: { '$regex': '^' + reqData.name, '$options': 'i' } });
    totalItems.where({ name: { '$regex': '^' + reqData.name, '$options': 'i' } });
  }
  if (reqData.status && reqData.status != "") {
    promises.where('status').equals(reqData.status);
    totalItems.where('status').equals(reqData.status);
  }
  //exec promisses
  totalItems.exec(function (errCount, resultCount) {

    promises.exec(function (err, resultSet) {
      if (err) {
        return res.status(200).json({
          message: err,
          arrData: []
        });
      } else {
        var arrResultSet = [];
        if (resultSet) {
          resultSet.forEach(function (items) {
            var image = '/images/category-placeholder.png';
            var fileExists = checkFileExistsSync(rootPath + '/public/images/' + items.image);
            if (fileExists && items.image) {
              image = '/images/' + items.image;
            }
            var created_on = dateFormat(items.created_on, "yyyy-mm-dd");
            var updated_on = dateFormat(items.updated_on, "yyyy-mm-dd");
            var arrTemp = {
              _id: items._id,
              display_name: items.display_name,
              name: items.name,
              image: image,
              created_on: created_on,
              updated_on: updated_on,
              status: items.status == '1' ? "Active" : "Inactive"
            };
            arrResultSet.push(arrTemp);
          });
        }
        return res.status(200).json({
          message: '',
          arrData: arrResultSet,
          total: Math.ceil(resultCount / limit)
        });
      }
    });
  });
});

// Find by pk
router.post('/findByPk', (req, res) => {
  var reqData = req.body;
  Attribute.findOne({ _id: reqData.id }, function (err, arrData) {
    if (err) {
      return res.status(200).json({
        message: err,
        attribute: [],
        seo: []
      });
    } else {
      Seo.findOne({ fk_id: reqData.id, type: 'static' }, function (errSeo, arrSeoData) {
        if (errSeo) {
          return res.status(200).json({
            message: errSeo,
            attribute: [],
            seo: []
          });
        } else {
          //console.log(arrCatData);
          return res.status(200).json({
            message: '',
            attribute: arrData,
            seo: arrSeoData
          });
        }
      });
    }
  });
});
module.exports = router;
