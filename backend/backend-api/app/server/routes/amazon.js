const express = require('express');
var mongoose = require('mongoose');
const dateFormat = require('dateformat');
var replaceall = require("replaceall");
const isset = require('isset');
const validator = require('validator');
var async = require('async');

const Category = require('mongoose').model('Category');
const SubCategory = require('mongoose').model('SubCategory');
const Attribute = require('mongoose').model('Attribute');
const SubCatAttribute = require('mongoose').model('SubCatAttribute');
const AttributeMapping = require('mongoose').model('AttributeMapping');
const Product = require('mongoose').model('Product');
const ProductMapping = require('mongoose').model('ProductMapping');
const ProductVendor = require('mongoose').model('ProductVendor');

const router = new express.Router();
var amazon = require('amazon-product-api');
var affiliate = require('snapdeal-affiliate-client');

var client = amazon.createClient({
  awsId: "AKIAJNRCYNFD3CPTKKDQ",
  awsSecret: "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",
  awsTag: "coupon25-20"
});

// var snapdealClient = affiliate.createClient({
//   SdAffId: '31712',
//   SdAffToken: '0beb1c80df235b3e5c3c77bfcc4739',
//   responseType: 'json'
// });

/**
 * Validate the sign up form
 *
 * @param {object} payload - the HTTP body message
 * @returns {object} The result of validation. Object contains a boolean validation result,
 *                   errors tips, and a global message for the whole form.
 */
function validateForm(payload) {
  const errors = {};
  let isFormValid = true;
  let message = '';

  if (payload.SearchIndex =='') {
    isFormValid = false;
    errors.SearchIndex = 'Category cannot be blank.';
  }
  if (!isFormValid) {
    message = 'Please fixed error.';
  }

  return {
    success: isFormValid,
    message,
    errors
  };
}

function getWords(str) {
    var splitStr = str.split(' ').slice(0, 20).join(' ');
    return splitStr;
}
function amoutFormat(amout) {
   var formated =0;
    if(amout !="" && amout >0)
     {
       formated = amout/100;
     }
     return formated;
}

function subcatIdentifier(arrAncestors, catID, productID, productImg, arrAtt){

  arrAncestors.forEach(function(ancestorItem){
  if(isset(ancestorItem.BrowseNode)){
  var objType = typeof ancestorItem.BrowseNode;
  if(objType =='object')
  {
    var arrAncestors = ancestorItem.BrowseNode[0].Ancestors;
    var objTypeAncestors = typeof ancestorItem.BrowseNode[0].Ancestors;
    if(objTypeAncestors =='object' && !isset(ancestorItem.BrowseNode[0].Ancestors[0].BrowseNode[0].IsCategoryRoot)){
    return subcatIdentifier(ancestorItem.BrowseNode[0].Ancestors, catID, productID, productImg, arrAtt);
    }else{
      subcategory = ancestorItem.BrowseNode[0].Name[0];
      //console.log("Return  =="+JSON.stringify(subcategory));
      //sleep.sleep(5);
      //console.log("Call after 5 sec. ="+catResultSet);
      if(isset(subcategory))
       {
         SubCategory.findOne({fk_category_id:catID,name: subcategory}, function(subCaterr, subCatResultSet){
          if(subCaterr)
          {
           console.log("Error Occurs during subcategory ="+subCaterr);
          }else{
            if(subCatResultSet)
            {
              subCatAttributeUpdate(subCatResultSet._id, arrAtt);
              attrProdMapping(subCatResultSet._id, arrAtt, productID);
            }else{
              // create subcat
              var now = new Date();
              var current_datetime =dateFormat(now, "yyyy-mm-dd HH:MM:ss");
              var replaceSpecialChar = subcategory.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
              var replaceSpace =replaceSpecialChar.replace(/  +/g, ' ');
              var slug =replaceall(" ", "-", replaceSpace).toLowerCase();

              var tempSubCatObj ={
                fk_category_id:catID,
                name:subcategory,
                slug: slug,
                description:'',
                image: productImg,
                status:1,
                created_on: current_datetime,
                updated_on: current_datetime
              }
              var objSubCat = new SubCategory(tempSubCatObj);
              objSubCat.save((subCatErr, subCatInsertResultSet) => {
              if(subCatErr)
               {
                // console.log("Error during sub category insertion =="+subCatErr);
               }else{
                 if(subCatInsertResultSet)
                 {
                   subCatAttributeUpdate(subCatInsertResultSet._id, arrAtt);
                   attrProdMapping(subCatInsertResultSet._id, arrAtt, productID);
                 }
               }
             });
            }
         }
       });
      } // end subcat
    }
   }
  }
});
}

function subCatAttributeUpdate(fk_subcat_id, arrAtt){
  var arrAttribute =[];
  arrAtt.forEach(function(items){
    arrAttribute.push(items.key);
  });
  Attribute.find({name: { $in: arrAttribute }}, function(attrErr, arrData){
    if(attrErr)
    {
      console.log("Error Occurs during attr fetching "+attrErr);
    }else{
       if(arrData)
       {
         arrData.forEach(function(item){
            SubCatAttribute.findOne({fk_subcat_id: fk_subcat_id, fk_attribute_id: item._id}, function(err, resultSet){
              if(err)
              {
                console.log("Error Occurs during subcatattr fetching "+err);
              }else{
                if(resultSet){

                }else{
                  var now = new Date();
                  var current_datetime =dateFormat(now, "yyyy-mm-dd HH:MM:ss");
                  var tempObj ={
                    fk_subcat_id:fk_subcat_id,
                    fk_attribute_id:item._id,
                    status:1,
                    created_on: current_datetime,
                    updated_on: current_datetime
                  }
                  var obj = new SubCatAttribute(tempObj);
                  obj.save((insertErr, insertResultSet) => {
                  if(insertErr)
                   {
                     console.log("Error during sub category Attr insertion =="+insertErr);
                   }
                 });
                }
              }
            })
         });
       }
    }
  });
}

function attrProdMapping(fk_subcat_id, arrAtt, fk_product_id){

  var arrAttribute =[];
  var now = new Date();
  var current_datetime =dateFormat(now, "yyyy-mm-dd HH:MM:ss");
  arrAtt.forEach(function(items){
    arrAttribute.push(items.key);
  });
  Attribute.find({name: { $in: arrAttribute }}, function(attrErr, arrData){
    if(attrErr)
    {
      console.log("Error Occurs during attr fetching "+attrErr);
    }else{
       if(arrData)
       {
         arrData.forEach(function(item){
            AttributeMapping.findOne({fk_subcat_id: fk_subcat_id, fk_attribute_id: item._id}, function(err, resultSet){
              if(err)
              {
                console.log("Error Occurs during subcatattr fetching "+err);
              }else{
                if(resultSet){
                  var tempObj ={
                    fk_product_id: fk_product_id,
                    fk_attribute_mapping_id: resultSet._id,
                    name: resultSet.name,
                    slug: resultSet.slug,
                    status:1,
                    created_on: current_datetime,
                    updated_on: current_datetime
                  }

                  var obj = new ProductMapping(tempObj);
                  obj.save((insertProdErr, insertProdResultSet) => {
                  if(insertProdErr)
                   {
                     console.log("Error during product map insertion =="+insertProdErr);
                   }
                 });
                }else{
                  arrAtt.forEach(function(itemsAttr){
                    if(itemsAttr.key ==item.name)
                    {
                      var replaceSpecialChar = itemsAttr.value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
                      var replaceSpace =replaceSpecialChar.replace(/  +/g, ' ');
                      var slug =replaceall(" ", "-", replaceSpace).toLowerCase();

                      var tempObj ={
                        fk_subcat_id:fk_subcat_id,
                        fk_attribute_id:item._id,
                        name: itemsAttr.value,
                        slug: slug,
                        parent_id:'',
                        status:1,
                        created_on: current_datetime,
                        updated_on: current_datetime
                      }
                      var obj = new AttributeMapping(tempObj);
                      obj.save((insertErr, insertResultSet) => {
                      if(insertErr)
                       {
                         console.log("Error during sub category Attr insertion =="+insertErr);
                       }else{
                         if(insertResultSet){
                           var tempObj ={
                             fk_product_id: fk_product_id,
                             fk_attribute_mapping_id: insertResultSet._id,
                             name: insertResultSet.name,
                             slug: insertResultSet.slug,
                             status:1,
                             created_on: current_datetime,
                             updated_on: current_datetime
                           }

                           var obj = new ProductMapping(tempObj);
                           obj.save((insertProdErr, insertProdResultSet) => {
                           if(insertProdErr)
                            {
                              console.log("Error during product map insertion =="+insertProdErr);
                            }
                          });

                         }
                       }
                     });
                    }
                  });
                }
              }
            })
         });
       }
    }
  });
}

function attributeMgt(...params){

  var display_name = params[0].replace(/([a-z])([A-Z])/g, '$1 $2');
  Attribute.findOne({display_name:display_name, name:params[0]}, function(attrErr, attrResultSet){
   if(attrErr)
   {
     console.log("Error during attrubute fetching =="+attrErr);
   }else{
     if(!attrResultSet)
     {
       // Insert Attribute
     var attrTemp ={
         display_name: display_name,
         name:params[0],
         image:params[1],
         description:'',
         status:1,
         created_on: params[2],
         updated_on: params[2]
       }
     var attributeObj = new Attribute(attrTemp);
     attributeObj.save((attrErr, attributeResultSet) =>{
     if(attrErr)
       {
         //console.log("Error during attribute insertion ==");
       }else{
         console.log("Attribute has been inserted =="+attributeResultSet.name);
       }
     });
     }   // End Attribute insertion
   }
 });
}

router.post('/search', (req, res) => {

var reqBody = req.body;
const validationResult = validateForm(reqBody);
if (!validationResult.success) {
return res.status(400).json({
  success: false,
  message: validationResult.message,
  errors: validationResult.errors
});
}else{
client.itemSearch(reqBody, function(err, results, response) {
if (err) {
 console.log("Error ==",err);
 return res.status(400).json({
 success: false,
 message: "Error: "+err[0].Error[0].Message[0],
 errors:{},
 arrData:[],
 apiErro: err
 });
}else{
var now = new Date();
var current_datetime =dateFormat(now, "yyyy-mm-dd HH:MM:ss");
var arrAttrSkip =["Binding","EAN","Feature","HardwarePlatform", "Label", "LegalDisclaimer","Manufacturer", "NumberOfItems", "PackageQuantity", "PartNumber","ProductGroup", "ProductTypeName", "Title","UPC", "MPN"];
var arrDataResult =[];
results.forEach(function(items){
// build attribute array
var arrItemList = items.ItemAttributes[0];
var arrAtt =[];
for (var key in arrItemList) {
  var valueType =typeof arrItemList[key];
  if(!arrAttrSkip.includes(key) && arrItemList[key] !='[object Object]')
  {
    var temp = {key: key, value:arrItemList[key][0]};
    arrAtt.push(temp);
  }
}

// check ASIN is exists
if(isset(items.ASIN[0])){

Category.findOne({search_index:reqBody.SearchIndex}, function(catErr, catResultSet){
if(catErr)
{
 console.log("Error occure during featiching category ="+catErr);
}else{
 if(catResultSet)
{

 Product.findOne({vendor:'amazon', product_id:items.ASIN[0]}, function(err, arrProdData){
 if(err)
 {
  console.log("Product Error: "+err);
 }else{
 if(arrProdData)
  {
    // do update product
  }else{

    // Start product insertion
    var title =isset(items.ItemAttributes[0].Title[0])? items.ItemAttributes[0].Title[0]:'';
    var replaceSpecialChar = title.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
    var replaceSpace =replaceSpecialChar.replace(/  +/g, ' ');
    var splitString = getWords(replaceSpace);
    var slug =replaceall(" ", "-", splitString).toLowerCase();
    var price =0;
    if(isset(items.ItemAttributes[0].ListPrice))
    {
      price = amoutFormat(items.ItemAttributes[0].ListPrice[0].Amount[0]);
    }else {
       if(isset(items.OfferSummary)){
         if(isset(items.OfferSummary[0].LowestUsedPrice)){
           if(isset(items.OfferSummary[0].LowestUsedPrice[0].Amount)){
             price = amoutFormat(items.OfferSummary[0].LowestUsedPrice[0].Amount[0]);
           }
         }
     }
    }

    // Product part start here
   var arrProdTemp ={
      vendor: 'amazon',
      product_id: items.ASIN[0],
      name: title,
      slug:slug,
      description: isset(items.EditorialReviews)? items.EditorialReviews[0].EditorialReview[0].Content[0]:'',
      feature: isset(items.ItemAttributes[0].Feature)? items.ItemAttributes[0].Feature[0]:'',
      price: price,
      smallImage: isset(items.SmallImage)? items.SmallImage[0].URL[0]:'',
      mediumImage: isset(items.MediumImage)? items.MediumImage[0].URL[0]:'',
      largeImage: isset(items.LargeImage)? items.LargeImage[0].URL[0]:'',
      status:1,
      created_on: current_datetime,
      updated_on: current_datetime
    }
   var productObj = new Product(arrProdTemp);
   productObj.save((productResultErr, productResultSet) => {
   if(productResultErr)
    {
      console.log("Error during product insertion =="+productResultErr)
    }else{
      // Product Vendor part start here
      var jsonProductVendor ={
        fk_product_id :productResultSet._id,
        vendor: productResultSet.vendor,
        product_id: productResultSet.product_id,
        name: productResultSet.name,
        product_detail_url: items.DetailPageURL[0],
        price: price,
        stock: 'In Stock',
        status: 1,
        created_on: current_datetime,
        updated_on: current_datetime
      }
      var productVendorObj = new ProductVendor(jsonProductVendor);
      productVendorObj.save((productVendorErr, productVendorResultSet) =>{
      if(productVendorErr)
      {
       console.log("Error during product Vendor insertion =="+productVendorErr);
     }
    }); // end of product vendor insertion

    // async execution Attribute
    var i = 0;
    async.each(arrAtt, function(listItem, next) {
      attributeMgt(listItem.key, productResultSet.mediumImage, current_datetime);
      i++;
      if(i == arrAtt.length)
      {
        // Call Subcategory
        var arrAncestors = items.BrowseNodes[0].BrowseNode[0].Ancestors;
        subcatIdentifier(arrAncestors, catResultSet._id,productResultSet._id, productResultSet.mediumImage, arrAtt);
      }
      next();
    }, function(err) {
    });
  }
  }); // end product saved
  }  // end product insertion
 }
}); // emd product check exists
}
}
}); // End cat exists
} // end check ASIN is exists


    }); //end main foreach part
      return res.status(200).json({
        success: true,
        message: '',
        errors: {},
        arrData: results,
        apiErro:''
      });
   } // end else part
  }); // search items
 } // end else part
}); // end function here

// Category list
router.post('/categoryList', (req, res) => {

  Category.find({status:1}, function(err, resultSet){
    if(err)
    {
      return res.status(409).json({
        message: err,
        arrData:{}
      });
    }else{
      var arrResultSet =[];
      if(resultSet)
      {
        resultSet.forEach(function(items){
           var arrTemp ={
              id: items.search_index,
              name: items.name
           };
           arrResultSet.push(arrTemp);
        });
      }
      return res.status(200).json({
        message: '',
        arrData:arrResultSet
      });
    }
  });
});
module.exports = router;
