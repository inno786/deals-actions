const express = require('express');
var mongoose = require('mongoose');
const dateFormat = require('dateformat');
const asyncLoop = require('node-async-loop');
const Client = require('flipkart-affiliate');
const FlipKartCategory = require('mongoose').model('FlipKartCategory');
const FlipkartProduct = require('mongoose').model('FlipkartProduct');
const apiCredintial = require('../../config/apiCredintial.js');

let FkCategory = require('../../helper/FkCategory')
let FkCategoryObj = new FkCategory;

const router = new express.Router();

function jsUcfirst(string) {
    let newString = string.replace(/\b[a-z]/g, function (f) {
        return f.toUpperCase();
    });
    return newString.replace(/_/g, " ");
}

// Category list
router.post('/list', async (req, res) => {

    let outPut = {
        message: '',
        arrData: [],
        total: 0
    }

    let reqData = req.body;
    let limit = parseInt(reqData.limit);
    let currentPage = parseInt(reqData.currentPage);
    if (currentPage > 0) {
        currentPage = currentPage - 1;
    }
    let skip = parseInt(limit * currentPage);

    let FKPromises = FlipKartCategory.find().skip(skip).limit(limit);
    let totalItems = FlipKartCategory.count();

    if (reqData.resource && reqData.resource != "") {
        FKPromises.where('resource').equals(reqData.resource);
        totalItems.where('resource').equals(reqData.resource);
    }
    if (reqData.status && reqData.status != "") {
        FKPromises.where('status').equals(reqData.status);
        totalItems.where('status').equals(reqData.status);
    }

    let totalItem = await findTotalItems(totalItems);
    let catResponse = await queryExecute(FKPromises);
    
    if (catResponse.error) {
        outPut.message = 'Error occure during category list'
        return  res.status(200).json(outPut)
    } else {
        let arrResultSet = [];
        if (catResponse.result.length > 0) {
            asyncLoop(catResponse.result, (items, callback) => {
                if (items) {
                    var created_on = dateFormat(items.created_on, "yyyy-mm-dd H:MM:ss");
                    var updated_on = dateFormat(items.updated_on, "yyyy-mm-dd H:MM:ss");
                    arrResultSet.push({
                        _id: items._id,
                        name: items.name,
                        resource: items.resource,
                        url: items.url,
                        status: items.status,
                        created_on: created_on,
                        updated_on: updated_on
                    });
                }
                callback();
            }, err => {
                outPut.arrData = arrResultSet
                outPut.total = totalItem ? Math.ceil(parseInt(totalItem) / limit) : 0
                return res.status(200).json(outPut)
            });
        } else {
            outPut.message = 'No item found.'
            return res.status(200).json(outPut)
        }
    }
});

// Get Category
router.post('/getCategory', async (req, res) => {
    let response = await FkCategoryObj.getCategory();
     return res.status(200).json({message: JSON.stringify(response.message)})
});

router.post('/changeStatus', (req, res) => {
    let reqBody = req.body;
    let resource = reqBody.resource;
    let currentStatus = reqBody.currentStatus;
     // console.log("resource =>",resource);
     // console.log("currentStatus =>",currentStatus);
    if (currentStatus != "" && resource.length > 0) {
        asyncLoop(resource, function (obj, callback) {
            const arrData = {
                status: currentStatus
            };
            FlipKartCategory.update({ resource: obj }, { $set: arrData }, (err, resultSet) => {
                if (err) {
                    return res.status(200).json({
                        message: "Error has been occured."
                    });
                } else {
                    callback();
                }
            });
        }, function (err) {
            if (err) {
                return res.status(200).json({
                    message: "Error has been occured."
                });
            } else {
                return res.status(200).json({
                    message: "Records has been updated successfully."
                });
            }
        });
    } else {
        return res.status(200).json({
            message: "Please select atleast one items"
        });
    }
});

// Category list
router.post('/product_list', async (req, res) => {

    let outPut = {
        message: '',
        arrData: [],
        total: 0
    }

    let reqData = req.body;
    let limit = parseInt(reqData.limit);
    let currentPage = parseInt(reqData.currentPage);
    if (currentPage > 0) {
        currentPage = currentPage - 1;
    }
    let skip = parseInt(limit * currentPage);
    let fkCatProduct = FlipkartProduct.aggregate([
        {
          $lookup: {
            from: "flipkartcategories",
            localField: "resource",
            foreignField: "resource",
            as: "fkCat"
          }
        },
        {
          $match: {
            resource: (reqData.resource && reqData.resource != "") ? reqData.resource : { $ne: '' }
          }
        },
        {
            $skip: skip
          },
          {
            $limit: limit
          },
        {
          $project: {
            _id: "$_id",
            name:"$fkCat.name",
            resource:"$resource",
            status: "$status",
            created_on:"$created_on",
            updated_on:"$updated_on"
          }
        }
      ]);
      let totalItems = FlipkartProduct.aggregate([
        {
          $lookup: {
            from: "flipkartcategories",
            localField: "resource",
            foreignField: "resource",
            as: "fkCat"
          }
        },
        {
          $match: {
            resource: (reqData.resource && reqData.resource != "") ? reqData.resource : { $ne: '' }
          }
        },
        { $group: { _id: null, count: { $sum: 1 } } }
      ]);

    let totalItem = await findTotalItemsAggregate(totalItems);
    let productResponse = await queryExecute(fkCatProduct);

    //console.log("catResponse =>", catResponse)
    if (productResponse.error) {
        outPut.message = 'Error occure during category list'
        return  res.status(200).json(outPut)
    } else {
        let arrResultSet = [];
        if (productResponse.result.length > 0) {
            asyncLoop(productResponse.result, (items, callback) => {
                if (items) {
                    var created_on = dateFormat(items.created_on, "yyyy-mm-dd H:MM:ss");
                    var updated_on = dateFormat(items.updated_on, "yyyy-mm-dd H:MM:ss");
                    arrResultSet.push({
                        _id: items._id,
                        name:items.name[0],
                        resource: items.resource,
                        status: items.status,
                        created_on: created_on,
                        updated_on: updated_on
                    });
                }
                callback();
            }, err => {
                outPut.arrData = arrResultSet
                outPut.total = totalItem ? Math.ceil(parseInt(totalItem) / limit) : 0
                return res.status(200).json(outPut)
            });
        } else {
            outPut.message = 'No item found.'
            return res.status(200).json(outPut)
        }
    }
});


router.post('/changeCatProd', (req, res) => {
    let reqBody = req.body;
    let resource = reqBody.resource;
    let currentStatus = reqBody.currentStatus;
     // console.log("resource =>",resource);
    //  console.log("currentStatus =>",currentStatus);

    if (currentStatus != "" && resource.length > 0) {
      
       asyncLoop(resource,  (item, callback) =>{

            if(currentStatus === 'delete')
            {
                FlipkartProduct.remove({ _id: mongoose.Types.ObjectId(item)}, (err, resultSet) => {
                    if (err) {
                        console.error(err);
                    }
                    callback();
                });
            }else{
                FlipkartProduct.update({ _id: mongoose.Types.ObjectId(item) }, { $set: {status:currentStatus} }, (err, resultSet) => {
                if (err) {
                    console.error(err);
                }
                callback();
            });
           }
        }, err =>{
            return res.status(200).json({
                message: "Records has been updated successfully."
            });
        }); 
    } else {
        return res.status(200).json({
            message: "Please select atleast one items"
        });
    }
});

function findTotalItems(reqData)
{
    return new Promise(resolve =>{
        return reqData.exec((error, result) => {
            let response =  error ? 0 : parseInt(result);
            return resolve(response)
        })
    })
}
function findTotalItemsAggregate(reqData)
{
    return new Promise(resolve =>{
        return reqData.exec((error, result) => {
            let response =  result.length >0 ? parseInt(result[0].count):0;
            return resolve(response)
        })
    })
}
function queryExecute(reqData)
{
    return new Promise(resolve =>{
        return reqData.exec((error, result) => {
            if(error)
            {
                return resolve({error:error})
            }else{
                return resolve({error:null, result:result})
            }
        })
    })
}

module.exports = router;
