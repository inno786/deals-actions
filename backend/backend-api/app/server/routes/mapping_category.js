const express = require('express');
const mongoose = require('mongoose');
const dateFormat = require('dateformat');
const Category = require('mongoose').model('Category');
const SubCategory = require('mongoose').model('SubCategory');
const CategoryMapping = require('mongoose').model('CategoryMapping');
const asyncLoop = require('node-async-loop');
var fs = require('fs');
const router = new express.Router();

/**
 * Validate the sign up form
 *
 * @param {object} payload - the HTTP body message
 * @returns {object} The result of validation. Object contains a boolean validation result,
 *                   errors tips, and a global message for the whole form.
 */
function validateForm(payload) {
  const errors = {};
  let isFormValid = true;
  let message = '';

  if (payload.fk_category_id =='') {
    isFormValid = false;
    errors.fk_category_id = 'Please select category';
  }
  if (payload.source =='') {
    isFormValid = false;
    errors.source = 'Please select source';
  }
  if (payload.fk_category_parent_id =='') {
    isFormValid = false;
    errors.fk_category_parent_id = 'Please select feed category';
  }

  if (!isFormValid) {
    message = 'Please fixed error.';
  }

  return {
    success: isFormValid,
    message,
    errors
  };
}
function checkFileExistsSync(filepath){
  let flag = true;
  try{
    fs.accessSync(filepath, fs.F_OK);
  }catch(e){
    flag = false;
  }
  return flag;
}


router.post('/create', (req, res) => {

  let reqBody = req.body,
      arrSubcat =reqBody.arrSubcat,
      now = new Date(),
      current_datetime =dateFormat(now, "yyyy-mm-dd HH:MM:ss");

   if(arrSubcat.length >0)
   {
    CategoryMapping.remove({fk_category_id:reqBody.fk_category_id,source:reqBody.source,fk_category_parent_id:reqBody.fk_category_parent_id}).exec(function(errRemove, arrRemove){

      asyncLoop(arrSubcat,  (elem, callback) =>{
        // body...
        var arrData = {
          fk_category_id: reqBody.fk_category_id,
          source: reqBody.source,
          fk_category_parent_id: reqBody.fk_category_parent_id,
          fk_sub_category_id: elem.id,
          fk_sub_category_name: elem.name,
          status: 1,
          created_on:current_datetime,
          updated_on:current_datetime
        };
      //  console.log(arrData);
        var model = new CategoryMapping(arrData);
        model.save((err, resultSet) => {
          if (err) {
            callback();
          }else{
            callback();
          }
       });
      },function(err){

					if(err){
             res.status(200).json({
            success: false,
            message: 'Error occurred during update',
            errors:{}
            });
					}else{
            return res.status(200).json({
               success: true,
               message: 'Record has been inserted successfully.',
               errors:{}
             });
					}
				});
      });
    
}else{
   res.status(200).json({
    success: false,
    message: 'Please select atleat one subcategory.',
    errors:{}
    }); 
 }  
});

// Category list
router.post('/list', (req, res) => {

  let reqData = req.body;
  let limit = parseInt(reqData.limit);
  let currentPage = parseInt(reqData.currentPage);
  if(currentPage >0)
  {
    currentPage = currentPage-1;
  }
  let skip = parseInt(limit * currentPage);

  let promises =  CategoryMapping.find().populate({ path: 'fk_category_id', select: 'name' }).skip(skip).limit(limit);
  let totalItems =  CategoryMapping.count().populate({ path: 'fk_category_id', select: 'name' });

  if(reqData.fk_category_id && reqData.fk_category_id !="")
  {
    promises.where('fk_category_id').equals(reqData.fk_category_id);
    totalItems.where('fk_category_id').equals(reqData.fk_category_id);
  }
  if(reqData.fk_sub_category_name && reqData.fk_sub_category_name !="")
  {
    promises.where({fk_sub_category_name:{'$regex': '^'+reqData.fk_sub_category_name, '$options' : 'i'}});
    totalItems.where({fk_sub_category_name:{'$regex': '^'+reqData.fk_sub_category_name, '$options' : 'i'}});
  }
  if(reqData.status && reqData.status !="")
  {
    promises.where('status').equals(reqData.status);
    totalItems.where('status').equals(reqData.status);
  }

  totalItems.exec(function(errCount, resultCount){

    promises.exec(function(err, resultSet){
      if(err)
      {
        return res.status(409).json({
          message: err,
          arrData:{}
        });
      }else{
        //console.log("Result Data =>",resultSet);
        var arrResultSet =[];
        if(resultSet)
        {
          //console.log(resultSet);
          asyncLoop(resultSet, (items, callback) => {

            if(items)
            {

             var image ='/images/category-placeholder.png';
             var fileExists = checkFileExistsSync(rootPath + '/public/images/'+items.image);
             if(fileExists && items.image)
             {
               image ='/images/'+items.image;
             }
             var created_on = dateFormat(items.created_on, "yyyy-mm-dd");
             var updated_on = dateFormat(items.updated_on, "yyyy-mm-dd");
             var arrTemp ={
                _id: items._id,
                fk_sub_category_name: items.fk_sub_category_name,
                category: items.fk_category_id.name,
                fk_category_parent_id: items.fk_category_parent_id,
                image: image,
                source: items.source,
                status: items.status =='1'? "Active":"Inactive",
                created_on: created_on,
                updated_on: updated_on
             };
             arrResultSet.push(arrTemp);
             callback()
            }else{
              callback()
            }
            
          }, err =>{
            return res.status(200).json({
              message: '',
              arrData:arrResultSet,
              total: Math.ceil(resultCount/limit)
            });
          });
        }
      }
    });
  });
});

// Find by pk
router.post('/findByPk', (req, res) => {

});
module.exports = router;
