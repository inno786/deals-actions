const express = require('express');
var mongoose = require('mongoose');
const dateFormat = require('dateformat');
const Product = require('mongoose').model('Product');
const router = new express.Router();

// Category list
router.post('/list', (req, res) => {

      let output ={
        message: '',
        arrData:[],
        total:0
    }
    let reqData = req.body;
    let limit = parseInt(reqData.limit);
    let currentPage = parseInt(reqData.currentPage);
    if(currentPage >0)
    {
      currentPage = currentPage-1;
    }
    let skip = parseInt(limit * currentPage);

    let promises = Product.find().skip(skip).limit(limit);
    let totalItems = Product.count();

    if(reqData.name && reqData.name !="")
    {
      promises.where({name:{'$regex': '^'+reqData.name, '$options' : 'i'}});
      totalItems.where({name:{'$regex': '^'+reqData.name, '$options' : 'i'}});
    }
    if(reqData.vendor && reqData.vendor !="")
    {
      promises.where('vendor').equals(reqData.vendor);
      totalItems.where('vendor').equals(reqData.vendor);
    }
    if(reqData.status && reqData.status !="")
    {
 
      promises.where('status').equals(reqData.status);
      totalItems.where('status').equals(reqData.status);
    }

    //exec promisses
  totalItems.exec(function(errCount, resultCount){

    promises.exec(function(err, resultSet){
      if(err)
      {
         output.message = err
         res.send(output);
      }else{
         output.arrData = resultSet
         output.total = Math.ceil(resultCount/limit)
         res.json(output);
      }
    });
  });
});

module.exports = router;
