const express = require('express');
const mongoose = require('mongoose');
const asyncLoop = require('node-async-loop');
const dateFormat = require('dateformat');
const Category = require('mongoose').model('Category');
const SubCategory = require('mongoose').model('SubCategory');
const Attribute = require('mongoose').model('Attribute');
const SubCatAttribute = require('mongoose').model('SubCatAttribute');
const AttributeMapping = require('mongoose').model('AttributeMapping');
const FlipKartCategory = require('mongoose').model('FlipKartCategory');


const router = new express.Router();

// Category list
router.post('/categoryList', (req, res) => {

  let reqData = req.body;
  let promises = Category.find();

  if (reqData.main_category && reqData.main_category != "") {
    promises.where('main_category').equals(reqData.main_category);
  }
  if (reqData.feed && reqData.feed != "") {
    promises.where({ source: { $ne: "" } });
  }

  //exec promisses
  promises.exec(function (err, resultSet) {
    if (err) {
      return res.status(200).json({
        message: err,
        arrData: []
      });
    } else {
      return res.status(200).json({
        message: '',
        arrData: resultSet
      });
    }
  });
});

// Sub Category list
router.post('/subcategoryList', (req, res) => {

  let reqData = req.body;
  let promises = SubCategory.aggregate([
    {
      $lookup: {
        from: "categorymappings",
        localField: "_id",
        foreignField: "fk_sub_category_id",
        as: "subCatMap"
      }
    },
    {
      $match: {
        status: 1,
        fk_category_id: reqData.fk_category_id ? mongoose.Types.ObjectId(reqData.fk_category_id) : 0,
        source: reqData.source
      }
    },
    {
      $project: {
        _id: "$_id",
        name: "$name",
        source: "$source",
        subCatMap:
          {
            $filter:
              {
                input: "$subCatMap",
                as: "subCatMapping",
                cond: { $eq: ["$$subCatMapping.fk_category_id", reqData.fk_category_id ? mongoose.Types.ObjectId(reqData.fk_category_id) : 0], $eq: ["$$subCatMapping.source", reqData.source] }
              }
          }
      }
    }
  ]);
  //exec promisses
  promises.exec(function (err, resultSet) {
    if (err) {
      return res.status(200).json({
        message: err,
        arrData: []
      });
    } else {
      let arrResult = [];
      if (resultSet) {

        asyncLoop(resultSet, (elem, callback) => {
          //console.log("name =>", elem.name)
          //console.log("id =>", elem._id)
          if (elem) {
            var temp = {
              _id: elem._id,
              name: elem.name,
              source: elem.source
            };
            if (elem.subCatMap.length > 0) {
              temp.mapped = true;
            } else {
              temp.mapped = false;
            }
            arrResult.push(temp);
            callback()
          } else {
            callback()
          }
        }, err => {
          return res.status(200).json({
            message: err,
            arrData: arrResult
          });
        });
      } else {
        return res.status(200).json({
          message: 'no record',
          arrData: []
        });
      }
    }
  });
});

// Attribute list
router.post('/attributeList', (req, res) => {

  let reqData = req.body;
  let promises = Attribute.aggregate([
    {
      $match: {
        status: 1
      }
    },
    {
      $lookup: {
        from: "subcatattributes",
        localField: "_id",
        foreignField: "fk_attribute_id",
        as: "attrMap"
      }
    },
    {
      $project: {
        display_name: "$display_name",
        name: "$name",
        image: "$image",
        attrMap:
          {
            $filter:
              {
                input: "$attrMap",
                as: "attrMapping",
                cond: { $eq: ["$$attrMapping.fk_subcat_id", mongoose.Types.ObjectId(reqData.fk_subcat_id)] }
              }
          }
      }
    }
  ]);
  //exec promisses
  promises.exec(function (err, resultSet) {
    if (err) {
      return res.status(409).json({
        message: err,
        arrData: []
      });
    } else {
      let arrResult = [];
      if (resultSet) {
        resultSet.forEach(elem => {
          var temp = {
            _id: elem._id,
            name: elem.name,
            display_name: elem.display_name,
            image: elem.image,
            status: elem.status
          };
          if (elem.attrMap.length > 0) {
            elem.attrMap.forEach(elem1 => {
              temp.mapped = true;
            });
          } else {
            temp.mapped = false;
          }
          arrResult.push(temp);
        });
      }
      return res.status(200).json({
        message: '',
        arrData: arrResult
      });
    }
  });
});

// mapped Subcategory List
router.post('/mappedSubcatlist', (req, res) => {

  let reqData = req.body;

  let promises = SubCatAttribute.find().populate({ path: 'fk_attribute_id', select: '_id display_name' });

  if (reqData.fk_subcat_id && reqData.fk_subcat_id != "") {
    promises.where('fk_subcat_id').equals(reqData.fk_subcat_id);
  }

  //exec promisses
  promises.exec(function (err, resultSet) {
    if (err) {
      return res.status(409).json({
        message: err,
        arrData: []
      });
    } else {
      var arrResultSet = [];

      if (resultSet) {
        resultSet.forEach(elem => {
          // body...
          var temp = { _id: elem.fk_attribute_id._id, display_name: elem.fk_attribute_id.display_name };
          arrResultSet.push(temp);
        });
      }
      console.log("dfsdf =>", resultSet);
      return res.status(200).json({
        message: '',
        arrData: arrResultSet
      });
    }
  });
});


// Flipkart category list
router.post('/flipkartCategoryList', (req, res) => {

  let reqData = req.body;

  let promises = FlipKartCategory.find();

  //exec promisses
  promises.exec((err, resultSet) => {
    if (err) {
      return res.send(200, { message: err, arrData: [] })
    } else {
      let arrResultSet = [];
      if (resultSet) {
        asyncLoop(resultSet, (item, callback) => {
          if (item) {
            arrResultSet.push({ resource: item.resource, name: item.name })
          }
          callback()

        }, err => {
          return res.status(200).json({
            message: '',
            arrData: arrResultSet
          })
        })
      } else {
        return res.status(200).json({ message: '', arrData: [] })
      }
    }
  });
});


module.exports = router;
