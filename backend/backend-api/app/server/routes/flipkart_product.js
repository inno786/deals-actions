const express = require('express');
var mongoose = require('mongoose');
const dateFormat = require('dateformat');
const asyncLoop = require('node-async-loop');
const Client = require('flipkart-affiliate');
const FlipkartProduct = require('mongoose').model('FlipkartProduct');
const FlipKartCategory = require('mongoose').model('FlipKartCategory');
const apiCredintial = require('../../config/apiCredintial.js');
const router = new express.Router();

// Define credintial details
const affObj = Client.createClient({
  FkAffId: apiCredintial.flipkart.affID,
  FkAffToken: apiCredintial.flipkart.token,
  responseType: 'json'
});

let FkProduct = require('../../helper/FkProduct')
let FkProductObj = new FkProduct;

function jsUcfirst(string)
{
    let newString = string.replace(/\b[a-z]/g,function(f){return f.toUpperCase();});
    return newString.replace(/_/g, " ");
}

// Category list
router.post('/list', async (req, res) => {

    let outPut = {
      message: '',
      arrData: [],
      total: 0
    }

    let reqData = req.body;
    let limit = parseInt(reqData.limit);
    let currentPage = parseInt(reqData.currentPage);
    if(currentPage >0)
    {
      currentPage = currentPage-1;
    }
    let skip = parseInt(limit * currentPage);

    let FKPromises = FlipkartProduct.find().populate({ path: 'resource', select: 'resource name' }).skip(skip).limit(limit);
    let totalItems = FlipkartProduct.count().populate({ path: 'resource', select: 'resource name' });

    if(reqData.resource && reqData.resource !="")
    {
      FKPromises.where('resource').equals(reqData.resource);
      totalItems.where('resource').equals(reqData.resource);
    }
    if(reqData.status && reqData.status !="")
    {
      FKPromises.where('status').equals(reqData.status);
      totalItems.where('status').equals(reqData.status);
    }

    let totalItem = await findTotalItems(totalItems);
    let response = await queryExecute(FKPromises);

    if (response.error) {
      outPut.message = 'Error occure during category list'
      return  res.status(200).json(outPut)
  } else {
        var arrResultSet =[];
        if(response.result.length >0)
        {
          asyncLoop(response.result, (items, callback) =>{

             var created_on = dateFormat(items.created_on, "yyyy-mm-dd");
             var updated_on = dateFormat(items.updated_on, "yyyy-mm-dd");
             var arrTemp ={
                _id: items._id,
                name: items.resource.name,
                status: items.status,
                created_on: created_on,
                updated_on: updated_on
             };
             arrResultSet.push(arrTemp);
             callback();
          }, error =>{
            outPut.arrData = arrResultSet
            outPut.total = totalItem ? Math.ceil(parseInt(totalItem) / limit) : 0
            return res.status(200).json(outPut)
          });
        }else{
          return res.status(200).json(outPut)
        }
      }
});

// Get Product
router.post('/getProduct', async (req, res) => {
    let response = await FkProductObj.getProduct(req.body);
    return res.status(200).json(response);
});

// Get Product
router.post('/removeProduct', (req, res) => {

let reqBody = req.body;
if(reqBody.id !='')
{
 // console.log("reqBody.id =>", reqBody.id)
  FlipKartCategory.findOne({_id:reqBody.id}, function(err, arrData){
    if(err)
    {
      return res.status(200).json({
         message:  JSON.stringify(err)
       });
    }else{
      if(arrData){
          FlipkartProduct.remove({resource:arrData.resource},function(errRemove, arrDataRemove){
            if(errRemove)
            {
              return res.status(200).json({
                 message:  JSON.stringify(errRemove)
               });
             }else{
               return res.status(200).json({
                  message:  "Product has been removed successfully"
                });
             }
          });
        }
    }
  });
}
});


function findTotalItems(reqData)
{
    return new Promise(resolve =>{
        return reqData.exec((error, result) => {
            let response =  error ? 0 : parseInt(result);
            return resolve(response)
        })
    })
}
function findTotalItemsAggregate(reqData)
{
    return new Promise(resolve =>{
        return reqData.exec((error, result) => {
            let response =  result.length >0 ? parseInt(result[0].count):0;
            return resolve(response)
        })
    })
}
function queryExecute(reqData)
{
    return new Promise(resolve =>{
        return reqData.exec((error, result) => {
            if(error)
            {
                return resolve({error:error})
            }else{
                return resolve({error:null, result:result})
            }
        })
    })
}

module.exports = router;
