const router = require('express').Router();
const mongoose = require('mongoose');
const dateFormat = require('dateformat');
const DynamicSeo = mongoose.model('DynamicSeo');

function validateForm(payload) {
    let error = {
        type: '',
    },
        isFormValid = true,
        message = '';

    if (payload.type == '') {
        isFormValid = false;
        error.name = 'Please select type';
    }

    if (!isFormValid) {
        message = 'Please fixed error.';
    }

    return {
        success: isFormValid,
        message,
        error
    };
}

function findDynamicSeoByAttribute(condition) {
    return new Promise(resolve => {
        return DynamicSeo.findOne(condition, (err, result) => {
            if (err) {
                return resolve({ error: JSON.stringify(err) })
            } else {
                if (result) {
                    return resolve({ error: null, result: true })
                } else {
                    return resolve({ error: null, result: false })
                }
            }
        })
    })
}

router.post('/create', async (req, res) => {

    let error = {
        type: ''
    },
        reqBody = req.body;

    let validationResult = await validateForm(reqBody);
    if (!validationResult.success) {
        return res.status(200).json({
            success: false,
            message: validationResult.message,
            error: validationResult.error
        });
    } else {
        const now = new Date();
        const current_datetime = dateFormat(now, "yyyy-mm-dd HH:MM:ss");

        const arrData = {
            type: reqBody.type,
            title: reqBody.title.trim(),
            keyword: reqBody.keyword.trim(),
            description: reqBody.description.trim(),
            top_description: reqBody.top_description.trim(),
            bottom_description: reqBody.bottom_description.trim(),
            created_on: current_datetime,
            updated_on: current_datetime
        };

        let duplicate = await findDynamicSeoByAttribute({ type: arrData.type })
        if (duplicate.error) {
            return res.status(200).json({
                success: false,
                message: duplicate.error,
                error: error
            });
        } else {
            if (duplicate.result) {
                error.name = 'Seo is already exists.'
                return res.status(200).json({
                    success: false,
                    message: 'Check the form for errors.',
                    error: error
                });
            } else {
                //console.log(arrData);
                const newDynamicSeo = new DynamicSeo(arrData);
                newDynamicSeo.save((err, resultSet) => {
                    if (err) {
                        return res.status(200).json({
                            success: false,
                            message: JSON.stringify(err),
                            error: error
                        });
                    } else {
                        return res.status(200).json({
                            success: true,
                            message: 'Category has been created successfully.',
                            error: error
                        });
                    }
                });
            }
        }
    }
});

// Update action start from here
router.post('/update', async (req, res) => {

    let error = {
        type: ''
    },
        reqBody = req.body;
    let validationResult = await validateForm(reqBody);
    if (!validationResult.success) {
        return res.status(200).json({
            success: false,
            message: validationResult.message,
            error: validationResult.error
        });
    } else {
        const now = new Date();
        const current_datetime = dateFormat(now, "yyyy-mm-dd HH:MM:ss");
        const arrData = {
            type: reqBody.type,
            title: reqBody.title.trim(),
            keyword: reqBody.keyword.trim(),
            description: reqBody.description.trim(),
            top_description: reqBody.top_description.trim(),
            bottom_description: reqBody.bottom_description.trim(),
            created_on: current_datetime,
            updated_on: current_datetime
        };
        let duplicate = await findDynamicSeoByAttribute({
            type: arrData.type,
            _id: { $ne: mongoose.Types.ObjectId(reqBody._id) }
        })
        if (duplicate.error) {
            return res.status(200).json({
                success: false,
                message: duplicate.error,
                error: error
            });
        } else {
            if (duplicate.result) {
                error.name = 'Seo is already exists.'
                return res.status(200).json({
                    success: false,
                    message: 'Check the form for errors.',
                    error: error
                });
            } else {
                DynamicSeo.update({ _id: reqBody._id }, { $set: arrData }, (err, resultSet) => {
                    if (err) {
                        return res.status(200).json({
                            success: false,
                            message: 'Could not process the form.',
                            error: error
                        });
                    } else {
                        return res.status(200).json({
                            success: true,
                            message: 'Record has been updated successfully.',
                            error: error
                        });
                    }
                });
            }
        }
    }
});

// Seo list
router.post('/list', (req, res) => {
    let reqData = req.body;
    let promises = DynamicSeo.find();
    promises.exec(function (err, resultSet) {
        if (err) {
            return res.status(200).json({
                message: err,
                arrData: []
            });
        } else {
            return res.status(200).json({
                message: '',
                arrData: resultSet
            });
        }
    });
});

// Find by pk
router.post('/findByPk', (req, res) => {
    var reqData = req.body;
    DynamicSeo.findOne({ _id: reqData.id }, function (err, result) {
        if (err) {
            return res.status(200).json({
                message: err,
                arrData: {}
            });
        } else {
            return res.status(200).json({
                message: null,
                arrData: result
            });
        }
    });
});

// Remove by pk
router.post('/deleteByPk', (req, res) => {
    var reqData = req.body;
    DynamicSeo.remove({ _id: reqData.id }, function (err, result) {
        if (err) {
            return res.status(200).json({
                message: err
            });
        } else {
            return res.status(200).json({
                message: null
            });
        }
    });
});

module.exports = router;
