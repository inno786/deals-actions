const router = require('express').Router();
const mongoose = require('mongoose');
const Attribute = mongoose.model('Attribute');
const AttributeMapping = mongoose.model('AttributeMapping');
const AttributeValue = mongoose.model('AttributeValue');
const Category = mongoose.model('Category');
const CategoryMapping = mongoose.model('CategoryMapping');
const FlipKartCategory = mongoose.model('FlipKartCategory');
const FlipkartProduct = mongoose.model('FlipkartProduct');
const ObjectTaxonomy = mongoose.model('ObjectTaxonomy');
const Product = mongoose.model('Product');
const ProductSpecific = mongoose.model('ProductSpecific');
const ProductSpecificList = mongoose.model('ProductSpecificList');
const Seo = mongoose.model('Seo');
const SubCatAttribute = mongoose.model('SubCatAttribute');
const SubCategory = mongoose.model('SubCategory');

router.post('/clearData', (req, res) => {

    Promise.all([
        AttributeFun(),
        AttributeMappingFun(),
        AttributeValueFun(),
        CategoryFun(),
        CategoryMappingFun(),
        FlipKartCategoryFun(),
        FlipkartProductFun(),
        ObjectTaxonomyFun(),
        ProductFun(),
        ProductSpecificFun(),
        ProductSpecificListFun(),
        SeoFun(),
        SubCatAttributeFun(),
        SubCategoryFun()
    ]).then(data => {
        //console.log("data =>", data);
        return res.status(200).json({
            success: true,
            message: 'Schema has been reset successfully.'
        });
    });
});

const AttributeFun = () => {
    return new Promise(resolve => {
        Attribute.collection.drop(error => {
            return resolve({ error: error})
        });
    })
}
const AttributeMappingFun = () => {
    return new Promise(resolve => {
        AttributeMapping.collection.drop(error => {
            return resolve({ error: error})
        });
    })
}
const AttributeValueFun = () => {
    return new Promise(resolve => {
        AttributeValue.collection.drop(error => {
            return resolve({ error: error})
        });
    })
}
const CategoryFun = () => {
    return new Promise(resolve => {
        Category.collection.drop(error => {
            return resolve({ error: error})
        });
    })
}
const CategoryMappingFun = () => {
    return new Promise(resolve => {
        CategoryMapping.collection.drop(error => {
            return resolve({ error: error})
        });
    })
}
const FlipKartCategoryFun = () => {
    return new Promise(resolve => {
        FlipKartCategory.collection.drop(error => {
            return resolve({ error: error})
        });
    })
}
const FlipkartProductFun = () => {
    return new Promise(resolve => {
        FlipkartProduct.collection.drop(error => {
            return resolve({ error: error})
        });
    })
}
const ObjectTaxonomyFun = () => {
    return new Promise(resolve => {
        ObjectTaxonomy.collection.drop(error => {
            return resolve({ error: error})
        });
    })
}
const ProductFun = () => {
    return new Promise(resolve => {
        Product.collection.drop(error => {
            return resolve({ error: error})
        });
    })
}
const ProductSpecificFun = () => {
    return new Promise(resolve => {
        ProductSpecific.collection.drop(error => {
            return resolve({ error: error})
        });
    })
}
const ProductSpecificListFun = () => {
    return new Promise(resolve => {
        ProductSpecificList.collection.drop(error => {
            return resolve({ error: error})
        });
    })
}
const SeoFun = () => {
    return new Promise(resolve => {
        Seo.collection.drop(error => {
            return resolve({ error: error})
        });
    })
}
const SubCatAttributeFun = () => {
    return new Promise(resolve => {
        SubCatAttribute.collection.drop(error => {
            return resolve({ error: error})
        });
    })
}
const SubCategoryFun = () => {
    return new Promise(resolve => {
        SubCategory.collection.drop(error => {
            return resolve({ error: error})
        });
    })
}


module.exports = router;
