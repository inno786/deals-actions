const express = require('express');
const mongoose = require('mongoose');
const dateFormat = require('dateformat');
const replaceall = require("replaceall");
const SubCategory = require('mongoose').model('SubCategory');
const Attribute = require('mongoose').model('Attribute');
const AttributeMapping = require('mongoose').model('AttributeMapping');
const Seo = require('mongoose').model('Seo');
const async = require('node-async-loop');
var fs = require('fs');
const router = new express.Router();

/**
 * Validate the sign up form
 *
 * @param {object} payload - the HTTP body message
 * @returns {object} The result of validation. Object contains a boolean validation result,
 *                   errors tips, and a global message for the whole form.
 */
function validateForm(payload) {
  const errors = {};
  let isFormValid = true;
  let message = '';

  if (payload.fk_subcat_id =='') {
    isFormValid = false;
    errors.fk_subcat_id = 'Please select subcategory.';
  }
  if (payload.fk_attribute_id =='') {
    isFormValid = false;
    errors.fk_attribute_id = 'Please select attribute.';
  }
  if (payload.display_name =='') {
    isFormValid = false;
    errors.display_name = 'Display name cannot be blank.';
  }
  if (payload.name =='') {
    isFormValid = false;
    errors.name = 'Name cannot be blank.';
  }

  if (!isFormValid) {
    message = 'Please fixed error.';
  }

  return {
    success: isFormValid,
    message,
    errors
  };
}
function jsUcfirst(string)
{
    return string.replace(/\b[a-z]/g,function(f){return f.toUpperCase();});
}

function checkFileExistsSync(filepath){
  let flag = true;
  try{
    fs.accessSync(filepath, fs.F_OK);
  }catch(e){
    flag = false;
  }
  return flag;
}

function objectFindByKey(array, key, value) {
    for (var i = 0; i < array.length; i++) {
        if (array[i][key] === value) {
            return array[i];
        }
    }
    return null;
}

router.post('/create', (req, res) => {

  let reqBody = req.body;
  const validationResult = validateForm(reqBody);
  if (!validationResult.success) {
    return res.status(400).json({
      success: false,
      message: validationResult.message,
      errors: validationResult.errors
    });
  }else{

    const now = new Date();
    const current_datetime =dateFormat(now, "yyyy-mm-dd HH:MM:ss");
    const arrData = {
      fk_subcat_id: reqBody.fk_subcat_id.trim(),
      fk_attribute_id: reqBody.fk_attribute_id.trim(),
      display_name: jsUcfirst(reqBody.display_name.trim()),
      name: reqBody.name.trim(),
      slug: replaceall(" ", "-", reqBody.name.trim()).toLowerCase(),
      image: reqBody.image.trim(),
      status: reqBody.status.trim(),
      parent_id: reqBody.parent_id.trim(),
      created_on:current_datetime,
      updated_on:current_datetime
    };
    //console.log(arrData);
    const objAttributeMapping = new AttributeMapping(arrData);
    objAttributeMapping.save((err, resultSet) => {
      if (err) {
          return res.status(400).json({
          success: false,
          message: 'Could not process the form.',
          errors:{}
          });
      }else{
          const arrData = {
            fk_id: resultSet._id,
            type: 'static',
            keyword: reqBody.keyword.trim(),
            top_description: reqBody.top_description.trim(),
            bottom_description: reqBody.bottom_description.trim(),
            created_on:current_datetime,
            updated_on:current_datetime
          };
       const SeoObj = new Seo(arrData);
       SeoObj.save((errSeo) => {
          if(errSeo){
              return res.status(400).json({
                success: false,
                message: errSeo,
                errors:{}
                });
          }else{
             return res.status(200).json({
                success: true,
                message: 'Record has been added successfully.',
                errors:{}
              });
          }
       });
      }
    });
  }
});


// Update action start from here
router.post('/update', (req, res) => {

  reqBody = req.body;
  const validationResult = validateForm(reqBody);
  if (!validationResult.success) {
    return res.status(400).json({
      success: false,
      message: validationResult.message,
      errors: validationResult.errors
    });
  }else{

  const now = new Date();
  const current_datetime =dateFormat(now, "yyyy-mm-dd HH:MM:ss");

  const arrData = {
    fk_subcat_id: reqBody.fk_subcat_id.trim(),
    fk_attribute_id: reqBody.fk_attribute_id.trim(),
    display_name: jsUcfirst(reqBody.display_name.trim()),
    name: reqBody.name.trim(),
    slug: replaceall(" ", "-", reqBody.name.trim()).toLowerCase(),
    image: reqBody.image.trim(),
    status: reqBody.status.trim(),
    parent_id: reqBody.parent_id.trim(),
    updated_on:current_datetime
  };
  AttributeMapping.update({_id:reqBody._id},{$set :arrData},(err, resultSet) => {
    if (err) {

        return res.status(400).json({
        success: false,
        message: 'Could not process the form.',
        errors:{}
        });
    }else{

      Seo.findOne({fk_id:reqBody._id}, function(seoErr, seoRes){
        if(seoErr){
            return res.status(400).json({
              success: false,
              message: seoErr,
              errors:{}
              });
        }else{
          if(seoRes){
            const arrData = {
              keyword: reqBody.keyword.trim(),
              top_description: reqBody.top_description.trim(),
              bottom_description: reqBody.bottom_description.trim(),
              updated_on:current_datetime
            };
            Seo.update({fk_id:reqBody._id},{$set :arrData},(errSeo, seoUpdateRes) => {
               if(errSeo){
                   return res.status(400).json({
                     success: false,
                     message: errSeo,
                     errors:{}
                     });
               }else{
                  return res.status(200).json({
                     success: true,
                     message: 'Record has been updated successfully.',
                     errors:{}
                   });
               }
            });
          }else{

            const arrData = {
              fk_id: reqBody._id,
              type: 'static',
              keyword: reqBody.keyword.trim(),
              top_description: reqBody.top_description.trim(),
              bottom_description: reqBody.bottom_description.trim(),
              created_on:current_datetime,
              updated_on:current_datetime
            };
            const SeoObj = new Seo(arrData);
            SeoObj.save((errSeo) => {
            if(errSeo){
                return res.status(400).json({
                  success: false,
                  message: errSeo,
                  errors:{}
                  });
            }else{
               return res.status(200).json({
                  success: true,
                  message: 'Record has been updated successfully.',
                  errors:{}
                });
            }
            });
          }
        }
      });
    }
  });
 }
});

// Category list
router.post('/list', (req, res) => {

  let reqData = req.body;
  let limit = parseInt(reqData.limit);
  let currentPage = parseInt(reqData.currentPage);
  if(currentPage >0)
  {
    currentPage = currentPage-1;
  }
  let skip = parseInt(limit * currentPage);

  let promises =  AttributeMapping.find().populate({ path: 'fk_subcat_id', select: '_id name' }).populate({ path: 'fk_attribute_id', select: '_id display_name' }).skip(skip).limit(limit);
  let totalItems =  AttributeMapping.count().populate({ path: 'fk_subcat_id', select: '_id name' }).populate({ path: 'fk_attribute_id', select: '_id display_name' });

  if(reqData.fk_subcat_id && reqData.fk_subcat_id !="")
  {
    promises.where('fk_subcat_id').equals(reqData.fk_subcat_id);
    totalItems.where('fk_subcat_id').equals(reqData.fk_subcat_id);
  }
  if(reqData.fk_attribute_id && reqData.fk_attribute_id !="")
  {
    promises.where('fk_attribute_id').equals(reqData.fk_attribute_id);
    totalItems.where('fk_attribute_id').equals(reqData.fk_attribute_id);
  }

  totalItems.exec(function(errCount, resultCount){

    promises.exec(function(err, resultSet){
      if(err)
      {
        return res.status(409).json({
          message: err,
          arrData:{}
        });
      }else{
        var arrResultSet =[];
        if(resultSet.length)
        {
        async(resultSet, function (elem, callback){

             var checkExistSubCat = objectFindByKey(arrResultSet, 'fk_subcat_id', elem.fk_subcat_id._id);
             var checkExistAttr = objectFindByKey(arrResultSet, 'fk_attribute_id', elem.fk_attribute_id._id);
             if(checkExistSubCat && checkExistAttr)
              {
                checkExistSubCat.attr.push({_id:elem._id,display_name:elem.display_name});
              }else{
                  var temp ={
                    fk_subcat_id:elem.fk_subcat_id._id,
                    fk_attribute_id:elem.fk_attribute_id._id,
                    subcat_name:elem.fk_subcat_id.name,
                    attribute_name:elem.fk_attribute_id.display_name,
                    attr:[{_id:elem._id,display_name:elem.display_name}]
                  };
                  arrResultSet.push(temp);
                }
              callback();
            }, function(err){
              if(err)
              {
                console.log("New Errror =>",err);
              }else{
              //  console.log("RRR =>",arrResultSet);
              let totalObj = parseInt(arrResultSet.length);
              return res.status(200).json({
                message: '',
                arrData:arrResultSet,
                total: Math.ceil(totalObj/limit)
              });
              }
            });

        }else{
          return res.status(200).json({
            message: err,
            arrData:[],
            total:0
          });
        }
      }
    });
  });
});


// Find by pk
router.post('/findByPk', (req, res) => {

  var reqData = req.body;
  AttributeMapping.findOne({_id:reqData.id}, function(err, objData){
    if(err)
    {
       return res.status(409).json({
        message: err,
        arrData: [],
        seo: []
      });
     }else{
        Seo.findOne({fk_id:reqData.id, type: 'static'}, function(errSeo, arrSeoData){
           if(errSeo)
           {
             return res.status(409).json({
              message: errSeo,
              arrData: [],
              seo: []
            });
          }else{
            //console.log(arrCatData);
            return res.status(200).json({
              message: '',
              arrData: objData,
              seo: arrSeoData
            });
          }
        });
     }
  });
});
// Find by id subcategory
router.post('/subCategoryById', (req, res) => {

  let reqData = req.body;
  if(reqData.id)
  {
    let promises =  AttributeMapping.find().populate({ path: 'fk_subcat_id', select: '_id name' });
    promises.exec(function(err, resultSet){
      if(err)
      {
        return res.status(409).json({
          message: err,
          arrData:[]
        });
      }else{
        let arrResultSet =[];
        if(resultSet.length >0)
        {
          async(resultSet, function (elem, callback){
            var temp ={
                _id:elem.fk_subcat_id._id,
                name:elem.fk_subcat_id.name
              };
             arrResultSet.push(temp);
            callback();
            }, function(err){
                if(err)
                {
                  console.log("New Errror =>",err);
                }else{
                    return res.status(200).json({
                      message: '',
                      arrData:arrResultSet
                    });
                }
              });

        }else{
          return res.status(200).json({
            message: '',
            arrData:[]
          });
        }
      }
    });
  }
});
// Find by id attribute
router.post('/attributeById', (req, res) => {

  let reqData = req.body;
  if(reqData.id)
  {
    let promises =  AttributeMapping.find().populate({ path: 'fk_attribute_id', select: '_id display_name' });
    promises.exec(function(err, resultSet){
      if(err)
      {
        return res.status(409).json({
          message: err,
          arrData:[]
        });
      }else{
        let arrResultSet =[];
        if(resultSet.length >0)
        {
          async(resultSet, function (elem, callback){
            var temp ={
                _id:elem.fk_attribute_id._id,
                display_name:elem.fk_attribute_id.display_name
              };
             arrResultSet.push(temp);
            callback();
            }, function(err){
                if(err)
                {
                  console.log("New Errror =>",err);
                }else{
                    return res.status(200).json({
                      message: '',
                      arrData:arrResultSet
                    });
                }
              });

        }else{
          return res.status(200).json({
            message: '',
            arrData:[]
          });
        }
      }
    });
  }
});
module.exports = router;
