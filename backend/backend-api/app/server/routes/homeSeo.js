const router = require('express').Router();
const mongoose = require('mongoose');
const dateFormat = require('dateformat');
const replaceall = require("replaceall");
const HomeSeo = mongoose.model('HomeSeo');

function validateForm(payload) {
    let error = {
        title: ''
    },
        isFormValid = true;

    if (payload.title == '') {
        isFormValid = false;
        error.title = 'Title cannot be blank.';
    }
    return {
        success: isFormValid,
        error
    };
}

function findHomeSeo() {
    return new Promise(resolve => {
        return HomeSeo.findOne({}, (err, result) => {
            if (err) {
                return resolve({ error: JSON.stringify(err) })
            } else {
                if (result) {
                    return resolve({ error: null, result: true, id: result._id })
                } else {
                    return resolve({ error: null, result: false })
                }
            }
        })
    })
}

router.post('/create-update', async (req, res) => {

    let error = {
        title: ''
    },
        reqBody = req.body;

    let validationResult = await validateForm(reqBody);
    if (!validationResult.success) {
        return res.status(200).json({
            success: false,
            error: validationResult.error
        });
    } else {
        const now = new Date();
        const current_datetime = dateFormat(now, "yyyy-mm-dd HH:MM:ss");

        const arrData = {
            title: reqBody.title,
            keyword: reqBody.keyword,
            description: reqBody.description,
            top_description: reqBody.top_description,
            bottom_description: reqBody.bottom_description,
            banner: reqBody.banner,
            banner_status: reqBody.banner_status,
            created_on: current_datetime,
            updated_on: current_datetime
        };

        let duplicate = await findHomeSeo();
        if (duplicate.error) {
            return res.status(200).json({
                success: false,
                message: duplicate.error,
                error: error
            });
        } else {
            if (duplicate.result) {
                HomeSeo.update({ _id: duplicate.id }, { $set: arrData }, (err, resultSet) => {
                    if (err) {
                        return res.status(200).json({
                            success: false,
                            message: 'Could not process the form.',
                            error: error
                        });
                    } else {
                        return res.status(200).json({
                            success: true,
                            message: 'Home Seo has been updated successfully.',
                            error: error
                        });
                    }
                });
            } else {
                //console.log(arrData);
                const newHomeSeo = new HomeSeo(arrData);
                newHomeSeo.save((err, resultSet) => {
                    if (err) {
                        return res.status(200).json({
                            success: false,
                            message: JSON.stringify(err),
                            error: error
                        });
                    } else {
                        return res.status(200).json({
                            success: true,
                            message: 'Home Seo has been created successfully.',
                            error: error
                        });
                    }
                });
            }
        }
    }
});

// Find by pk
router.post('/findSeo', (req, res) => {
    HomeSeo.findOne({}, (err, result) => {
        if (err) {
            return res.status(200).json({
                message: err,
                arrData: {}
            });
        } else {
            return res.status(200).json({
                message: null,
                arrData: result
            });
        }
    });
});
module.exports = router;
