/*
 * To handle the User collection in the connected database means a User model
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var CategoryMappingSchema = new Schema({
  fk_category_id:{
      type: Schema.Types.ObjectId,
      ref: 'Category'
  },
  source:{
      type: String,
      ref: 'Category'
  },
  fk_category_parent_id:{
      type: Schema.Types.ObjectId,
      ref: 'Category'
  },
  fk_sub_category_id:{
      type: Schema.Types.ObjectId
  },
  fk_sub_category_name:{
      type: String
  },
  status:{
      type: Number,default:0
  },
  created_on:{
        type:Date,default:Date.now()
    },
  updated_on:{
      type:Date,default:Date.now()
  }
});
module.exports = mongoose.model('CategoryMapping',CategoryMappingSchema);
