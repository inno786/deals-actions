/*
 * To handle the User collection in the connected database means a User model
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var SubCategorySchema = new Schema({
    fk_category_id:{
        type: Schema.Types.ObjectId,
		ref: 'Category'
    },
    name: {
         type : String
    },
    slug: {
         type : String
    },
    feed_name: {
         type : String
    },
    description:{
        type: String
    },
    image:{
        type: String
    },
    status:{
        type: Number,default:0
    },
    source:{
      type: String
    },
    created_on:{
        type:Date,default:Date.now()
    },
    updated_on:{
        type:Date,default:Date.now()
    }
});
module.exports = mongoose.model('SubCategory',SubCategorySchema);
