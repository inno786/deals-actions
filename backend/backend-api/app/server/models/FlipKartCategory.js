/*
 * To handle the User collection in the connected database means a User model
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var FlipKartCategorySchema = new Schema({
    name: {
         type : String
    },
    resource: {
         type : String
    },
    url:{
        type: String
    },
    status:{
        type: String
    },
    created_on:{
        type:Date,default:Date.now()
    },
    updated_on:{
        type:Date,default:Date.now()
    }
});
module.exports = mongoose.model('FlipKartCategory',FlipKartCategorySchema);
