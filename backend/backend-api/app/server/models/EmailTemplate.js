/* 
 * To handle the User collection in the connected database means a User model
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var EmailTemplateSchema = new Schema({
    template:{
        type : String , unique : true, required : true, dropDups: true
    },
    subject:{
        type: String,required:true
    },
    from:{
        type: String,required:true
    },
    template_body:{
        type: String,required:true
    },
    status:{
        type: Number,default:0
    },
    created_on:{
        type:Date,default:Date.now()
    },
    updated_on:{
        type:Date,default:Date.now()
    }
});
module.exports = mongoose.model('EmailTemplate',EmailTemplateSchema);
