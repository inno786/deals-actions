/*
 * To handle the User collection in the connected database means a User model
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var SubCatAttributeSchema = new Schema({
    fk_subcat_id:{
        type: Schema.Types.ObjectId,
        ref: 'SubCategory'
    },
    fk_attribute_id:{
         type: Schema.Types.ObjectId,
         ref: 'Attribute'
    },
  fk_attribute_display_name: {
         type : String
    },
    status:{
        type: Number,default:0
    },
    created_on:{
        type:Date,default:Date.now()
    },
    updated_on:{
        type:Date,default:Date.now()
    }
});
module.exports = mongoose.model('SubCatAttribute',SubCatAttributeSchema);
