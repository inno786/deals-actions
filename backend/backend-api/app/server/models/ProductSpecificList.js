/*
 * To handle the User collection in the connected database means a User model
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ProductSpecificListSchema = new Schema({
    fk_product_id:{
        type: Schema.Types.ObjectId,
        ref: 'Product'
    },
    fk_ProductSpecific_id:{
        type: Schema.Types.ObjectId,
        ref: 'ProductSpecific'
    },
    key: {
        type : String
    },
    value: {
        type : String
    },
    status:{
        type: Number,default:0
    }
});
module.exports = mongoose.model('ProductSpecificList',ProductSpecificListSchema);
