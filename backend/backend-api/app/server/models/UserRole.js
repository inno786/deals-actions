/* 
 * To handle the User collection in the connected database means a User model
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var UserRoleSchema = new Schema({
    name:{
        type: String, unique : true, required : true, dropDups: true
    },
    description:{
        type: String
    },
    status:{
       type: Number,default:0
    },
    created_on:{
        type:Date,default:Date.now()
    },
    updated_on:{
        type:Date,default:Date.now()
    }
});
module.exports = mongoose.model('UserRole',UserRoleSchema);