const mongoose = require('mongoose');

module.exports.connect = (uri) => {
  mongoose.connect(uri);
  mongoose.connection.on('error', (err) => {
    console.error(`Mongoose connection error: ${err}`);
    process.exit(1);
  });

  // load models
  require('./User');
  require('./UserRole');
  require('./UserInRole');
  require('./Category');
  require('./CategoryMapping');
  require('./SubCategory');
  require('./Attribute');
  require('./AttributeValue');
  require('./ObjectTaxonomy');
  require('./SubCatAttribute');
  require('./AttributeMapping');
  require('./ProductSpecific');
  require('./ProductSpecificList');
  require('./Product');
  require('./Seo');
  require('./DynamicSeo');
  require('./HomeSeo');
  require('./FlipKartCategory');
  require('./FlipkartProduct');
  require('./EmailTemplate');
};
