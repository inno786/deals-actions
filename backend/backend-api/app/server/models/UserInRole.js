/* 
 * To handle the User collection in the connected database means a User model
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserInRoleSchema = new Schema({
    fk_user_role_id:{
        type: Schema.Types.ObjectId,
		ref: 'UserRole'
    },
    fk_user_id:{
         type: Schema.Types.ObjectId,
         ref: 'UserModel'
    },
    status:{
        type: Number,default:0
    },
    created_on:{
        type:Date,default:Date.now()
    },
    updated_on:{
        type:Date,default:Date.now()
    }
});
module.exports = mongoose.model('UserInRole',UserInRoleSchema);