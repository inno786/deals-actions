const mongoose = require('mongoose');
var passwordHash = require('password-hash');

// define the User model schema
const UserSchema = new mongoose.Schema({
  name: String,
  email: {
    type: String,
    index: { unique: true }
  },
  password: String,
  mobile: String,
  role: String,
  status: Number,default:0 ,
  created_on:{
        type:Date,default:Date.now()
    },
    updated_on:{
        type:Date,default:Date.now()
    } 
});


/**
 * Compare the passed password with the value in the database. A model method.
 *
 * @param {string} password
 * @returns {object} callback
 */
UserSchema.methods.comparePassword = function comparePassword(password, callback) {
  
    getResult = passwordHash.verify(password, this.password);
    callback(null,getResult);
};


/**
 * The pre-save hook method.
 */
UserSchema.pre('save', function saveHook(next) {
  const user = this;

  // proceed further only if the password is modified or the user is new
  if (!user.isModified('password')) return next();

   user.password = passwordHash.generate(user.password);
   return next();
});


module.exports = mongoose.model('User', UserSchema);
