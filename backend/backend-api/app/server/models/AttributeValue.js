/*
 * To handle the User collection in the connected database means a User model
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var AttributeValueSchema = new Schema({

    fk_attribute_id:{
         type: Schema.Types.ObjectId,
         ref: 'Attribute'
    },
    display_name: {
         type : String
    },
    name: {
         type : String
    },
    slug: {
         type : String
    },
    parent_id: {
         type : String
    },
    image: {
         type : String
    },
    status:{
        type: Number,default:1
    },
    created_on:{
        type:Date,default:Date.now()
    },
    updated_on:{
        type:Date,default:Date.now()
    }
});
module.exports = mongoose.model('AttributeValue',AttributeValueSchema);
