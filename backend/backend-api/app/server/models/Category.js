/*
 * To handle the User collection in the connected database means a User model
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var CategorySchema = new Schema({
    name: {
         type : String
    },
    slug: {
         type : String
    },
    search_index:{
        type: String
    },
    description:{
        type: String
    },
    image:{
        type: String
    },
    status:{
        type: Number,default:0
    },
    main_category:{
        type: Number,default:0
    },
    source:{
      type: String
    },
    created_on:{
        type:Date,default:Date.now()
    },
    updated_on:{
        type:Date,default:Date.now()
    }
});
module.exports = mongoose.model('Category',CategorySchema);
