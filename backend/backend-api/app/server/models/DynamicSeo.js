/* 
 * To handle the User collection in the connected database means a User model
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var DynamicSeoSchema = new Schema({
    type: {
         type : String
    },
    title: {
        type : String
    },
    keyword: {
         type : String
    },
    description: {
        type : String
    },
    top_description:{
        type: String
    },
    bottom_description:{
        type: String
    },
    created_on:{
        type:Date,default:Date.now()
    },
    updated_on:{
        type:Date,default:Date.now()
    }
});
module.exports = mongoose.model('DynamicSeo',DynamicSeoSchema);