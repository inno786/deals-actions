/*
 * To handle the User collection in the connected database means a User model
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectTaxonomySchema = new Schema({
  fk_product_id:{
      type: Schema.Types.ObjectId,
      ref: 'Product'
  },
  fk_sub_category_id:{
      type: Schema.Types.ObjectId,
      ref: 'SubCategory'
  },
  fk_attribute_value_id:{
      type: Schema.Types.ObjectId,
      ref: 'AttributeValue'
  },
  taxonomy:{
      type: String
  },
  status:{
      type: Number,default:1
  },
  source:{
      type: String
  },
  created_on:{
        type:Date,default:Date.now()
    },
  updated_on:{
      type:Date,default:Date.now()
  }
});
module.exports = mongoose.model('ObjectTaxonomy',ObjectTaxonomySchema);
