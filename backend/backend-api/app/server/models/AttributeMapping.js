/*
 * To handle the User collection in the connected database means a User model
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var AttributeMappingSchema = new Schema({
    fk_subcat_id:{
        type: Schema.Types.ObjectId,
		    ref: 'SubCategory'
    },
    fk_attribute_id:{
         type: Schema.Types.ObjectId,
         ref: 'Attribute'
    },
    display_name: {
         type : String , required : true
    },
    name: {
         type : String , required : true
    },
    slug: {
         type : String ,required : true
    },
    parent_id: {
         type : String
    },
    image: {
         type : String
    },
    status:{
        type: Number,default:0
    },
    created_on:{
        type:Date,default:Date.now()
    },
    updated_on:{
        type:Date,default:Date.now()
    }
});
module.exports = mongoose.model('AttributeMapping',AttributeMappingSchema);
