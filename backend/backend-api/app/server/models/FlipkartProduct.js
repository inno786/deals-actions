/*
 * To handle the User collection in the connected database means a User model
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var FlipkartProductSchema = new Schema({
    resource:{
        type : String,
        ref: 'FlipKartCategory'
    },
  raw_data: {
         type : String
    },
    status:{
      type: String
    },
    created_on:{
        type:Date,default:Date.now()
    },
    updated_on:{
        type:Date,default:Date.now()
    }
});
module.exports = mongoose.model('FlipkartProduct',FlipkartProductSchema);
