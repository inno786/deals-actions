/*
 * To handle the User collection in the connected database means a User model
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ProductSchema = new Schema({
    product_id:{
        type: String
    },
    vendor:{
        type: String
    },
    name: {
         type : String
    },
    slug: {
         type : String
    },
    description:{
        type: String
    },
    maximum_retail_price:{
        type: Number,default:0
    },
    flipkart_selling_price:{
        type: Number,default:0
    },
    flipkart_special_price:{
        type: Number,default:0
    },
    shipping_charges:{
        type: Number,default:0
    },
    estimated_delivery_time:{
        type: String
    },
    seller_name:{
        type: String
    },
    seller_average_rating:{
        type: String
    },
    seller_no_of_ratings:{
        type: Number,default:0
    },
    seller_no_of_reviews:{
        type: Number,default:0
    },
    product_url:{
        type: String
    },
    img_200:{
        type: String
    },
    img_400:{
        type: String
    },
    img_800:{
        type: String
    },
    stock:{
        type: Number,default:1
    },
    parent_id:{
          type: String
    },
    status:{
        type: String, required : true
    },
    visitors:{
        type: Number,default:1
    },
    created_on:{
        type:Date,default:Date.now()
    },
    updated_on:{
        type:Date,default:Date.now()
    }
});
module.exports = mongoose.model('Product',ProductSchema);
