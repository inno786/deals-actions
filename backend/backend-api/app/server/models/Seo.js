/* 
 * To handle the User collection in the connected database means a User model
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var SeoSchema = new Schema({
    fk_id:{
        type: Schema.Types.ObjectId
    },
    type: {
         type : String
    },
    keyword: {
         type : String
    },
    top_description:{
        type: String
    },
    bottom_description:{
        type: String
    },
    created_on:{
        type:Date,default:Date.now()
    },
    updated_on:{
        type:Date,default:Date.now()
    }
});
module.exports = mongoose.model('Seo',SeoSchema);