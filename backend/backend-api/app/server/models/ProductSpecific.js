/*
 * To handle the User collection in the connected database means a User model
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ProductSpecificSchema = new Schema({
  name: {
       type : String
  },
  status:{
      type: Number,default:0
  }
});
module.exports = mongoose.model('ProductSpecific',ProductSpecificSchema);
