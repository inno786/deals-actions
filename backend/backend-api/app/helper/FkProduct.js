const Client = require('flipkart-affiliate'),
    apiCredintial = require('../config/apiCredintial.js'),
    dateFormat = require('dateformat'),
    asyncLoop = require('node-async-loop'),
    FlipKartCategory = require('mongoose').model('FlipKartCategory'),
    FlipkartProduct = require('mongoose').model('FlipkartProduct');

let affObj = Client.createClient({
    FkAffId: apiCredintial.flipkart.affID,
    FkAffToken: apiCredintial.flipkart.token,
    responseType: 'json'
});
let now = new Date();
let current_datetime = dateFormat(now, "yyyy-mm-dd HH:MM:ss");
let defThis;

class FkProduct {

    constructor() {
        defThis = this;
    }

    async getProduct(reqBody) {
        return new Promise(async (resolve, reject) => {
            if (reqBody.id != '') {
                let fkCatRes = await this.findCategory(reqBody)
                if (fkCatRes.error) {
                    return resolve({
                        message: JSON.stringify(fkCatRes.error)
                    })
                } else {
                    let arrData = fkCatRes.result
                    if (arrData) {
                        let catUrl = arrData.url + '&inStock=true';
                        let productsFeedRes = await this.getProductsFeed(catUrl)
                        if (productsFeedRes.error) {
                            return resolve({
                                message: JSON.stringify(productsFeedRes.error)
                            })
                        } else {
                            this.fkTraverseData({
                                resource: arrData.resource,
                                arrData: JSON.parse(productsFeedRes.result)
                            }, (error, response) => {
                                if (error) {
                                    return resolve({
                                        message: JSON.stringify(error)
                                    })
                                } else {
                                    return resolve({
                                        message: response
                                    })
                                }
                            });
                        }
                    } else {
                        return resolve({
                            message: "No data found"
                        })
                    }
                }
            } else {
                return resolve({
                    message: "No resource available"
                })
            }
        })
    }

    async fkTraverseData(options, callback) {
        if (options.arrData.nextUrl) {
            var temp = {
                resource: options.resource,
                raw_data: JSON.stringify(options.arrData),
                status: 'ready',
                created_on: current_datetime,
                updated_on: current_datetime
            };
            let addProdRes = await this.addFlipkartProduct(temp)
            if (addProdRes.arror) {
                return callback(addProdRes.arror)
            } else {
                let productsFeedRes = await this.getProductsFeed(options.arrData.nextUrl)
                if (productsFeedRes.error) {
                    return callback(productsFeedRes.error)
                } else {
                    return this.fkTraverseData({
                        resource: options.resource,
                        arrData: JSON.parse(productsFeedRes.result)
                    }, callback)
                }
            }
        } else {
            return callback(null, 'Category product has been imported successfully.')
        }
    }

    // find category
    findCategory(data) {
        return new Promise(resolve => {
            return FlipKartCategory.findOne({ _id: data.id }, (error, result) => {
                if (error) {
                    return resolve({ error: error })
                } else {
                    return resolve({ error: null, result: result })
                }
            })
        })
    }
    // add product feed
    addFlipkartProduct(objData) {
        let obj = new FlipkartProduct(objData);
        return new Promise(resolve => {
            return obj.save((error, result) => {
                if (error) {
                    return resolve({ error: error })
                } else {
                    return resolve({ error: null, result: result })
                }
            })
        })
    }
    // get product feed
    getProductsFeed(catUrl) {
        return new Promise(resolve => {
            return affObj.getProductsFeed({
                url: catUrl
            }, (error, result) => {
                if (error) {
                    return resolve({ error: error })
                } else {
                    return resolve({ error: null, result: result })
                }
            })
        })
    }

}

module.exports = FkProduct