const Client = require('flipkart-affiliate'),
    apiCredintial = require('../config/apiCredintial.js'),
    dateFormat = require('dateformat'),
    asyncLoop = require('node-async-loop'),
    FlipKartCategory = require('mongoose').model('FlipKartCategory');

let affObj = Client.createClient({
    FkAffId: apiCredintial.flipkart.affID,
    FkAffToken: apiCredintial.flipkart.token,
    responseType: 'json'
});

class FkCategory {

    getCategory() {
        let now = new Date();
        let current_datetime = dateFormat(now, "yyyy-mm-dd HH:MM:ss");

        return new Promise((resolve, reject) => {
            return affObj.getCategoryFeed({trackingId: apiCredintial.flipkart.affID}, function (err, result) {
                if (err) {
                    return resolve({
                        message: JSON.stringify(err)
                    })
                } else {
                    let arrData = JSON.parse(result);
                    let apiListings = arrData.apiGroups.affiliate.apiListings;
                    let jsonToArray = [];
                    let keys = Object.keys(apiListings);
                    return asyncLoop(keys, function (elem, callback) {
                        jsonToArray.push(apiListings[elem]);
                        callback();
                    }, function (loopErr) {
                        if (loopErr) {
                            return resolve({
                                message: JSON.stringify(loopErr)
                            })
                        } else {
                            if (jsonToArray.length > 0) {
                                return FlipKartCategory.remove().exec(function (errRemove, arrRemove) {
                                    if (errRemove) {
                                        return resolve({
                                            message: JSON.stringify(errRemove)
                                        })
                                    } else {
                                        return asyncLoop(jsonToArray, function (arrObj, callbackInner) {

                                            let obj = arrObj.availableVariants['v1.1.0'];
                                            let name = obj.resourceName.trim();
                                            let newString = name.replace(/\b[a-z]/g, function (f) {
                                                return f.toUpperCase();
                                            });
                                            var temp = {
                                                name: newString.replace(/_/g, " "),
                                                resource: name,
                                                url: obj.get.trim(),
                                                status: 'inactive',
                                                created_on: current_datetime,
                                                updated_on: current_datetime
                                            };
                                            var catObj = new FlipKartCategory(temp);
                                            catObj.save((errSaved, arrDataSaved) => {
                                                if (errSaved) {
                                                  /*  return resolve({
                                                        message: JSON.stringify(errSaved)
                                                    }) */
                                                } else {
                                                  //  callbackInner();
                                                }
                                                callbackInner();
                                            });
                                        }, function (errInner) {
                                            if (errInner) {
                                                return resolve({
                                                    message: JSON.stringify(errInner)
                                                })
                                            } else {
                                                return resolve({
                                                    message: 'Category has been imported successfully'
                                                })
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    });
                }
            });
        })
    }
}

module.exports = FkCategory