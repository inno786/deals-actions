const mongoose = require('mongoose'),
    dateFormat = require('dateformat'),
    asyncLoop = require('node-async-loop'),
    asyncEach = require("async"),
    replaceall = require("replaceall"),
    FlipkartProduct = mongoose.model('FlipkartProduct'),
    FlipKartCategory = mongoose.model('FlipKartCategory'),
    Product = mongoose.model('Product'),
    ProductSpecific = mongoose.model('ProductSpecific'),
    ProductSpecificList = mongoose.model('ProductSpecificList'),
    Category = mongoose.model('Category'),
    SubCategory = mongoose.model('SubCategory'),
    Attribute = mongoose.model('Attribute'),
    AttributeValue = mongoose.model('AttributeValue'),
    ObjectTaxonomy = mongoose.model('ObjectTaxonomy');

let now = new Date();
let current_datetime = dateFormat(now, "yyyy-mm-dd HH:MM:ss");
let defThis;

let response = {
    error: null
}

const feedSource = 'Flipkart'

class FkProductImport {

    constructor() {
        defThis = this;
    }

    updateResource(condition) {

        let obj = {
            status: condition.status,
            updated_on: current_datetime
        };

        return new Promise((resolve, reject) => {
           return FlipkartProduct.update({_id: condition._id}, {$set: obj}, (error, result) => {
                if (error) {
                    return resolve({error:error})
                } else {
                    return resolve({error:null, result:'success'})
                }
            })
        })
    }

productList(reqBody) {

 return new Promise(resolve => {

    return asyncLoop(reqBody, async (items, callback) => {

        if(items)
        {
            let inputData = items.productBaseInfoV1;
            let shipping = items.productShippingInfoV1;
            let specification = items.categorySpecificInfoV1.specificationList;

            if (Object.hasOwnProperty.call(inputData, 'attributes')) {
                inputData.attributes.brands = inputData.productBrand;
            }
            let arrAttributes = await this.meargeAttribute(inputData.attributes)
            /* console.log("inputData =>", JSON.stringify(items.categorySpecificInfoV1.specificationList, " ", 2))
            console.log("arrAttributes =>", arrAttributes)
            return resolve({error:null, result:arrAttributes}) */

            if (inputData.productId) {
                let productExists = await this.findProduct({product_id: inputData.productId})
                if (productExists.error) {
                    console.error(productExists.error)
                    callback()
                } else {
                    if (productExists.result) {
                        // Update case
                        let updatePrice = await this.updatePrice(productExists.result, inputData, shipping)
                        let addProductSpecific = await this.addProductSpecificList(productExists.result._id, specification)
                        if (updatePrice.error) {
                            console.error(updatePrice.error)
                            callback()
                        } else {
                            //console.log("Product updated successfully")
                            callback()
                        }
                    } else {
                        // New item insert
                        let insertProduct = await this.addProduct(inputData, shipping)

                        if (insertProduct.error) {
                            console.error(insertProduct.error)
                            callback()
                        } else {
                            let addProductSpecific = await this.addProductSpecificList(insertProduct.result._id, specification)
                           
                            let arrCategory = inputData.categoryPath.split('>');
                              //  arrCategory.shift(); // skip parent category
                            if (arrCategory.length > 0) {

                                let categoryInfo = await this.findCategory({search_index: arrCategory[0]})
                                if (categoryInfo.error) {
                                    console.error(categoryInfo.error)
                                    callback()
                                } else {
                                    if (categoryInfo.result) {
                                        arrCategory.shift(); // skip parent category
                                        if (arrCategory.length > 0) {
                                            // Add sub cat and object taxonomy
                                            let subCategoryResp = await this.subCategoryMgt(arrCategory, insertProduct.result, categoryInfo.result)
                                            // start process on Attributes
                                            if (arrAttributes.length > 0) {
                                                 let attributeResp = await this.attributeMgt(
                                                    arrAttributes,
                                                    insertProduct.result._id,
                                                    insertProduct.result.img_200
                                                );
                                                callback()
                                            }
                                        }else{
                                            callback()
                                        }
                                    } else {
                                        // Parent category no exists
                                        let replaceSpecialChar = arrCategory[0].replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
                                        let replaceSpace = replaceSpecialChar.replace(/  +/g, ' ');
                                        let slug = replaceall(" ", "-", replaceSpace).toLowerCase();

                                        let name = arrCategory[0].replace(/([a-z])([A-Z])/g, '$1 $2');
                                        name = name.charAt(0).toUpperCase() + name.slice(1);
                                        let temp = {
                                            name: name,
                                            slug: slug,
                                            search_index: arrCategory[0],
                                            description: '',
                                            image: insertProduct.result.img_200,
                                            status: 1,
                                            main_category: 0,
                                            source: feedSource,
                                            created_on: insertProduct.result.created_on,
                                            updated_on: insertProduct.result.updated_on
                                        }
                                        let obj = new Category(temp);
                                        obj.save(async (error, objCat) => {
                                            if (error) {
                                                console.error(error)
                                                callback()
                                            } else {
                                                arrCategory.shift(); // skip parent category
                                                if (arrCategory.length > 0) {
                                                    // Add sub cat and object taxonomy
                                                    let subCategoryResp = await this.subCategoryMgt(arrCategory, insertProduct.result, objCat)
                                                    // start process on Attributes
                                                    if (arrAttributes.length > 0) {
                                                        let attributeResp = await this.attributeMgt(
                                                            arrAttributes,
                                                            insertProduct.result._id,
                                                            insertProduct.result.img_200
                                                        ) 
                                                    }
                                                    callback()
                                                }else{
                                                    callback()
                                                }
                                            }
                                        })
                                    }
                                }
                            }else{
                                callback()
                            }
                        }
                    }
                }
            }else{
                callback()
            }
        }else{
         callback();    
        }
        }, error => {
                return resolve({error:null, result:'success'})
        })
    })
 }

    // Add Attribute
    meargeAttribute(attribute) {
        return new Promise(resolve => {
            let arrAttributes = [];
           return asyncEach.forEachOf(attribute, (item, key, callback) => {
                if (item) {
                    arrAttributes.push({key: key, value: item});
                }
                callback()
            }, error => {
                return resolve(arrAttributes)
            })
        })
    }

    /**
     * Find product
     */
    findProduct(condition) {
        return new Promise(resolve => {

           return Product.findOne(condition, (error, response) => {
                if (error) {
                    return resolve({error: error})
                } else {
                    return resolve({error: null, result: response})
                }
            })
        })
    }

    /**
     * Update price
     * @param params
     * @returns {Promise}
     */
    updatePrice(arrProduct, inputData, shipping) {

        let nowTime = new Date();
        let current_date_and_time = dateFormat(nowTime, "yyyy-mm-dd HH:MM:ss");

        return new Promise(resolve => {
            let objProdUpdate ={
                maximum_retail_price: (Object.hasOwnProperty.call(inputData, 'maximumRetailPrice')  && Object.hasOwnProperty.call(inputData.maximumRetailPrice, 'amount')) ? inputData.maximumRetailPrice.amount : arrProduct.maximum_retail_price,
                flipkart_selling_price: (Object.hasOwnProperty.call(inputData, 'flipkartSellingPrice') && Object.hasOwnProperty.call(inputData.flipkartSellingPrice, 'amount')) ? inputData.flipkartSellingPrice.amount : arrProduct.flipkart_selling_price,
                flipkart_special_price: (Object.hasOwnProperty.call(inputData, 'flipkartSpecialPrice') && Object.hasOwnProperty.call(inputData.flipkartSpecialPrice, 'amount')) ? inputData.flipkartSpecialPrice.amount : arrProduct.flipkart_special_price,
                shipping_charges: (Object.hasOwnProperty.call(shipping, 'shippingCharges')  && Object.hasOwnProperty.call(shipping.shippingCharges, 'amount'))? shipping.shippingCharges.amount : arrProduct.shipping_charges,
                estimated_delivery_time: (Object.hasOwnProperty.call(shipping, 'estimatedDeliveryTime'))? shipping.estimatedDeliveryTime : arrProduct.estimated_delivery_time,
                seller_name: (Object.hasOwnProperty.call(shipping, 'sellerName'))? shipping.sellerName : arrProduct.seller_name,
                seller_average_rating: (Object.hasOwnProperty.call(shipping, 'sellerAverageRating'))? shipping.sellerAverageRating : arrProduct.seller_average_rating,
                seller_no_of_ratings: (Object.hasOwnProperty.call(shipping, 'sellerNoOfRatings'))? shipping.sellerNoOfRatings : arrProduct.seller_no_of_ratings,
                seller_no_of_reviews: (Object.hasOwnProperty.call(shipping, 'sellerNoOfReviews'))? shipping.sellerNoOfReviews : arrProduct.seller_no_of_reviews,
                stock: (Object.hasOwnProperty.call(inputData, 'inStock') && inputData.inStock) ? 1:0,
                updated_on : current_date_and_time
            };
            return Product.update({_id: arrProduct._id}, {$set: objProdUpdate}, (error, response) => {
                if (error) {
                    return resolve({error: error})
                } else {
                    return resolve({error: null})
                }
            })
        })
    }

    /**
     * Add Product
     * @param inputData
     * @returns {Promise}
     */
    async addProduct(inputData, shipping) {

        let nowTime = new Date();
        let current_date_and_time = dateFormat(nowTime, "yyyy-mm-dd HH:MM:ss");
        return new Promise(async (resolve) => {
            let name = inputData.title;
            let replaceSpecialChar = name.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
            let replaceSpace = replaceSpecialChar.replace(/  +/g, ' ');
            let splitString = replaceSpace.split(' ').slice(0, 20).join(' ');
            let slug = replaceall(" ", "-", splitString).toLowerCase();

            let existsProduct = await this.findProduct({slug: slug, parent_id: null})
            if (existsProduct.error) {
                console.error(existsProduct.error)
                return resolve({error: existsProduct.error})
            } else {
                let parent_id = null;
                let status = 'published';
                if (existsProduct.result) {
                    parent_id = existsProduct.result.product_id;
                    status = 'inherit';
                }
                    let amount = 0;
                    if (Object.hasOwnProperty.call(inputData, 'flipkartSpecialPrice')) {
                        if (inputData.flipkartSpecialPrice) {
                            amount = inputData.flipkartSpecialPrice.amount;
                        }
                    }
                    let temp = {
                        product_id: Object.hasOwnProperty.call(inputData,'productId') ? inputData.productId : '',
                        vendor: 'Flipkart',
                        name: Object.hasOwnProperty.call(inputData,'title') ? inputData.title : '',
                        slug: slug,
                        description: Object.hasOwnProperty.call(inputData,'productDescription') ? inputData.productDescription : '',
                        maximum_retail_price: (Object.hasOwnProperty.call(inputData,'maximumRetailPrice')  && Object.hasOwnProperty.call(inputData.maximumRetailPrice,'amount')) ? inputData.maximumRetailPrice.amount : 0,
                        flipkart_selling_price: (Object.hasOwnProperty.call(inputData,'flipkartSellingPrice') && Object.hasOwnProperty.call(inputData.flipkartSellingPrice,'amount')) ? inputData.flipkartSellingPrice.amount : 0,
                        flipkart_special_price: (Object.hasOwnProperty.call(inputData,'flipkartSpecialPrice') && Object.hasOwnProperty.call(inputData.flipkartSpecialPrice,'amount')) ? inputData.flipkartSpecialPrice.amount : 0,
                        shipping_charges: (Object.hasOwnProperty.call(shipping,'shippingCharges') && Object.hasOwnProperty.call(shipping.shippingCharges,'amount'))? shipping.shippingCharges.amount : 0,
                        estimated_delivery_time: (Object.hasOwnProperty.call(shipping,'estimatedDeliveryTime'))? shipping.estimatedDeliveryTime : '',
                        seller_name: (Object.hasOwnProperty.call(shipping,'sellerName'))? shipping.sellerName : '',
                        seller_average_rating: (Object.hasOwnProperty.call(shipping,'sellerAverageRating'))? shipping.sellerAverageRating : 0,
                        seller_no_of_ratings: (Object.hasOwnProperty.call(shipping,'sellerNoOfRatings'))? shipping.sellerNoOfRatings : 0,
                        seller_no_of_reviews: (Object.hasOwnProperty.call(shipping,'sellerNoOfReviews'))? shipping.sellerNoOfReviews : 0,
                        product_url: Object.hasOwnProperty.call(inputData,'productUrl') ? inputData.productUrl.split('&affid=indarbiha').slice(0).join('') : '',
                        img_200: (Object.hasOwnProperty.call(inputData,'imageUrls') && Object.hasOwnProperty.call(inputData.imageUrls,'200x200')) ? inputData.imageUrls['200x200'] : '',
                        img_400: (Object.hasOwnProperty.call(inputData,'imageUrls') && Object.hasOwnProperty.call(inputData.imageUrls,'400x400')) ? inputData.imageUrls['400x400'] : '',
                        img_800: (Object.hasOwnProperty.call(inputData,'imageUrls') && Object.hasOwnProperty.call(inputData.imageUrls,'800x800')) ? inputData.imageUrls['800x800'] : '',
                        stock: (Object.hasOwnProperty.call(inputData,'inStock') && inputData.inStock) ? 1 : 0,
                        parent_id: parent_id,
                        status: status,
                        visitors: 1,
                        created_on: current_date_and_time,
                        updated_on: current_date_and_time
                    }
                    let productObj = new Product(temp);
                    return productObj.save((error, response) => {
                        if (error) {
                            return resolve({error: error})
                        } else {
                            return resolve({error: null, result: response})
                        }
                    })
            }
        })
    }

    /**
     * find category
     * @param condition
     * @returns {Promise}
     */
    findCategory(condition) {
        return new Promise(resolve => {

            return Category.findOne(condition, (error, response) => {
                if (error) {
                    return resolve({error: error})
                } else {
                    return resolve({error: null, result: response})
                }
            })
        })
    }

    /**
     * @start process
     * @param params
     * @returns {Promise}
     */
    async subCategoryMgt(arrCategory, insertProduct, categoryInfo) {

        return new Promise(resolve => {

            return asyncLoop(arrCategory, async (items, callback) => {

                let response = await this.findSubcategory({fk_category_id: mongoose.Types.ObjectId(categoryInfo._id), feed_name: items})
                if (response.error) {
                    console.error(response.error)
                    callback()
                } else {
                    if (response.result) {
                        let objTaxoCheck = await this.findObjectTaxonomy({
                            fk_product_id: mongoose.Types.ObjectId(insertProduct._id),
                            fk_attribute_value_id: mongoose.Types.ObjectId(response.result._id)
                        })
                        if (objTaxoCheck.error) {
                            console.error(objTaxoCheck.error)
                            callback()
                        } else {
                            if (!objTaxoCheck.result) {
                                await this.insertObjectTaxonomy(
                                    mongoose.Types.ObjectId(insertProduct._id),
                                    mongoose.Types.ObjectId(response.result._id),
                                    "Category"
                                )
                                callback()
                            }else{
                                callback()
                            }
                        }
                    } else {
                        let subcategoryRes = await this.insertSubCategory(
                            items,
                            mongoose.Types.ObjectId(categoryInfo._id),
                            insertProduct.img_200
                        )
                        if (subcategoryRes.error) {
                            console.error(subcategoryRes.error)
                            callback()
                        } else {
                            let objTaxoCheck = await this.findObjectTaxonomy({
                                fk_product_id: mongoose.Types.ObjectId(insertProduct._id),
                                fk_attribute_value_id: mongoose.Types.ObjectId(subcategoryRes.result._id)
                            })
                            if (objTaxoCheck.error) {
                                console.error(objTaxoCheck.error)
                                callback()
                            } else {
                                if (!objTaxoCheck.result) {
                                    await this.insertObjectTaxonomy(
                                        mongoose.Types.ObjectId(insertProduct._id),
                                        mongoose.Types.ObjectId(subcategoryRes.result._id),
                                        "Category"
                                    )
                                    callback()
                                }else{
                                    callback()
                                }
                            }
                        }
                    }
                }
            }, error => {
                if (error) {
                    console.error(error)
                    return resolve({error: error})
                } else {
                    return resolve({error: null, result: 'success'})
                }
            })
        })
    }

    /**
     * @find Subcategory
     * @param condition
     * @returns {Promise}
     */
    findSubcategory(condition) {
        return new Promise(resolve => {

           return SubCategory.findOne(condition, (error, response) => {
                if (error) {
                    return resolve({error: error})
                } else {
                    return resolve({error: null, result: response})
                }
            })
        })
    }

    /**
     * @insert into Object Taxonomy collection
     * @param params
     * @returns {Promise}
     */
    insertObjectTaxonomy(product_id, subcat_id, taxonomy) {

        let nowTime = new Date();
        let current_date_and_time = dateFormat(nowTime, "yyyy-mm-dd HH:MM:ss");

        return new Promise(resolve => {

            let temp = {
                fk_product_id: product_id,
                fk_sub_category_id: subcat_id,
                fk_attribute_value_id: subcat_id,
                taxonomy: taxonomy,
                status: 1,
                source: feedSource,
                created_on: current_date_and_time,
                updated_on: current_date_and_time
            }
            let obj = new ObjectTaxonomy(temp);
           return obj.save((error, response) => {
                if (error) {
                    return resolve({error: error})
                } else {
                    return resolve({error: null, result: response})
                }
            });
        })
    }

    /**
     * @insert into subcategory collection
     * @param params
     * @returns {Promise}
     */
    insertSubCategory(items, category_id, small_image) {

        let nowTime = new Date();
        let current_date_and_time = dateFormat(nowTime, "yyyy-mm-dd HH:MM:ss");
        return new Promise(resolve => {
            let replaceSpecialChar = items.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
            let replaceSpace = replaceSpecialChar.replace(/  +/g, ' ');
            let slug = replaceall(" ", "-", replaceSpace).toLowerCase();

            let temp = {
                fk_category_id: category_id,
                name: items,
                slug: slug,
                feed_name: items,
                description: '',
                image: small_image,
                status: 1,
                source: feedSource,
                created_on: current_date_and_time,
                updated_on: current_date_and_time
            }
            let obj = new SubCategory(temp)
            return obj.save((error, response) => {
                if (error) {
                    return resolve({error: error})
                } else {
                    return resolve({error: null, result: response})
                }
            })
        })
    }

    /**
     * attribure mgt
     * @param params
     * @returns {Promise}
     */
    attributeMgt(arrAttributes, product_id, small_image) {

        let nowTime = new Date();
        let created_on = dateFormat(nowTime, "yyyy-mm-dd HH:MM:ss");
        let updated_on = created_on;
        let source = feedSource;

        return new Promise(resolve => {

            return asyncLoop(arrAttributes, async (item, callback) => {

                let attributeRes = await this.findAttribute({name: item.key})
                if (attributeRes.error) {
                    console.error(attributeRes.error)
                    callback()
                } else {
                    if (attributeRes.result) {

                        let attributeValueRes = await this.findAttributeValue({
                            fk_attribute_id: attributeRes.result._id,
                            name: {'$regex': '^' + item.value, '$options': 'i'}
                        })
                        if (attributeValueRes.error) {
                            console.error(attributeValueRes.error)
                        } else {
                            if (attributeValueRes.result) {
                                let objTaxoCheck = await this.findObjectTaxonomy({
                                    fk_product_id: product_id,
                                    fk_attribute_value_id: attributeValueRes.result._id
                                })
                                if (objTaxoCheck.error) {
                                    console.error(objTaxoCheck.error)
                                } else {
                                    if (!objTaxoCheck.result) {
                                        await this.insertObjectTaxonomy(
                                            product_id,
                                            attributeValueRes.result._id,
                                            "Attribute"
                                        )
                                    }
                                }
                            } else {
                                // Add Attribute value
                                let attrValueRes = await this.insertAttributeValue(
                                    item.value,
                                    attributeRes.result._id,
                                    small_image
                                )
                                if (attrValueRes.error) {
                                    console.error(attrValueRes.error)
                                } else {
                                    let objTaxoCheck = await this.findObjectTaxonomy({
                                        fk_product_id: product_id,
                                        fk_attribute_value_id: attrValueRes.result._id
                                    })
                                    if (objTaxoCheck.error) {
                                        console.error(objTaxoCheck.error)
                                    } else {
                                        if (!objTaxoCheck.result) {
                                            await this.insertObjectTaxonomy(
                                                product_id,
                                                attrValueRes.result._id,
                                                "Attribute"
                                            )
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        // Insert Attribute
                        let attrResp = await this.insertAttribute(item.key, small_image)
                        if (attrResp.error) {
                            console.error(attrResp.error)
                        } else {
                            let attributeValueRes = await this.findAttributeValue({
                                fk_attribute_id: attrResp.result._id,
                                name: {'$regex': '^' + item.value, '$options': 'i'}
                            })
                            if (attributeValueRes.error) {
                                console.error(attributeValueRes.error)
                            } else {
                                if (attributeValueRes.result) {
                                    let objTaxoCheck = await this.findObjectTaxonomy({
                                        fk_product_id: product_id,
                                        fk_attribute_value_id: attributeValueRes.result._id
                                    })
                                    if (objTaxoCheck.error) {
                                        console.error(objTaxoCheck.error)
                                    } else {
                                        if (!objTaxoCheck.result) {
                                            await this.insertObjectTaxonomy(
                                                product_id,
                                                attributeValueRes.result._id,
                                                "Attribute"
                                            )
                                        }
                                    }
                                } else {
                                    // Add Attribute value
                                    let attrValueRes = await this.insertAttributeValue(
                                        item.value,
                                        attrResp.result._id,
                                        small_image
                                    )
                                    if (attrValueRes.error) {
                                        console.error(attrValueRes.error)
                                    } else {
                                        let objTaxoCheck = await this.findObjectTaxonomy({
                                            fk_product_id: product_id,
                                            fk_attribute_value_id: attrValueRes.result._id
                                        })
                                        if (objTaxoCheck.error) {
                                            console.error(objTaxoCheck.error)
                                        } else {
                                            if (!objTaxoCheck.result) {
                                                await this.insertObjectTaxonomy(
                                                    product_id,
                                                    attrValueRes.result._id,
                                                    "Attribute"
                                                )
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    callback()
                }
            }, error => {
                if (error) {
                    return resolve({error: error})
                } else {
                    return resolve({error: null, result: 'success'})
                }
            })
        })
    }

    /**
     * @find Attribute
     * @param condition
     * @returns {Promise}
     */
    findAttribute(condition) {
        return new Promise(resolve => {

            return Attribute.findOne(condition, (error, response) => {
                if (error) {
                    return resolve({error: error})
                } else {
                    return resolve({error: null, result: response})
                }
            })
        })
    }

    /**
     * @find attribute value
     * @param condition
     * @returns {Promise}
     */
    findAttributeValue(condition) {
        return new Promise(resolve => {

            return AttributeValue.findOne(condition, (error, response) => {
                if (error) {
                    return resolve({error: error})
                } else {
                    return resolve({error: null, result: response})
                }
            })
        })
    }

    /**
     * @find object taxonomy
     * @param condition
     * @returns {Promise}
     */
    findObjectTaxonomy(condition) {
        return new Promise(resolve=> {

            return ObjectTaxonomy.findOne(condition, (error, response) => {
                if (error) {
                    return resolve({error: error})
                } else {
                    return resolve({error: null, result: response})
                }
            })
        })
    }

    /**
     * @insert into Attribute Value collection
     * @param params
     * @returns {Promise}
     */
    insertAttributeValue(item, attr_id, image) {
        let nowTime = new Date();
        let created_on = dateFormat(nowTime, "yyyy-mm-dd HH:MM:ss");
        let updated_on = created_on;

        return new Promise(resolve => {
            let replaceSpecialChar = item.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
            let replaceSpace = replaceSpecialChar.replace(/  +/g, ' ');
            let slug = replaceall(" ", "-", replaceSpace).toLowerCase();

            let temp = {
                fk_attribute_id: attr_id,
                display_name: item,
                name: item,
                slug: slug,
                parent_id: null,
                image: image,
                status: 1,
                created_on: created_on,
                updated_on: updated_on
            }
            let obj = new AttributeValue(temp);
            return obj.save((error, response) => {
                if (error) {
                    return resolve({error: error})
                } else {
                    return resolve({error: null, result: response})
                }
            })
        })
    }

    /**
     * @insert attribute collection
     * @param params
     * @returns {Promise}
     */
    insertAttribute(key, image) {

        let nowTime = new Date();
        let current_date_and_time = dateFormat(nowTime, "yyyy-mm-dd HH:MM:ss");
        return new Promise(resolve => {
            let display_name = key.replace(/([a-z])([A-Z])/g, '$1 $2')
            display_name = display_name.charAt(0).toUpperCase() + display_name.slice(1)

            let temp = {
                name: key,
                display_name: display_name,
                image: image,
                status: 1,
                created_on: current_date_and_time,
                updated_on: current_date_and_time
            }
            let obj = new Attribute(temp);
            return obj.save((error, response) => {
                if (error) {
                    return resolve({error: error})
                } else {
                    return resolve({error: null, result: response})
                }
            })
        })
    }

    addProductSpecificList(prodId, specification){
        return new Promise(resolve =>{
            return asyncLoop(specification, async(item, callback) =>{

                let specificationRes = await this.findOrInsertSpecification(item.key);
                if(specificationRes.error)
                {
                    callback();
                }else{
                    let spList = await this.findOrUpdateOrInsertSpecificationList(prodId, specificationRes.id, item.values);
                    callback();
                }

            }, err =>{
                return resolve({error:err});
            })
        })
    }

    findOrInsertSpecification(key)
    {
        return new Promise(resolve =>{
            if(key)
            {
                return ProductSpecific.findOne({name: {'$regex': '^' + key, '$options': 'i'}}, (error, response) =>{
                    if(error)
                    {
                        return resolve({error:error});
                    }else{
                        if(response && Object.key(response).length >0)
                        {
                            return resolve({error:null, id:response._id});
                        }else{
                            let temp = {
                                name: key,
                                status: 1
                            }
                            let obj = new ProductSpecific(temp);
                            return obj.save((err, result) => {
                                if (err) {
                                    return resolve({error: err})
                                } else {
                                    return resolve({error:null, id:result._id});
                                }
                            })
                        }
                    }
                })
            }else{
                return resolve({error:error});
            }
        })
    }
    findOrUpdateOrInsertSpecificationList(prodId, spId, arrData)
    {
        return new Promise(resolve =>{
            return asyncLoop(arrData, async(item, callback) =>{
                 ProductSpecificList.findOne({
                        fk_product_id:prodId,
                        fk_ProductSpecific_id:spId,
                        key:item.key
                    }, (error, response) =>{
                    if(error)
                    {
                        callback();
                    }else{
                        if(response && Object.key(response).length >0)
                        {
                             ProductSpecificList.update({_id: response._id}, {$set: {value:item.value.join('^?')}}, (err, result) => {
                                if (err) {
                                    callback();
                                } else {
                                    callback();
                                }
                            })
                        }else{
                            let temp = {
                                fk_product_id: prodId,
                                fk_ProductSpecific_id: spId,
                                key:item.key,
                                value:item.value.join('^?'),
                                status:1
                            }
                            let obj = new ProductSpecificList(temp);
                            obj.save((errors, results) => {
                                if (errors) {
                                    callback();
                                } else {
                                    callback();
                                }
                            })
                        }
                    }
                })
            }, err =>{
                return resolve({error:err});
            })
        })
    } 
}

module.exports = FkProductImport