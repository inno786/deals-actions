require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const passport = require('passport');
const config = require('./config');
var  path = require('path');
var cors = require('cors');
var busboy = require('connect-busboy');

// connect to the database and load models
require('./server/models').connect(config.dbUri);

const app = express();

// Enable CORS
app.use(cors());
app.use(busboy());
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

// tell the app to parse HTTP body messages
global.rootPath = __dirname;
app.use(express.static(path.join(__dirname, 'public')));
// pass the passport middleware
app.use(passport.initialize());

// load passport strategies
const localSignupStrategy = require('./server/passport/local-signup');
const localLoginStrategy = require('./server/passport/local-login');
//const createCategoryStrategy = require('./server/passport/local-category');

//passport.use('local-signup', localSignupStrategy);
passport.use('local-login', localLoginStrategy);
//passport.use('local-category', createCategoryStrategy);

// pass the authorization checker middleware
const authCheckMiddleware = require('./server/middleware/auth-check');
app.use('/api', authCheckMiddleware);
app.use('/common', authCheckMiddleware);
app.use('/category', authCheckMiddleware);
app.use('/subcategory', authCheckMiddleware);
app.use('/mapping_category', authCheckMiddleware);
app.use('/attribute', authCheckMiddleware);
app.use('/subcat_attr', authCheckMiddleware);
app.use('/attribute_mapping', authCheckMiddleware);
app.use('/products', authCheckMiddleware);
app.use('/flipkart_category', authCheckMiddleware);
app.use('/flipkart_product', authCheckMiddleware);
app.use('/seo', authCheckMiddleware);
app.use('/homeSeo', authCheckMiddleware);
app.use('/reset', authCheckMiddleware);
//app.use('/flipkart', authCheckMiddleware);
//app.use('/amazon', authCheckMiddleware);

// routes
const authRoutes = require('./server/routes/auth');
const apiRoutes = require('./server/routes/api');
const fileuploadRoutes = require('./server/routes/fileupload');
const userRoutes = require('./server/routes/user');
const categoryRoutes = require('./server/routes/category');
const commonRoutes = require('./server/routes/common');
const subCategoryRoutes = require('./server/routes/subcategory');
const mappingCategoryRoutes = require('./server/routes/mapping_category');
const attributeRoutes = require('./server/routes/attribute');
const subCatAttrRoutes = require('./server/routes/subcat_attr');
const AttributeMappingRoutes = require('./server/routes/attribute_mapping');
const ProductsRoutes = require('./server/routes/products');
const FlipkartCategoryRoutes = require('./server/routes/flipkart_category');
const FlipkartProductRoutes = require('./server/routes/flipkart_product');
const FlipkartRoutes = require('./server/routes/flipkart');
const SeoRoutes = require('./server/routes/seo');
const HomeSeoRoutes = require('./server/routes/homeSeo');
const ResetRoutes = require('./server/routes/reset');
//const amazonRoutes = require('./server/routes/amazon');


app.use('/auth', authRoutes);
app.use('/api', apiRoutes);
app.use('/fileupload', fileuploadRoutes);
app.use('/user', userRoutes);
app.use('/category', categoryRoutes);
app.use('/common', commonRoutes);
app.use('/subcategory', subCategoryRoutes);
app.use('/mapping_category', mappingCategoryRoutes);
app.use('/attribute', attributeRoutes);
app.use('/subcat_attr', subCatAttrRoutes);
app.use('/attribute_mapping', AttributeMappingRoutes);
app.use('/products', ProductsRoutes);
app.use('/flipkart_category', FlipkartCategoryRoutes);
app.use('/flipkart_product', FlipkartProductRoutes);
app.use('/flipkart', FlipkartRoutes);
app.use('/seo', SeoRoutes);
app.use('/homeSeo', HomeSeoRoutes);
app.use('/reset', ResetRoutes);
//app.use('/amazon', amazonRoutes);

// start the server
app.listen(process.env.PORT, () => {
  console.log('Server is running on port *'+process.env.PORT);
});
